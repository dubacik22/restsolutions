﻿using Equationer;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading;

namespace EquationerTest
{
    [TestFixture]
    public class EquationerTests
    {
        private EquationParser parser;

        [SetUp]
        public void SetUp()
        {
            this.parser = new EquationParser
            {
                FetchFunction = name => new DummyFunc(),
                FetchVariable = name => new DummyValue(),
            };
        }

        [TestCase("(true) ? (4 lt 5) ? 1 : 2 : 3", 1.0)]
        [TestCase("(True) ? (4 lt 5) ? 1 : 2 : 3", 1.0)]
        [TestCase("(false) ? (4 lt 5) ? 1 : 2 : 3", 3.0)]
        [TestCase("(False) ? (4 lt 5) ? 1 : 2 : 3", 3.0)]
        [TestCase("(true) ? (4 lt 4) ? 1 : 2 : 3", 2.0)]
        [TestCase("(True) ? (4 lteq 4) ? 1 : 2 : 3", 1.0)]
        [TestCase("(true) ? (4 gt 4) ? 1 : 2 : 3", 2.0)]
        [TestCase("(true) ? (4 gteq 4) ? 1 : 2 : 3", 1.0)]
        [TestCase("4+5*(2-2)", 4.0)]
        [TestCase("-4.10586243451957E-08", -4.10586243451957E-08)]
        [TestCase("(4+2)", 6.0)]
        [TestCase("-(4+2)", -6.0)]
        [TestCase("-1.2 * f(v)", double.NaN)]
        [TestCase("(v) ? 1 : (v > 1) ? 's' : f(v, 2)", double.NaN)]
        [TestCase("('aa'== ( 'a' +'a') ) ? ( 4 + 8) : true", 12.0)]
        [TestCase("2^3", 8.0)]
        [TestCase("2^-3", 0.125)]
        [TestCase("(2+1)^3", 27.0)]
        [TestCase("15/3^1/2", 2.5)]
        [TestCase("15/3^(1/2)", 8.6602540378443873)]
        [TestCase("f(1)", 1)]
        [TestCase("f(0, 1 + 1)", 2)]
        [TestCase("1 + -1", 0)]
        [TestCase("(d == 0) ? 0 : -1", -1)]
        [TestCase("f1(0, (a == 0) ? a : f2(0, -3))", -3)]
        public void Parse_TestResultToDouble(string input, double expectedOutput)
        {
            var ret = this.parser.Parse(input);
            Assert.That(ret.GetCalculatedValue(), Is.EqualTo(expectedOutput));
        }

        [TestCase("(4 gteq 4)", true)]
        [TestCase("4 gteq 4", true)]
        [TestCase("5 lteq 4", false)]
        [TestCase("4 lteq 4", true)]
        [TestCase("3 gteq 4", false)]
        [TestCase("4 gteq 4", true)]
        [TestCase("4 lt 4", false)]
        [TestCase("3 lt 4", true)]
        [TestCase("4 gt 4", false)]
        [TestCase("4 gt 1", true)]
        [TestCase("!(( 'a' +'a')=='aa')", false)]
        [TestCase("-4.10586243451957E-08 lteq 2", true)]
        [TestCase("(4>5)==false", true)]
        [TestCase("4<=5", true)]
        [TestCase("(true) and ('qw' == 'as')", false)]
        [TestCase("true and (true and false and true and true and !false)", false)]
        [TestCase("5>(1+3)", true)]
        [TestCase("5 gt (1+3)", true)]
        [TestCase("4>=(1+3)", true)]
        [TestCase("4 gteq (1+3)", true)]
        [TestCase("4==(1+3)", true)]
        [TestCase("4 < -10", false)]
        [TestCase("4 lt -10", false)]
        [TestCase("-9 > -10", true)]
        [TestCase("4 > -10", true)]
        [TestCase("4 gt -10", true)]
        public void Parse_TestResultToBool(string input, bool expectedOutput)
        {
            var ret = this.parser.Parse(input);
            Assert.That(ret.GetCalculatedValue(), Is.EqualTo(expectedOutput));
        }

        [TestCase("'4'", "4")]
        [TestCase("( 'a' +'a')", "aa")]
        [TestCase("'a' + (('t' == 'f') ? 'a' : 'b')", "ab")]
        [TestCase("'\\'pripoj\\' popis tlacitka'", "'pripoj' popis tlacitka")]
        [TestCase("(true)?(false)?f1():'t':'l'", "t")]
        [TestCase("(true) ? (100 > (2 * 60)) ? 2 : 'cas je primoc maly' : 3", "cas je primoc maly")]
        [TestCase("f(2, 's' + v + 's' + v + 's', 's', (true) ? 'true' : 's', v, 's')", "true")]
        public void Parse_TestResultToString(string input, string expectedOutput)
        {
            var ret = this.parser.Parse(input);
            Assert.That(ret.GetCalculatedValue(), Is.EqualTo(expectedOutput));
        }

        [TestCase("!( 'a' +'a')", null)]
        public void Parse_TestResultToObject(string input, string expectedOutput)
        {
            var ret = this.parser.Parse(input);
            Assert.That(ret.GetCalculatedValue(), Is.EqualTo(expectedOutput));
        }

        [TestCase("4 fd 4")]
        [TestCase("f3(''l)")]
        [TestCase("f1(f2(v1), v2, 0), v2, 0)")]
        [TestCase("() ? f() : f(v / v, null, 0)")]
        public void Parse_Throws(string input)
        {
            Assert.That(() => this.parser.Parse(input), Throws.Exception);
        }

        [TestCase("f(v) + f(v)")]
        public void Parse_DontThrows(string input)
        {
            Assert.That(() => this.parser.Parse(input), Throws.Nothing);
        }

        [Test]
        public void Parse_ConcurentTests()
        {
            var testTuples = new List<Tuple<string, object>>()
            {
                new Tuple<string, object>("(2+1)^3", 27.0),
                new Tuple<string, object>("15/3^1/2", 2.5),
                new Tuple<string, object>("true and (true and false and true and true and !false)", false),
            };
            var tests = new List<ThreadTestAndResult>();
            foreach (var tuple in testTuples) tests.Add(new ThreadTestAndResult(tuple.Item1, tuple.Item2));
            foreach (var test in tests)
            {
                test.Thread.Start();
                Thread.Sleep(3);
            }
            foreach (var test in tests)
            {
                test.Thread.Join();
                Assert.That(test.ResultCalculatable.GetCalculatedValue(), Is.EqualTo(test.ExpectedResult));
            }
        }
        private class ThreadTestAndResult
        {
            private EquationParser parser = new EquationParser
            {
                FetchFunction = name => new DummyFunc(),
                FetchVariable = name => new DummyValue(),
            };
            public Thread Thread { get; }
            public ICalculatable ResultCalculatable { get; private set; }
            public object ExpectedResult { get; }

            public ThreadTestAndResult(string textToParse, object expectedResult)
            {
                this.Thread = new Thread(() =>
                {
                    this.ResultCalculatable = this.parser.Parse(textToParse);
                });
                this.ExpectedResult = expectedResult;
            }
        }
        //[TestMethod] 
        //public void TestEq24() 
        //{ 
        //    var ret = this.Parser.Parse("-4.10586243451957E-08 lteq 2"); 
        //    Assert.AreEqual(true, ret.GetCalculatedValue()); 
        //} 
        //[TestMethod] 
        //public void TestEq24() 
        //{ 
        //    var ret = this.Parser.Parse("-4.10586243451957E-08 lteq 2"); 
        //    Assert.AreEqual(true, ret.GetCalculatedValue()); 
        //} 
        private static void Equationer()
        {


            //var str = "PL163110663PL01"; 
            //var key = "052f07947591d5963ac3dfda7bf73757"; 
            //var masked = CS_OLD.MaskSting(str, key); 
            //var unmasked = CS_OLD.UnmaskSting(masked); 

            //parsed = parser.Parse("!Dummy(true)"); 
            //res = parsed.GetCalculatedValue(); 
            //parsed = parser.Parse("f1(f2(v*v,2),null,0)"); 
            //parsed = parser.Parse("a(a, f)+e"); 
            //parsed = parser.Parse("('aa'!='aa')?-2: (3+1)"); 
            //res = parsed.GetCalculatedValue(); 
            //parsed = parser.Parse("('aa'== ( 'a' +'a') ) ? ( 4 + 8) : true"); 
            //res = parsed.GetCalculatedValue(); 
            //parsed = parser.Parse("('aa'=='aa')?!true:true"); 
            //res = parsed.GetCalculatedValue(); 
            //parsed = parser.Parse("(f(#a) or f(#a) or f(#a))"); 
            //parsed = parser.Parse("!(f(#a) or f(#a) or f(#a))"); 
            //parsed = parser.Parse("AddToDate(#DatumMerania,0,2,0 )"); 
            //parsed = parser.Parse("mult(div(a,b),(4+5),'aa')"); 
            //parsed = parser.Parse("LimitToSnasU(null, 1, #Accreditation, ComboShortcut(#Name, #Name.$ItemsSource)+'_a', -60)"); 
            //res = parsed.GetCalculatedValue(); 
            //parsed = parser.Parse("avg(johany.inpRowsCnt, 'qwe') / 2"); 
            //parsed = parser.Parse("((4+5)/(2+8))"); 
            //parsed = parser.Parse("mult(-b+a)"); 
            //parsed = parser.Parse("(-mult(a,-b,4+5))+2"); 
            //parsed = parser.Parse("t(3-b)"); 
            //parsed = parser.Parse("mult(a,b,(4+5))"); 
            //parsed = parser.Parse("mult(a,b,((4+5)))"); 
            //parsed = parser.Parse("mult(a,4+5,)"); //ok 
            //parsed = parser.Parse("mult()"); 
            //parsed = parser.Parse("mult(,a)"); 

            //parsed = parser.Parse("mult((a,b,((4+5))))"); 
            //parsed = parser.Parse("mult(,"); 
            //parsed = parser.Parse(",mul"); 
            //parsed = parser.Parse("mult(a),b"); 
            //parsed = parser.Parse(")"); 
        }
    }
}
