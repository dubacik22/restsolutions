﻿using Equationer;

namespace EquationerTest
{
    public class DummyValue : IGetVariable
    {
        public object Variable { get => double.NaN; }
    }
}
