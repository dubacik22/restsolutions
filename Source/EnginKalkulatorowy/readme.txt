kniznica pre riesenie refreshu vypoctov (volani) metod pri zmene ich vstupnych parametrov.

Kniznica ma dokonceny IBA neoptimalizovany mod (ukazal sa byt rychlejsi).
V neoptimalizovanom mode sa spustaju vypocty v poradi interne vytvorenom pocas inicializacie EK (skor inicializovany vzorec sa refreshne skor).
EK v tomto mode caka na onPropertyChanged nainicializovanych objektov a v pripade, ak najde metodu ktora je zavisla od zmenenej property, tak ju spusti.
EK si vytvara stack metod ktore je potrebne spustit a snazi sa v ramci moznosti (vytvoreneho stacku) zamedzit opakovanemu spusteniu metod.
EK, podla priority, v pripade potreby spustenia metody ktora sa uz nachadza v stacku, tuto metodu presunie na zaciatok stacku, ponecha ju na mieste, alebo ju presunie na jeho koniec.

v EK je potrebne inicializovat metody pomocou metody
void add(EKEquation equation, EKProperty output, EKProperty input1, EKProperty input2, EKProperty input3, ...)

EKEquation (kuk EKEquationReadme)
EKProperty (kuk EKPropertyReadme)

V neoptimalizovanom mode EK nepouziva output propertu, preto je mozne inicializovat aj komplikovanejsie metody ktore menia viac vystupnych properiet naraz. vtedy staci nastavit ako vystupnu propertu hocico:
public bool Dummy { get; set; }
void setChangeDummy()
{
    var eq = new EKEquation(changeDummy, true);
	EKProperty oekp = new EKProperty("Dummy", this);
	EKProperty p1ekp = new EKProperty("p1", this);
	EKProperty p2ekp = new EKProperty("O1.p2", this);
	EK.add(eq, oekp, isokekp); //mame nastaveny force, co moze viest ku exception ak bude O1==null a nebude to v samotnej metode osetrene
}
void changeDummy(EKPropertyChangedEventArgs ea)
{
	LP1 = p1;
	LP2 = p1+O1.p2; //<- tu moze vzniknut problem ak pocas inicializacie EK nebude nastaveny O1
	...
}
V tomto pripade EK spusti changeDummy pri zmene p1 alebo O1 alebo p2. tato metoda zmeni rozne property, na ktore EK pocuva. AK sa najde metoda ktora je zavisla od zmenenych properiet, tak ich EK spusti.

EK do spustenych metod vklada EKPropertyChangedEventArgs (kuk EKPropertyChangedEventArgsReadme)

EK je mozne pauznut a znovu spustit:
EK.Pause(); - pozastavi automaticky prepocet vzorcov, iba si ich uklada do stacku. pauzovanie je aditivne, takze pre 3 volania pause je nutne 3x zavolat start
EK.Start(force); - znizi pocitadlo a ak sa vynuluje tak prepocita stack. v pripade ak force==true, tak sa resetne pauzove pocitadlo, takze vynuti prepocet stacku
!!! Pri vytvoreni EK je tento EK automaticky pauznuty. takze ak sa po jeho vytvoreni ihned aj inicializuje, tak sa vsetky metody ktore maju nastavene "recalculate" (alebo aj "force") nepocitaju, ale sa ukladaju do stacku.

v pripade potreby je mozne odinicializovat metodu. tymto sa odstrania listenery ktore pocuvaju na property tejto metody.
void remove(EquationFunc equation)

EK.Verbose nastavuje vyrecnost EK. AK je verbose==true, tak EK loguje viac hlasok o tom, aka properta sa prave zmenila a aky vzorec sa prave prepocitava atd.. loguje sa pomocou kniznice logger do logov (trace-ovych) ktore vytvorila aplikacia.
EK.VerboseMessages nastavuje spravy ktore chcem vo verbose mode logovat

--optimalizovany mod sa snazil vytvorit refresh retazec pre kazdu propertu ktora sa nachadzala v EK. toto vytvaranie optimalnych retazcov bolo velmi zdlhave.
komplikacie boli pri hladani properiet ktore mali byt rovnake (o2."o3.p1" == o1."o2.o3.p1") vtedy bolo potrebne prehladat zhody medzi vsetkymi propertami a v pripade zmeny regenerovat refresh retazec.

co sa tyka thread-safety:
mal by byt safe.
Cely stack pocita vlakno ktore spusti pocitanie.

EnginKalkulatorowyWThreading by mal spustat vzorce v osobitnom threade (pocitaju sa ale za radom, pocitacie vlakno ale uz nieje vlakno ktore spustilo pocitanie)
EnginKalkulatorowyWThreading potrebuje synchronizationContext do konstruktora. tento sa totom posiela do jednotlivych vzorcov cez eventArgs
zatial je vytvoreny a zbezne otestovany. takze opatrne

update 10.10.2017:
upraveny Namespace na Kalkulatorowanie
EK spusta prepocet v osobitnom vlakne.
Konstruktor EK potrebuje SynchronizationContent, ktory sa potom posiela do EKPropertyChangedEventArgs. tymto (ak do konstruktoru poslem synchronizationContent hlavneho vlakna) mozu metody ktore spusti EK (uz v osobitnom vlakne) zasahovat do hlavneho vlakna (ak napriklad treba upravit propertu UI atd).
Cely EK by mal odolat concurrent pristupom (upravene su iba public metody).


update 10.10.2017:
pridany EnginKalkulatorowyWThreading
