﻿using System;
using System.Reflection;

namespace Kalkulatorowanie
{
    public class IndexedPropertyInfo
    {
        public string PropertyPath;

        public object PropertyHolder;
        public Type PropertyHolderType;

        public PropertyInfo PropertyInfo;
        public object[] PropertyIndex;
        public bool? NumberIndex;
        public string PropertyName;
    }
}
