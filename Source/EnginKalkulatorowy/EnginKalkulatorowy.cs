﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using Logging;

namespace Kalkulatorowanie
{
    public class EnginKalkulatorowy
    {
        protected long lastID = 0;
        private readonly Equations equations = new Equations();
        private readonly Properties properties = new Properties();
        //private LinkedList<Property> ActProperty = new LinkedList<Property>();
        internal CalculatingEquationsList NonOptimizedEquationChain = new CalculatingEquationsList();
        protected long calculating = 0;
        public bool Verbose = false;
        public EVerboseMessages VerboseMessages = 0;
        public string VerbosePrefix = string.Empty;
        protected int pauseCount = 1;
        private readonly LinkedList<Equation> equationsToDispose = new LinkedList<Equation>();
        //private LinkedList<Property> propertiesToDispose = new LinkedList<Property>();

        public event EventHandler ChainCalculationStart;
        public event EventHandler ChainCalculationEnd;

        public int PauseCounter => this.pauseCount;

        public EnginKalkulatorowy()
        {
        }

        /// <summary>
        /// znizi pocitadlo a ak sa vynuluje tak prepocita stack
        /// </summary>
        /// <param name="force">resetne pauzove pocitadlo takze vynuti prepocet stacku</param>
        public virtual void Start(bool force)
        {
            if (force) this.ResetPauseCounter();
            if (this.pauseCount > 0) this.pauseCount--;
            if ((this.pauseCount != 0) || (this.calculating > 0)) return;
            var ea = new PropertyChangedEventArgs("Start");
            this.Calculate(ea);
        }

        /// <summary>
        /// pozastavi automaticky prepocet vzorcov, iba si ich uklada do stacku. pauzovanie je aditivne, takze pre 3 volania pause je nutne 3x zavolat start
        /// </summary>
        public void Pause()
        {
            this.pauseCount++;
        }

        /// <summary>
        /// reset pocitadla pauz (na 1)
        /// </summary>
        public void ResetPauseCounter()
        {
            this.pauseCount = 1;
        }

        /// <summary>
        /// prida propertu do enginu.
        /// ak neexistuje, tak ju vytvori, vytvori jej rodicov (ak ich treba)
        /// </summary>
        /// <param name="ekp"></param>
        /// <returns></returns>
        internal Property Add(EKProperty ekp, bool asInput)
        {
            var targetp = this.properties.FindOrCreate(ekp.PropertyPathHolder, ekp.PropertyPath);
            bool isNew = targetp.NeedConfigure;

            if (isNew) this.AddNewProperty(targetp, ekp.PropertyPathHolder, ekp.PropertyPath);
            else this.MarkAsInput(targetp);
            if (asInput) targetp.isInput = true;
            else targetp.isOutput = true;
            return targetp;
        }
        /// <summary>
        /// prida a nakonfiguruje vypocet pre EK
        /// </summary>
        /// <param name="equation">delegat funkcie obsahujuca vypocet</param>
        /// <param name="recalculate">prinuti engin prepocitat vzorec po pridani, resp po dokonceni aktualneho retazca vypoctu.
        /// vzorec by mal byt castou aktualne pocitaneho retazca
        /// engin odchytava vynimku pri prepocte, nech nedochne aplikacia ak nieje vypocet prisposobeny na vypecet pocas inicializacie EK</param>
        /// <param name="output">output properta (pre neoptimalizovany mod je nepotrebne)</param>
        /// <param name="inputs">vstupne property</param>
        public void Add(EKEquation equation, EKProperty output, params EKProperty[] inputs)
        {
            lock (this.equations)
            {
                Property prop;
                var inputPropsD = new List<Property>(inputs.Length);
                foreach (var ekp in inputs)
                {
                    if (!ekp.IsOK()) throw new NullReferenceException("propertyPathHolder or propertyName cannot be null");
                    prop = this.Add(ekp, true);
                    inputPropsD.Add(prop);
                }
                if (inputPropsD.Count < 1) return;
                var coords = this.equations.GetIndex(equation.Equation);
                if (coords is null || (coords.listIndex >= 0) || !output.IsOK()) return;
                var inputProps = inputPropsD.Distinct(new PropertyComparer(false));

                if (!output.IsOK()) throw new NullReferenceException("propertyPathHolder cannot be null");
                prop = this.Add(output, false);
                var outp = prop;

                var newEqu = new Equation(this, ++this.lastID, equation.Equation, equation.Priority, inputProps, outp);
                if (equation.Recalculate)
                {
                    if (!equation.Force)
                    {
                        foreach (var inputProp in newEqu.inputs)
                        {
                            if (!inputProp.IsAlive())
                            {
                                equation.Recalculate = false;
                                break;
                            }
                        }
                        equation.Recalculate = equation.Recalculate && newEqu.output.IsAlive();
                    }
                    if (equation.Recalculate)
                    {
                        newEqu.forceCalc = true;
                        this.NonOptimizedEquationChain.Add(newEqu);
                        if ((this.pauseCount == 0) && (this.calculating <= 0))
                        {
                            var ea = new PropertyChangedEventArgs("Start");
                            this.Calculate(ea);
                        }
                    }
                }

                //vlozim vzorec medzi zavislakov
                //nastavim needRecalc pre property ktore mozu ovplyvnovat moj vzorec
                foreach (var inputProp in inputProps)
                {
                    inputProp.Zavislaci.AddLast(newEqu);
                    prop = inputProp;
                    while (true)
                    {
                        prop = prop.OticMat;
                        if (prop is null) break;
                        prop.Zavislaci.AddLast(newEqu);
                    }
                }
                outp.ChleboDarcovia.AddLast(newEqu);
                prop = outp;
                while (true)
                {
                    prop = prop.OticMat;
                    if (prop is null) break;
                    prop.Zavislaci.AddLast(newEqu);
                }
                this.equations.Insert(coords, newEqu);
            }
        }
        /// <summary>
        /// odstrani vypocet z EK
        /// </summary>
        /// <param name="equation">delegat vypoctu</param>
        public void Remove(EquationFunc equation)
        {
            lock (this.equations)
            {
                var evl = this.equations.FindAll(equation);
                foreach (var item in evl) this.Remove(item);
            }
        }
        private void Remove(Equation equation)
        {
            equation.Dispose();

            Property p;
            Property oticmat;

            for (var i = 0; i < equation.inputs.Length; i++)
            {
                p = equation.inputs[i];
                while (p is object && !p.IsUsed())
                {
                    oticmat = p.OticMat;
                    this.properties.Remove(p);
                    p.Dispose();
                    p = oticmat;
                }
                equation.inputs[i] = null;
            }

            p = equation.output;
            while (p is object && !p.IsUsed())
            {
                oticmat = p.OticMat;
                this.properties.Remove(p);
                p.Dispose();
                p = oticmat;
            }
            this.equations.Remove(equation);
            this.NonOptimizedEquationChain.Remove(equation);
        }
        internal virtual void OnPropertyChanged(Property properta, PropertyChangedEventArgs ea)
        {
            if (properta is null) return;

            if ((this.calculating > 0) || (this.pauseCount != 0))
            {
                if (properta.isInput)
                {
                    //medzi zavislakmi su aj vzorce zavisle od deticiek tejto property. preto uz nemusim prechadzat jej detvaky
                    if (this.Verbose)
                    {
                        if (this.pauseCount != 0)
                        {
                            if ((this.VerboseMessages & EVerboseMessages.AddWhilePaused) != 0)
                                foreach (var v in properta.Zavislaci) Logger.Trace(this.VerbosePrefix + "awp: " + v.ToString());
                        }
                        else
                        {
                            if ((this.VerboseMessages & EVerboseMessages.AddWhileCalculating) != 0)
                                foreach (var v in properta.Zavislaci) Logger.Trace(this.VerbosePrefix + "awc: " + v.ToString());
                        }
                    }
                    this.NonOptimizedEquationChain.Add(properta.Zavislaci);
                }
                return;
            }

            this.NonOptimizedEquationChain.Clear();
            if (this.Verbose)
            {
                if ((this.VerboseMessages & EVerboseMessages.AddBeforeStart) != 0)
                    foreach (var v in properta.Zavislaci) Logger.Trace(VerbosePrefix + "abs: " + v.ToString());
            }
            this.NonOptimizedEquationChain.Add(properta.Zavislaci);
            //optimalizujem si jednu vrstvu (urobi mi to spravne poradie v prvej vrstve)
            //predpokladam ze sa mi zmeni vystup prvej vrstvy, tak si ulozim aj vzorce druhej vrstvy
            //foreach (Vzorec v in properta.Zavislaci) NonOptimizedNextVzorecChain.Add(v.output.Zavislaci);
            this.Calculate(ea);
        }

        internal void Calculate(PropertyChangedEventArgs ea)
        {
            this.calculating++;
            var ex = this.DoCalcNonOptim(ea);
            if (this.calculating > 0) this.calculating--;
            if (ex is object) ex.RethrowWithNoStackTraceLoss();
        }
        internal virtual Exception DoCalcNonOptim(PropertyChangedEventArgs ea)
        {
            this.StartCalculationNotify();
            while (this.NonOptimizedEquationChain.HaveNext())
            {
                var v = this.NonOptimizedEquationChain.GetNext();
                if (v is null) continue;
                if (this.Verbose)
                {
                    if ((this.VerboseMessages & EVerboseMessages.CalculatingNames) != 0)
                        Logger.Trace(this.VerbosePrefix + "cal: " + v.ToString());
                }
                var ex = v.Calculate(ea, null);
                if (ex is object)
                {
                    this.CalculationEnds();
                    return ex;
                }
            }
            this.CalculationEnds();
            return null;
        }

        internal void AskForErase(Equation equation)
        {
            lock (this.equationsToDispose)
            {
                this.equationsToDispose.AddLast(equation);
            }
            this.NonOptimizedEquationChain.Remove(equation);
        }

        protected void StartCalculationNotify()
        {
            this.ChainCalculationStart?.Invoke(this, new EventArgs());
        }
        protected void EndCalculationNotify()
        {
            this.ChainCalculationEnd?.Invoke(this, new EventArgs());
        }

        private void CalculationEnds()
        {
            this.EraseAskedEquations();
            this.EndCalculationNotify();
        }
        private void EraseAskedEquations()
        {
            lock (this.equationsToDispose)
            {
                lock (this.equations)
                {
                    foreach (var item in this.equationsToDispose)
                    {
                        this.Remove(item);
                    }
                    this.equationsToDispose.Clear();
                }
            }
        }
        private void AddNewProperty(Property targetp, IKalkulatorowaciaKlassa PropertyPathHolder, string PropertyPath)
        {
            Property parentp;
            Property p = null;
            foreach (var ipi in PropertyHelper.EnumeratePropertyInfos(PropertyPathHolder, PropertyPath))
            {
                //if (typeof(INotifyPropertyChanged).IsAssignableFrom(ipi.PropertyHolderType))
                //{
                //ak nemam vnorenu propertu, tak usetrim jedno hladanie
                if (ipi.PropertyPath.Equals(PropertyPath)) parentp = targetp;
                else parentp = this.properties.FindOrCreate(PropertyPathHolder, ipi.PropertyPath);

                if (parentp.NeedConfigure) parentp.Configure(this, PropertyPathHolder, ipi, ipi.PropertyName);
                if (p is object)
                {
                    p.isInput = true;
                    p.Detvaky.AddLast(parentp);
                    parentp.OticMat = p;
                }
                p = parentp;
                //}
            }
        }
        private void MarkAsInput(Property property)
        {
            while (true)
            {
                property = property.OticMat;
                if (property is null) break;
                property.isInput = true;
            }
        }
    }
}
