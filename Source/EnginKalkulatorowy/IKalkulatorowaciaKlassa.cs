﻿using System.ComponentModel;

namespace Kalkulatorowanie
{
    public interface IKalkulatorowaciaKlassa : INotifyPropertyChanged
    {
        ulong? EKObjectLptr { get; set; }
    }
}
