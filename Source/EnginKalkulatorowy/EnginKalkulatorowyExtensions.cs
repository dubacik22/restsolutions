﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Kalkulatorowanie
{
    public static class EnginKalkulatorowyExtensions
    {
        public static bool IsStarting(this EKPropertyChangedEventArgs ea)
        {
            return (ea.OrigEA.PropertyName == "Start");
        }
        public static bool AtFirstLayerOnStarting(this EKPropertyChangedEventArgs ea)
        {
            return (ea.OrigEA.PropertyName == "Start") && (ea.PropertyNames.Count == 0);
        }

        internal static void SetIsNextFired(this Property properta)
        {
            properta.isNextFired = true;
        }
        internal static void ResetIsNextFired(this Property properta)
        {
            properta.isNextFired = false;
        }
        /// <summary>
        /// zisti, ci je properta niekde pozuita, teda ci ma nahodeny isInput alebo isOutput, resp ci je parent nejakej pouzitej property
        /// </summary>
        /// <param name="properta"></param>
        /// <returns></returns>
        internal static bool IsUsed(this Property properta)
        {
            if (properta.isInput || properta.isOutput) return true;
            if (properta.Detvaky.Count < 1) return false;

            foreach (var p in properta.AllDetvakyEnumerable()) if (p.isInput || p.isOutput) return true;
            return false;
        }
        /// <summary>
        /// refreshne objekt v ktorom sa ma nachadat finalna properta
        /// </summary>
        /// <param name="property"></param>
        internal static void ReInitPropertyHolder(this Property property)
        {
            property.ReInitPropertyHolderInternal();
            if (property.Detvaky.Count < 1) return;
            property.ReinitDetvakyHolder();
        }

        /// <summary>
        /// iteruje cez rodica, dedka a starsej minulosti
        /// </summary>
        /// <param name="properta"></param>
        /// <returns></returns>
        internal static IEnumerable<Property> AllOticMatEnumerable(this Property properta)
        {
            Property p = properta;
            while (p.OticMat is object)
            {
                yield return p.OticMat;
                p = p.OticMat;
            }
        }
        /// <summary>
        /// iteruje cez vsetky detvaky, vnucatka a ostatnu haved
        /// </summary>
        /// <param name="properta"></param>
        /// <returns></returns>
        internal static IEnumerable<Property> AllDetvakyEnumerable(this Property properta)
        {
            Stack<PPEn> ppenStack = new Stack<PPEn>();
            Property p = properta;
            IEnumerator<Property> pe = p.Detvaky.GetEnumerator();
            bool gotNext = true;

            while (gotNext)
            {
                bool nextReady = pe.MoveNext();
                while (nextReady)
                {
                    Property actp = pe.Current;
                    yield return actp;
                    if (actp.Detvaky.Count < 1) nextReady = pe.MoveNext();
                    else
                    {
                        ppenStack.Push(new PPEn() { p = p, pe1 = pe });
                        p = actp;
                        pe = p.Detvaky.GetEnumerator();
                        break;
                    }
                }
                if (!nextReady)
                {
                    if (ppenStack.Count > 0)
                    {
                        PPEn oldppen = ppenStack.Pop();
                        p = oldppen.p;
                        pe = oldppen.pe1;
                    }
                    else gotNext = false;
                }
            }
        }
        /// <summary>
        /// iteruje cez priamy output a priame inputy vzorca (nie sekty ani detvaky ani oticmate)
        /// </summary>
        /// <param name="vzorec"></param>
        /// <returns></returns>
        internal static IEnumerable<Property> DirectPropertyEnumerable(this Equation vzorec)
        {
            yield return vzorec.output;
            foreach (Property p in vzorec.inputs) yield return p;
        }

        private static void ReinitDetvakyHolder(this Property properta)
        {
            IEnumerator<Property> pe = properta.Detvaky.GetEnumerator();
            bool gotNext = true;
            Stack<PPEn> pwenStack = new Stack<PPEn>();

            while (gotNext)
            {
                bool nextReady = pe.MoveNext();
                while (nextReady)
                {
                    Property actp = pe.Current;
                    actp.ReInitPropertyHolderInternal();
                    if (actp.Detvaky.Count < 1) nextReady = pe.MoveNext();
                    else
                    {
                        pwenStack.Push(new PPEn() { p = properta, pe1 = pe });
                        properta = actp;
                        pe = properta.Detvaky.GetEnumerator();
                        break;
                    }
                }
                if (!nextReady)
                {
                    if (pwenStack.Count > 0)
                    {
                        PPEn oldppen = pwenStack.Pop();
                        properta = oldppen.p;
                        pe = oldppen.pe1;
                    }
                    else gotNext = false;
                }
            }
        }

        /// <summary>
        /// Utility methods for exceptions.
        /// </summary>
        private static readonly FieldInfo remoteStackTraceString =
            typeof(Exception).GetField("_remoteStackTraceString", BindingFlags.Instance | BindingFlags.NonPublic) ??
            typeof(Exception).GetField("remote_stack_trace", BindingFlags.Instance | BindingFlags.NonPublic);

        /// <summary>
        /// Rethrows an exception object without losing the existing stack trace information.
        /// </summary>
        /// <param name="ex">The exception to re-throw.</param>
        /// <remarks>
        /// For more information on this technique, see
        /// http://www.dotnetjunkies.com/WebLog/chris.taylor/archive/2004/03/03/8353.aspx
        /// </remarks>
        public static void RethrowWithNoStackTraceLoss(this Exception ex)
        {
            remoteStackTraceString.SetValue(ex, ex.StackTrace + Environment.NewLine);
            throw ex;
        }
    }
}
