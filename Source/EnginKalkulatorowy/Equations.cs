﻿using System.Collections.Generic;

namespace Kalkulatorowanie
{
    internal class Equations
    {
        private readonly Dictionary<ulong, List<Equation>> InternalList = new Dictionary<ulong, List<Equation>>();
        
        /// <summary>
        /// iteruje cez list a najden index pod ktorym sa nachadza pozadovana properta.
        /// ak sa properta v liste nenachadza, tak sa vrati zaporny index + 1. (na ziskanie indexu kde sa ma properta vlozit, je potrebne urobit Abs(ret) - 1)
        /// ak sa vrati null, tak nieje mozne urcit polohu proerty (Item = null)
        /// </summary>
        /// <returns></returns>
        public Coords<Equation> GetIndex(EquationFunc Item)
        {
            if (Item is null) return null;
            ulong lptr = PropertyHelper.GetLptr(Item);
            return this.GetIndex(lptr, Item.Method.Name);
        }
        public Coords<Equation> GetIndex(Equation Item)
        {
            if (Item is null) return null;
            var lptr = PropertyHelper.GetLptr(Item);
            return this.GetIndex(lptr, Item.myEquationMethod.Name);
        }
        private Coords<Equation> GetIndex(ulong lptr, string methodName)
        {
            var coords = new Coords<Equation>
            {
                dictionaryKey = lptr
            };
            if (!this.InternalList.ContainsKey(coords.dictionaryKey))
            {
                coords.list = new List<Equation>();
                this.InternalList.Add(coords.dictionaryKey, coords.list);
            }
            else
            {
                coords.list = this.InternalList[coords.dictionaryKey];
            }
            var listIndex = this.GetIndex(coords.list, methodName, 0);
            if (listIndex.HasValue)
            {
                coords.listIndex = listIndex.Value;
                return coords;
            }
            return null;
        }
        private Coords<Equation> FindIndex(EquationFunc Item)
        {
            if (Item is null) return null;
            ulong lptr = PropertyHelper.GetLptr(Item);
            return this.FindIndex(lptr, Item.Method.Name);
        }
        private Coords<Equation> FindIndex(Equation Item)
        {
            if (Item is null) return null;
            var lptr = PropertyHelper.GetLptr(Item);
            return this.FindIndex(lptr, Item.myEquationMethod.Name);
        }
        private Coords<Equation> FindIndex(ulong lptr, string methodName)
        {
            var coords = new Coords<Equation>
            {
                dictionaryKey = lptr
            };
            if (!this.InternalList.ContainsKey(coords.dictionaryKey)) return null;
            else coords.list = this.InternalList[coords.dictionaryKey];
            var listIndex = this.GetIndex(coords.list, methodName, 0);
            if (listIndex.HasValue)
            {
                coords.listIndex = listIndex.Value;
                return coords;
            }
            return null;
        }
        private int? GetIndex(List<Equation> tlist, string methodName, int startingIndex)
        {
            int mi = startingIndex;
            int i = mi;
            int Mi = tlist.Count;

            if (Mi == mi) return -(mi + 1);
            int cr = tlist[i].myEquationMethod.Name.CompareTo(methodName);
            if (cr > 0) return -(mi + 1);
            if (cr == 0) return mi;
            if (Mi == 1) return -2;

            i = --Mi;
            cr = tlist[i].myEquationMethod.Name.CompareTo(methodName);
            if (cr < 0) return -Mi - 2;
            if (cr == 0) return Mi;

            bool doloop = true;
            while (doloop)
            {
                i = ((Mi - mi) / 2) + mi;
                cr = tlist[i].myEquationMethod.Name.CompareTo(methodName);
                if (cr > 0)
                {
                    Mi = i;
                    if ((Mi - mi) == 1) return -Mi - 1;
                }
                else if (cr < 0)
                {
                    mi = i;
                    if ((Mi - mi) == 1) return -Mi - 1;
                }
                if (cr == 0) return i;
            }
            return null;
        }

        public void Remove(Equation Item)
        {
            var coords = this.FindIndex(Item);
            if (coords is object)
            {
                if (coords.listIndex >= 0) coords.list.RemoveAt(coords.listIndex);
                if (coords.list.Count <= 0) this.InternalList.Remove(coords.dictionaryKey);
            }
        }
        public void Insert(Coords<Equation> coords, Equation item)
        {
            var index = coords.listIndex;
            if (index < 0) index = -coords.listIndex - 1;
            coords.list.Insert(index, item);
        }
        public IEnumerable<Equation> FindAll(EquationFunc equation)
        {
            var ret = new List<Equation>();
            int index = 0;
            var stop = false;
            var coords = this.FindIndex(equation);
            if (coords is null) return ret;
            var methodName = equation.Method.Name;
            
            do
            {
                var ItemIndex = this.GetIndex(coords.list, methodName, index);
                if (ItemIndex.HasValue && ItemIndex.Value >= 0)
                {
                    index = ItemIndex.Value;
                    ret.Add(coords.list[index]);
                    index++;
                    stop = false;
                }
                else stop = true;
            } while (!stop);
            return ret;
        }
    }
}
