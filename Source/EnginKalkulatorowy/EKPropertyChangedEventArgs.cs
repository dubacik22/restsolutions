﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;

namespace Kalkulatorowanie
{
    /// <summary>
    /// rovnica dostane odomna do vienka toto tu dole
    /// </summary>
    public class EKPropertyChangedEventArgs
    {
        /// <summary>
        /// eventArgs that has been sent by OnPropertyChanged of PropertyHolder
        /// </summary>
        public PropertyChangedEventArgs OrigEA;
        /// <summary>
        /// names of properties that has been change untill now
        /// </summary>
        public List<string> PropertyNames;
        /// <summary>
        /// synchronization context of outer thread (if it has been provided to constructor)
        /// </summary>
        public SynchronizationContext SynchronizationContext;
    }
}
