﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using Logging;
using System.Threading;
using System.Reflection;

namespace Kalkulatorowanie
{
    internal class Equation : IDisposable
    {
        private EnginKalkulatorowy EK;
        private readonly long id;
        private readonly List<Property> myInputChng = new List<Property>();

        //internal readonly EquationFunc myEquation;
        internal readonly WeakReference myEquationTarget;
        //in case of collecting propertyPathHolder, we still want to be comparable to other properties, therefore we need to save lptr of PropertyPathHolder
        internal readonly ulong lptr;
        internal readonly MethodInfo myEquationMethod;

        public bool forceCalc = false;
        public EquationPriority priority;
        public readonly Property[] inputs;
        public readonly Property[] oticMate;
        public readonly Property output;
        public long ID { get { return this.id; } }

        /// <summary>
        /// Searching in EquationsList is slow, because this list is unordered.
        /// to fasten the search, we directly add LinkedListNode into Equation, so we dont need to search anymore
        /// </summary>
        #region EquationsList stuff
        public LinkedListNode<Equation> EquationsListNode = null;

        #endregion

        public Equation(EnginKalkulatorowy ek, long ID, EquationFunc equation, EquationPriority priority, IEnumerable<Property> inputs, Property output)
        {
            this.EK = ek;
            this.id = ID;
            this.priority = priority;
            this.inputs = inputs.ToArray();
            this.oticMate = inputs.SelectMany((input) => input.AllOticMatEnumerable()).ToArray();
            this.output = output;
            this.myEquationTarget = new WeakReference(equation.Target);
            this.lptr = PropertyHelper.GetLptr(equation);
            this.myEquationMethod = equation.Method;
        }

        #region public
        public Exception Calculate(PropertyChangedEventArgs eventArguments, SynchronizationContext synchronizationContext)
        {
            //ak equation hodi vynimku ktora sa mozno v aplikacii odchyti, tak musim zabezpecit nech to nezablokuje EK
            //preto odchytam vynimku, upracem po sebe a neskor ju hodim zase. je to pomale, ale staci.
            Exception actExc = null;

            if (this.disposed) return actExc;
            var equationTarget = this.myEquationTarget.Target;
            if (equationTarget is null)//disposing though EK, to remove property that iare not needed
            {
                this.EK.AskForErase(this);
                return actExc;
            }
            List<string> pn = new List<string>(this.inputs.Length);

            var ll = this.GetMyChangedInput();

            foreach (var p in ll) pn.Add(p.Name);

            if ((pn.Count > 0) || forceCalc)
            {
                this.output.SetIsNextFired();
                EKPropertyChangedEventArgs ekea = new EKPropertyChangedEventArgs
                {
                    OrigEA = eventArguments,
                    SynchronizationContext = synchronizationContext,
                    PropertyNames = pn
                };
                try
                {
                    this.myEquationMethod.Invoke(equationTarget, new object[1] { ekea });
                    //myEquation(ekea);
                }
                catch (Exception ex)
                {
                    actExc = ex;
                    Logger.Fatal("error in equation " + ToString() + ": " + ex.ToString());
                }
                this.output.ResetIsNextFired();
            }
            lock (this.myInputChng)
            {
                this.myInputChng.Clear();
            }
            this.forceCalc = false;

            return actExc;
        }
        
        public void AddMyInputChanged(Property p)
        {
            lock (this.myInputChng)
            {
                this.myInputChng.Add(p);
            }
        }

        public int CompareTo(EquationFunc other)
        {
            return this.CompareTo(other.Target, other.Method);
        }
        public int CompareTo(Equation other)
        {
            return this.CompareTo(other.lptr, other.myEquationMethod);
        }


        public override bool Equals(object obj)
        {
            return Equals(obj as Equation);
        }
        public bool Equals(Equation other)
        {
            if (other is null) return false;
            return this.id == other.id;
        }
        public bool Equals(EquationFunc other)
        {
            if (other is null) return false;
            return ReferenceEquals(this.myEquationTarget.Target, other.Target) && this.myEquationMethod.Name.Equals(other.Method.Name); //myEquationMethod.Equals(other.Method);
        }
        public static bool Equals(Equation a, Equation b)
        {
            if (a is null) return b is null;
            return a.Equals(b);
        }

        public override int GetHashCode()
        {
            return this.id.GetHashCode();
        }

        //info o disposovani mam len preto, aby som sa nepocital ak uz nemam exsitovat
        bool disposed = false;
        public void Dispose()
        {
            if (this.disposed) return;
            this.disposed = true;

            //disposovat property mozem len ak sa uz nikde inde nenachadzaju. takze v engine
            foreach (Property p in this.inputs)
            {
                p.Zavislaci.Remove(this);
                if (p.isInput && (p.Zavislaci.Count == 0)) p.isInput = false;

                foreach (Property om in p.AllOticMatEnumerable())
                {
                    om.Zavislaci.Remove(this);
                    if (om.isInput && (om.Zavislaci.Count == 0)) om.isInput = false;
                }
            }
            this.output.ChleboDarcovia.Remove(this);
            this.output.isOutput = this.output.ChleboDarcovia.Count > 0;
            foreach (Property om in this.output.AllOticMatEnumerable())
            {
                om.Zavislaci.Remove(this);
                if (om.isInput && (om.Zavislaci.Count == 0)) om.isInput = false;
            }
            this.EK = null;
        }
        public override string ToString()
        {
            //return myEquationTarget.ToString() + "." + myEquationMethod.Name;
            return this.myEquationMethod.Name;
        }

        #endregion
        #region private
        private unsafe int CompareTo(Object otherTarget, MethodInfo otherMethod)
        {
            var equationTarget = this.myEquationTarget.Target;
            
            if (ReferenceEquals(otherTarget, equationTarget)) return this.myEquationMethod.Name.CompareTo(otherMethod.Name);
            if (otherTarget is null) return -1;
            if (equationTarget is null) return -1;

            return this.CompareTo(PropertyHelper.GetReferenceValue(otherTarget), otherMethod);
        }
        private unsafe int CompareTo(ulong otherLptr, MethodInfo otherMethod)
        {
            var xlptr = lptr;
            var ylptr = otherLptr;

            if (xlptr == ylptr) return this.myEquationMethod.Name.CompareTo(otherMethod.Name);
            if (xlptr > ylptr) return 1;
            return -1;
        }
        private IEnumerable<Property> GetMyChangedInput()
        {
            lock (this.myInputChng)
            {
                return this.myInputChng.Intersect(this.inputs).Concat(this.myInputChng.Intersect(this.oticMate)).Distinct().ToList();
            }
        }

        #endregion
    }
}
