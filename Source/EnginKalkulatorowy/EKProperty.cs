﻿namespace Kalkulatorowanie
{
    public struct EKProperty
    {
        internal IKalkulatorowaciaKlassa PropertyPathHolder;
        internal string PropertyPath;

        /// <summary>
        /// pomocna struktura pre inicializaciu EK
        /// </summary>
        /// <param name="PropertyPath">cesta.nazov</param>
        /// <param name="PropertyPathHolder">objekt v ktorom sa nachadza cesta.nazov</param>
        public EKProperty(string PropertyPath, IKalkulatorowaciaKlassa PropertyPathHolder)
        {
            this.PropertyPath = PropertyPath;
            this.PropertyPathHolder = PropertyPathHolder;
        }
        internal bool IsOK()
        {
            return !(this.PropertyPathHolder is null || string.IsNullOrEmpty(this.PropertyPath));
        }

        public override string ToString()
        {
            return this.PropertyPath;
        }
    }
}
