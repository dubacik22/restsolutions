﻿using System.Collections.Generic;

namespace Kalkulatorowanie
{
    internal class Properties
    {
        private readonly Dictionary<ulong, List<Property>> InternalList = new Dictionary<ulong, List<Property>>();

        /// <summary>
        /// najde pozadovanu propertu, popripade vytvori novu a ulozi ju. tuto propertu je potom nutne nakonfigurovat
        /// tuto metodu je mozne nahradit obycajnym findom a obycajnym add-om. teraz ale usetrime jedno iterovanie cez list
        /// </summary>
        /// <returns></returns>
        public Property FindOrCreate(IKalkulatorowaciaKlassa PropertyPathHolder, string PropertyName)
        {
            var coords = this.GetIndex(PropertyPathHolder, PropertyName);
            if (coords is null) return new Property(PropertyPathHolder, PropertyName);
            if (coords.listIndex >= 0) return coords.list[coords.listIndex];
            var np = new Property(PropertyPathHolder, PropertyName);
            coords.list.Insert(-coords.listIndex - 1, np);
            return np;
        }

        /// <summary>
        /// iteruje cez list a najden index pod ktorym sa nachadza pozadovana properta.
        /// ak sa properta v liste nenachadza, tak sa vrati zaporny index + 1. (na ziskanie indexu kde sa ma properta vlozit, je potrebne urobit Abs(ret) - 1)
        /// ak sa vrati null, tak nieje mozne urcit polohu proerty (Item = null)
        /// </summary>
        /// <returns></returns>
        private Coords<Property> GetIndex(Property Item)
        {
            if (Item is null) return null;
            var lptr = PropertyHelper.GetLptr(Item);
            return this.GetIndex(lptr, Item.PropertyPath);
        }
        private Coords<Property> GetIndex(IKalkulatorowaciaKlassa PropertyPathHolder, string PropertyPath)
        {
            if (PropertyPathHolder is null) return null;
            if (string.IsNullOrEmpty(PropertyPath)) return null;
            ulong lptr = PropertyHelper.GetLptr(PropertyPathHolder);
            return this.GetIndex(lptr, PropertyPath);
        }
        private Coords<Property> GetIndex(ulong lptr, string PropertyPath)
        {
            var coords = new Coords<Property>
            {
                dictionaryKey = lptr
            };
            if (!this.InternalList.ContainsKey(coords.dictionaryKey))
            {
                coords.list = new List<Property>();
                this.InternalList.Add(coords.dictionaryKey, coords.list);
            }
            else
            {
                coords.list = InternalList[coords.dictionaryKey];
            }
            var listIndex = this.GetIndex(coords.list, PropertyPath);
            if (listIndex.HasValue)
            {
                coords.listIndex = listIndex.Value;
                return coords;
            }
            return null;
        }
        private int? GetIndex(List<Property> tlist, string PropertyPath)
        {
            if (string.IsNullOrEmpty(PropertyPath)) return -1;

            int lowerIndex = 0;
            int i = lowerIndex;
            int upperIndex = tlist.Count;

            if (upperIndex == 0) return -1;
            int comparisonResult = tlist[i].PropertyPath.CompareTo(PropertyPath);
            if (comparisonResult > 0) return -1;
            if (comparisonResult == 0) return 0;
            if (upperIndex == 1) return -2;

            i = --upperIndex;
            comparisonResult = tlist[i].PropertyPath.CompareTo(PropertyPath);
            if (comparisonResult < 0) return -upperIndex - 2;
            if (comparisonResult == 0) return upperIndex;

            bool doloop = true;
            while (doloop)
            {
                i = ((upperIndex - lowerIndex) / 2) + lowerIndex;
                comparisonResult = tlist[i].PropertyPath.CompareTo(PropertyPath);
                if (comparisonResult > 0)
                {
                    upperIndex = i;
                    if ((upperIndex - lowerIndex) == 1) return -upperIndex - 1;
                }
                else if (comparisonResult < 0)
                {
                    lowerIndex = i;
                    if ((upperIndex - lowerIndex) == 1) return -upperIndex - 1;
                }
                if (comparisonResult == 0) return i;
            }
            return null;
        }

        public void Remove(Property property)
        {
            var coords = GetIndex(property);
            if(coords is object)
            {
                if (coords.listIndex >= 0) coords.list.RemoveAt(coords.listIndex);
                if (coords.list.Count <= 0) this.InternalList.Remove(coords.dictionaryKey);
            }
        }
    }
}
