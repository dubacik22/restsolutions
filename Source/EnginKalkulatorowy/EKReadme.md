library for solving the continuity of refreshes of calculations (calls) of methods when changing their input parameters.

The library has completed an ONLY non-optimized mode (it turned out to be faster).
In non-optimized mode, the calculations are run in the order it has been created internally during the initialization of the EK (the previously initialized formula is refreshed sooner).
In this mode, the EK waits for the onPropertyChanged event of the initialized objects, and if it finds a method that depends on the changed property, it will run it.
The EK creates a stack of methods that need to be run and tries to prevent the methods from running repeatedly within the possibility.
The EK if necessary to run a method that is already in the stack, moves this method inside the stack depending on the priority to the beginning of the stack, leaves it in place, or moves it to the end.

In the EK it is necessary to initialize methods using this method:
void Add(EKEquation equation, EKProperty output, EKProperty input1, EKProperty input2, EKProperty input3, ...)

EKEquation (see lower EKEquationReadme)
EKProperty (see lower EKPropertyReadme)

In the non-optimized mode, the EK does not use the output property, therefore it is possible to initialize even more complicated methods that change multiple output properties at once. Set the output property to anything:
public bool Dummy {get; set; }
void setChangeDummy ()
{
    var eq = new EKEquation (changeDummy, true);
    var oekp = new EKProperty ("Dummy", this);
    var p1ekp = new EKProperty ("p1", this);
    var p2ekp = new EKProperty ("O1.p2", this);
    this.EK.add (eq, oekp, isokekp); // we have force set, which can lead to an exception if O1==null and it is not treated in the method itself
}
void changeDummy (EKPropertyChangedEventArgs ea)
{
    LP1 = p1;
    LP2 = p1 + O1.p2; // <- this can cause a problem if O1 is not set during EK initialization
...
}
In this case, the EK triggers a changeDummy when changing p1 or O1 or p2. this method changes the various properties that the EK listens to. If a method is found that depends on the changed properties, the EK will run them.

EK inserts EKPropertyChangedEventArgs into running methods (see EKPropertyChangedEventArgsReadme)

EK can be paused and restarted:
EK.Pause(); - automatically pauses the recalculation of formulas, only stores them in the stack. pause is additive, so for 3 pause calls it is necessary to call start 3 times
EK.Start(force); - lowers the counter and if it resets, recalculates the stack. if force == true, the pause counter is reset, forcing the stack recalculation
!!! When an EK is created, this EK is automatically paused. So if it is initialized immediately after its creation, then all methods that have set "recalculate" (or "force") are not run, but are stored in the stack.
EK.PauseCount - getter of PauseCount
EK.ResetPauseCount() - reset of counter

if necessary, the method can be removed from configuration. this will remove listeners that are listening to the properties of this method.
void Remove(EquationFunc equation)

EK.Verbose sets the eloquence of the EK. If verbose == true, then the EK logs more messages about whether the property has just changed and what formula is just recalculated, etc .. it is logged using the logger library to the logs (traces) created by the application.
EK.VerboseMessages sets the messages I want to log in verbose mode

as for thread-safety:
should be safe.
The whole stack of methods is run by the thread that triggers the count.

EnginKalkulatorowyWThreading should run formulas in a special thread (but they count in a row, but the computing thread is no longer the thread that triggered the counting)
EnginKalkulatorowyWThreading needs synchronizationContext to the constructor. this is sent to the individual formulas via eventArgs
so far it has been created and thoroughly tested. so be careful


----------------------------------------------------------------------------------
EKEquationReadme:
EquationFunc Equation - is any method (it can also be private, but it must be visible within the whole class, so it must not be private in the 'base' class). it must be: void EquationFunc (EKPropertyChangedEventArgs ea)
EquationPriority Priority - determines the priority (low, mid, hi)
                            low: the method is inserted (moved) to the end of the stack when it needs to be recalculated
                            mid: the method is inserted at the end of the stack if necessary (if it is already there, it is left in its place)
                            hi: the method is inserted (moved) to the beginning of the stack when it needs to be recalculated

Recalculate - will try to recalculate the method if all input parameters are OK
Force - (if recalculate==true) recalculates the method even if the input parameters are not correct. under "not correct" is meant the case when it is in the property O1, for example. "O2.O3.P1" O2 or O3==null

constructors:
EKEquation (EquationFunc Equation) - low priority equation, Recalculate = false, Force = false
EKEquation (EquationFunc Equation, EquationPriority Priority) - Recalculate = false, Force = false
EKEquation (EquationFunc Equation, bool ForceRecalculate) - low priority equation, Recalculate = true
EKEquation (EquationFunc Equation, EquationPriority Priority, bool ForceRecalculate) - Recalculate = true

EK holds an object that contains a method in WeakReference


----------------------------------------------------------------------------------
EKPropertyReadme:
EKProperty is a structure designed for configuring the inputs and outputs of a method
EKProperty oekp = new EKProperty (String PropertyPath, IKalculatorowaciaKlassa PropertyPathHolder)
PropertyPathHolder cannot be null
PropertyPath can also be nested, such as: "holder.poperta". this means that the property is located in: PropertyPathHolder.InnerHolder.Poperty for a better understanding it is written as PropertyPathHolder. "InnerHolder.Poperty", or the above mentioned O1. "O2.P1"
In the case of a nested property, the EK listens to the change of the whole string chain of properties (if they are located in the IKalkulatorowaciaKlassa)
The EK listens even if the property is nested in the list (preferably ObservableCollection because it contains INotifyPropertyChanged). then it must be initialized as: "list.Item(2) ... Property".
The EK listens even if the property is nested in the dictionary (preferably IObservableMap because it contains INotifyDictionaryChanged). then it must be initialized as: "list.Item('PropertyKey') ... Property".


----------------------------------------------------------------------------------
EKPropertyChangedEventArgsReadme:
EKPropertyChangedEventArgs:

public PropertyChangedEventArgs origEA
original EventArgument of propery that triggered the recalculation (the method that was triggered indirectly (later by some result of former calculation, it is likely that it was triggered by several of its properties. therefore this EventArgument is not very interesting)

public List <string> PropertyNames
list of properties that caused the recalculation of this method (there is only a list of names of properties that can directly run this method)

public SynchronizationContext SynchronizationContext
synchronization context which is intended for setting the results of individual formulas.
is useful only in the case of a multithreaded EK

----------------------------------------------------------------------------------

update 10/10/2017:
modified Namespace to Kalkulatorowanie
The EK starts the recalculation in a separate thread.
The EK constructor needs a SynchronizationContent, which is then sent to the EKPropertyChangedEventArgs. With this (if I send the synchronizationContent of the main thread to the constructor), the methods that start the EC (already in a separate thread) can interfere with the main thread (if, for example, the UI property needs to be modified, etc.).
The whole EC should resist the concurrent approach (only public methods are modified).


update 10/10/2017:
added EnginKalkulatorowyWThreading
