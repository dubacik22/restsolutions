﻿namespace Kalkulatorowanie.PropertyPathParser
{
    public class PropertyPathPart
    {
        public string PathName;
        public object[] Indexes;
        public string WholePartName;
    }
}
