﻿using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Kalkulatorowanie.PropertyPathParser
{
    internal class Parser
    {
        #region Class data
        const char myPathPartDelimiterChar = '.';
        const char myArgumentDelimiterChar = ',';
        const char myQuoteSimpleChar = '\'';
        const char myQuoteDoubleChar = '"';
        const char myColArgumentStartChar = '(';
        const char myArrArgumentStartChar = '[';
        const char myColArgumentEndChar = ')';
        const char myArrArgumentEndChar = ']';

        #endregion
        #region Nested types
        private abstract class ParserState
        {
            public static string origString;
            protected static readonly Stack<ParserState> oldState = new Stack<ParserState>();
            public static readonly PathPartState PathPartState = new PathPartState();
            public static readonly ArgumentState ArgumentState = new ArgumentState();
            public static readonly StringArgumentState StringArgumentState = new StringArgumentState();

            public abstract ParserState AnyChar(char ch, ParserContext context);
            public abstract ParserState OnDot(char ch, ParserContext context);
            public abstract ParserState OnStartBracket(char ch, ParserContext context);
            public abstract ParserState OnComa(char ch, ParserContext context);
            public abstract ParserState OnEndBracket(char ch, ParserContext context);
            public abstract ParserState OnQuote(char ch, ParserContext context);
        }
        private class PathPartState : ParserState
        {
            public override ParserState AnyChar(char ch, ParserContext context)
            {
                context.AddCharToWholePathPart(ch);
                context.AddCharToPathPart(ch);
                ParserState.oldState.Push(this);
                return ParserState.PathPartState;
            }
            public override ParserState OnDot(char ch, ParserContext context)
            {
                context.NextPathPart();
                ParserState.oldState.Push(this);
                return ParserState.PathPartState;
            }
            public override ParserState OnStartBracket(char ch, ParserContext context)
            {
                context.AddCharToWholePathPart(ch);
                ParserState.oldState.Push(this);
                return ParserState.ArgumentState;
            }
            public override ParserState OnComa(char ch, ParserContext context)
            {
                context.AddCharToWholePathPart(ch);
                context.NextArgument();
                ParserState.oldState.Push(this);
                return ParserState.ArgumentState;
            }
            public override ParserState OnEndBracket(char ch, ParserContext context)
            {
                throw new InvalidDataException("in path: " + ParserState.origString + " there is unexpected bracket");
            }
            public override ParserState OnQuote(char ch, ParserContext context)
            {
                throw new InvalidDataException("in path: " + ParserState.origString + " there is unexpected quote");
            }
        }
        private class ArgumentState : ParserState
        {
            public override ParserState AnyChar(char ch, ParserContext context)
            {
                context.AddCharToWholePathPart(ch);
                context.AddCharToArgument(ch);
                return this;
            }
            public override ParserState OnDot(char ch, ParserContext context)
            {
                throw new InvalidDataException("there is dot in argument in path: " + ParserState.origString);
            }
            public override ParserState OnStartBracket(char ch, ParserContext context)
            {
                throw new InvalidDataException("there is twoo brackets immediately behind each other in path: " + ParserState.origString);
            }
            public override ParserState OnComa(char ch, ParserContext context)
            {
                context.AddCharToWholePathPart(ch);
                context.NextArgument();
                return this;
            }
            public override ParserState OnEndBracket(char ch, ParserContext context)
            {
                context.AddCharToWholePathPart(ch);
                context.NextArgument();
                return ParserState.oldState.Pop();
            }
            public override ParserState OnQuote(char ch, ParserContext context)
            {
                context.AddCharToWholePathPart(ch);
                context.ArgumentType(true);
                ParserState.oldState.Push(this);
                return ParserState.StringArgumentState;
            }
        }
        private class StringArgumentState : ArgumentState
        {
            public override ParserState OnDot(char ch, ParserContext context)
            {
                this.AnyChar(ch, context);
                return this;
            }
            public override ParserState OnStartBracket(char ch, ParserContext context)
            {
                this.AnyChar(ch, context);
                return this;
            }
            public override ParserState OnComa(char ch, ParserContext context)
            {
                this.AnyChar(ch, context);
                return this;
            }
            public override ParserState OnEndBracket(char ch, ParserContext context)
            {
                this.AnyChar(ch, context);
                return this;
            }
            public override ParserState OnQuote(char ch, ParserContext context)
            {
                context.AddCharToWholePathPart(ch);
                return ParserState.oldState.Pop();
            }
        }

        private class ParserContext
        {
            private readonly StringBuilder wholePathPartBuilder = new StringBuilder();
            private readonly StringBuilder pathPartBuilder = new StringBuilder();
            private readonly StringBuilder argumentBuilder = new StringBuilder();
            private bool isStringArgument;
            private readonly LinkedList<object> arguments = new LinkedList<object>();
            private readonly LinkedList<PropertyPathPart> currentParts = new LinkedList<PropertyPathPart>();

            public void AddCharToWholePathPart(char ch)
            {
                this.wholePathPartBuilder.Append(ch);
            }
            public void AddCharToPathPart(char ch)
            {
                this.pathPartBuilder.Append(ch);
            }
            public void NextPathPart()
            {
                this.currentParts.AddLast(new PropertyPathPart()
                {
                    WholePartName = this.wholePathPartBuilder.ToString(),
                    PathName = this.pathPartBuilder.ToString(),
                    Indexes = this.arguments.ToArray()
                });
                this.wholePathPartBuilder.Clear();
                this.pathPartBuilder.Clear();
                this.arguments.Clear();
            }
            public void ArgumentType(bool stringType)
            {
                this.isStringArgument = stringType;
            }
            public void AddCharToArgument(char ch)
            {
                this.argumentBuilder.Append(ch);
            }
            public void NextArgument()
            {
                var argumentStr = this.argumentBuilder.ToString();
                if (this.isStringArgument)
                {
                    this.arguments.AddLast(argumentStr);
                    this.isStringArgument = false;
                    this.argumentBuilder.Clear();
                    return;
                }
                if (!int.TryParse(argumentStr.Trim(), out int i)) i = 0;
                this.arguments.AddLast(i);
                this.argumentBuilder.Clear();
            }
            public LinkedList<PropertyPathPart> GetAllParts()
            {
                return this.currentParts;
            }
        }

        #endregion
        public LinkedList<PropertyPathPart> Parse(string text)
        {
            var context = new ParserContext();
            ParserState.origString = text;
            var currentState = ParserState.PathPartState as ParserState;
            foreach (var ch in text) currentState = this.GetNextState(ch, currentState, context);
            currentState.OnDot('.', context);
            return context.GetAllParts();
        }
        private ParserState GetNextState(char ch, ParserState currentState, ParserContext context)
        {
            switch (ch)
            {
                case Parser.myPathPartDelimiterChar: return currentState.OnDot(ch, context);
                case Parser.myArgumentDelimiterChar: return currentState.OnComa(ch, context);
                case Parser.myColArgumentStartChar: return currentState.OnStartBracket(ch, context);
                case Parser.myArrArgumentStartChar: return currentState.OnStartBracket(ch, context);
                case Parser.myColArgumentEndChar: return currentState.OnEndBracket(ch, context);
                case Parser.myArrArgumentEndChar: return currentState.OnEndBracket(ch, context);
                case Parser.myQuoteSimpleChar: return currentState.OnQuote(ch, context);
                case Parser.myQuoteDoubleChar: return currentState.OnEndBracket(ch, context);
                default: return currentState.AnyChar(ch, context);
            }
        }
    }
}