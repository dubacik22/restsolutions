﻿using System.Collections.Generic;

namespace Kalkulatorowanie
{
    /// <summary>
    /// there is just not enough functionality in System.Collections.Concurrent collections, therefore I need to implement something on our own
    /// </summary>
    internal class CalculatingEquationsList
    {
        #region fields
        private readonly LinkedList<Equation> InternalList = new LinkedList<Equation>();

        #endregion
        #region public
        public void Add(IEnumerable<Equation> equations)
        {
            foreach (var equation in equations) Add(equation);
        }
        public void Add(Equation equation)
        {
            lock (this.InternalList)
            {
                switch (equation.priority)
                {
                    case EquationPriority.low:
                        this.MoveOrAddToEndInternal(equation);
                        break;
                    case EquationPriority.mid:
                        this.AddIfIsNotInteral(equation);
                        break;
                    case EquationPriority.hi:
                        this.MoveOrAddToFrontInternal(equation);
                        break;
                }
            }
        }
        public bool HaveNext()
        {
            lock (this.InternalList)
            {
                return this.InternalList.Count > 0;
            }
        }
        public Equation GetNext()
        {
            lock (this.InternalList)
            {
                var ret = this.InternalList.First?.Value;
                if (ret is null) return null;
                this.InternalList.RemoveFirst();
                return ret;
            }
        }
        public void Clear()
        {
            lock (this.InternalList)
            {
                this.InternalList.Clear();
            }
        }
        public void Remove(Equation item)
        {
            if (!CalculatingEquationsList.HaveEquationsListNode(item)) return;
            if (CalculatingEquationsList.IsEquationInList(item)) this.Remove(item.EquationsListNode);
        }

        #endregion
        #region private
        private static bool HaveEquationsListNode(Equation equation)
        {
            if (equation.EquationsListNode is null) return false;
            return true;
        }
        private static bool IsEquationInList(Equation equation)
        {
            if (equation.EquationsListNode.List is null) return false;
            return true;
        }
        private void Remove(LinkedListNode<Equation> item)
        {
            lock (this.InternalList)
            {
                this.InternalList.Remove(item);
            }
        }
        private void MoveOrAddToFrontInternal(Equation equation)
        {
            if (!CalculatingEquationsList.HaveEquationsListNode(equation))
            {
                equation.EquationsListNode = this.InternalList.AddFirst(equation);
                return;
            }
            if (CalculatingEquationsList.IsEquationInList(equation)) this.InternalList.Remove(equation.EquationsListNode);
            this.InternalList.AddFirst(equation.EquationsListNode);
        }
        private void AddIfIsNotInteral(Equation equation)
        {
            if (!CalculatingEquationsList.HaveEquationsListNode(equation))
            {
                equation.EquationsListNode = this.InternalList.AddLast(equation);
                return;
            }
            if (!CalculatingEquationsList.IsEquationInList(equation)) this.InternalList.AddLast(equation.EquationsListNode);
        }
        private void MoveOrAddToEndInternal(Equation equation)
        {
            if (!CalculatingEquationsList.HaveEquationsListNode(equation))
            {
                equation.EquationsListNode = this.InternalList.AddLast(equation);
                return;
            }
            if (CalculatingEquationsList.IsEquationInList(equation)) this.InternalList.Remove(equation.EquationsListNode);
            this.InternalList.AddLast(equation.EquationsListNode);
        }

        #endregion
    }
}
