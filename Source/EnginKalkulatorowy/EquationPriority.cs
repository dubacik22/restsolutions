﻿namespace Kalkulatorowanie
{
    /// <summary>
    /// priority of calculation of equations
    /// </summary>
    public enum EquationPriority
    {
        /// <summary>
        /// equations will be added or moved to the end of the stack
        /// </summary>
        low,
        /// <summary>
        /// equation bill be added to the end of the stack. if the equation already exists in the stack, the equation will remain on its position
        /// </summary>
        mid,
        /// <summary>
        /// equation will be added or moved to the beginning of the stack
        /// </summary>
        hi
    }
}
