﻿using System.Collections.Generic;

namespace Kalkulatorowanie
{
    internal class PropertyComparer : IEqualityComparer<Property>, IComparer<Property>
    {
        private readonly bool onlyReference;

        public PropertyComparer(bool onlyReference)
        {
            this.onlyReference = onlyReference;
        }

        public int Compare(Property x, Property y)
        {
            if (ReferenceEquals(x, y)) return 0;
            if (x is null) return -1;
            return x.CompareTo(y);
        }

        public bool Equals(Property a, Property b)
        {
            return Property.Equals(a, b, this.onlyReference);
        }
        // If Equals() returns true for a pair of objects, 
        // GetHashCode must return the same value for these objects. 
        public int GetHashCode(Property p)
        {
            // Check whether the object is null. 
            if (p is null) return 0;
            // Calculate the hash code for the Vzorec. 
            return p.GetHashCode();
        }

    }
}
