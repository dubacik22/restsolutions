EKPropertyChangedEventArgs:

public PropertyChangedEventArgs origEA
povodny EventArgument property ktora spustila prepocet (pri metode ktora bola spustena nepriamo je pravdepodobne, ze bola spustena viacerimi svojimi propertami. preto tento EventArgument nieje az taky zaujimavy)

public List<string> PropertyNames
zoznam properiet ktore sposobili prepocet tejto metody (nachadza sa iba zoznam properiet ktore mozu priamo spustit tuto metodu)

public SynchronizationContext SynchronizationContext
synchronization context ktory je urceny pre nastavovanie vysledkov jednotlivych vzorcov.
je uzitocny iba v pripade multithreadoveho EK