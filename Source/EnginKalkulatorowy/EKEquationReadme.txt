EquationFunc Equation - je lubovolna metoda (moze byt aj private, ale musi byt viditelna v ramci celej triedy, takze nesmie byt private v 'base' triede). jej tvar musi byt: void EquationFunc(EKPropertyChangedEventArgs ea)
EquationPriority Priority - urcuje prioritu (low, mid, hi)
                            low: metoda je pri potrebe prepocitania vlozena (presunuta) na koniec stacku
							mid: metoda je v pripade potreby vlozena na koniec stacku (ak tam uz je, tak je ponechana na jej mieste)
							hi: metoda je pri potrebe prepocitania vlozena (presunuta) na zaciatok stacku

Recalculate - posnazi sa prepocitat metodu ak ma vsetky vstupne parametre v poriadku
Force - (ak je recalculate == true) prepocita metodu aj ked nema vstupne parametre v poriadku. pod tymto "nie v poriadku" sa chape pripad, ked je napr v properte O1."O2.O3.P1" O2 alebo O3 == null

konstruktory:
EKEquation(EquationFunc Equation) - low priority equation, Recalculate = false, Force = false
EKEquation(EquationFunc Equation, EquationPriority Priority) -  Recalculate = false, Force = false
EKEquation(EquationFunc Equation, bool ForceRecalculate) - low priority equation, Recalculate = true
EKEquation(EquationFunc Equation, EquationPriority Priority, bool ForceRecalculate) - Recalculate = true

EK drzi objekt ktory obsahuje metodu vo WeakReference