﻿namespace Kalkulatorowanie
{
    public struct EKEquation
    {
        internal EquationFunc Equation;
        internal EquationPriority Priority;
        internal bool Recalculate;
        internal bool Force;

        /// <summary>
        /// create equation Configuration (equation will not be recalculated on start and is lowPriority)
        /// </summary>
        /// <param name="Equation">equation function</param>
        public EKEquation(EquationFunc Equation)
        {
            this.Equation = Equation;
            this.Priority = EquationPriority.low;
            this.Recalculate = false;
            this.Force = false;
        }

        /// <summary>
        /// create equation Configuration (equation will not be recalculated on start)
        /// </summary>
        /// <param name="Equation">equation function</param>
        /// <param name="Priority">priority of calculation</param>
        public EKEquation(EquationFunc Equation, EquationPriority Priority)
        {
            this.Equation = Equation;
            this.Priority = Priority;
            this.Recalculate = false;
            this.Force = false;
        }

        /// <summary>
        /// create equation Configuration that can be recalculated on start (equation is lowPriority)
        /// </summary>
        /// <param name="Equation">equation function</param>
        /// <param name="ForceRecalculate">forcing of recalculation even if there are not all of the input properties OK</param>
        public EKEquation(EquationFunc Equation, bool ForceRecalculate)
        {
            this.Equation = Equation;
            this.Priority = EquationPriority.low;
            this.Recalculate = true;
            this.Force = ForceRecalculate;
        }
        
        /// <summary>
        /// create equation Configuration that can be recalculated on start
        /// </summary>
        /// <param name="Equation">equation function</param>
        /// <param name="Priority">priority of calculation</param>
        /// <param name="ForceRecalculate">forcing of recalculation even if there are not all of the input properties OK</param>
        public EKEquation(EquationFunc Equation, EquationPriority Priority, bool ForceRecalculate)
        {
            this.Equation = Equation;
            this.Priority = Priority;
            this.Recalculate = true;
            this.Force = ForceRecalculate;
        }
    }
    public delegate void EquationFunc(EKPropertyChangedEventArgs ea);
}
