﻿using System.Collections.Generic;

namespace Kalkulatorowanie
{
    internal class Coords<T>
    {
        public ulong dictionaryKey;
        public int listIndex;
        public List<T> list;
    }
}
