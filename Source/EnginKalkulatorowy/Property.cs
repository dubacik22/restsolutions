﻿using ObservableDictionary;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Kalkulatorowanie
{
    internal class Property : IDisposable
    {
        EnginKalkulatorowy EK;
        //INotifyPropertyChanged propertyHolder;
        WeakReference propertyHolder;
        public INotifyPropertyChanged PropertyHolder
        {
            get => (INotifyPropertyChanged)this.propertyHolder?.Target;
            private set => this.propertyHolder = new WeakReference(value);
        }
        //INotifyPropertyChanged propertyPathHolder;
        WeakReference propertyPathHolder;
        public IKalkulatorowaciaKlassa PropertyPathHolder
        {
            get => (IKalkulatorowaciaKlassa)this.propertyPathHolder?.Target;
            private set
            {
                this.propertyPathHolder = new WeakReference(value);
                if (value is null) Lptr = 0;
                else this.Lptr = PropertyHelper.GetLptr(value);
            }
        }

        public ulong Lptr { get; private set; }
        public string PropertyPath { get; private set; }
        private string _propertyName; //chceme len Item[]
        public string Name { get; private set; }

        //public bool changed = false;
        public bool isInput = false;
        public bool isOutput = false;
        public bool isNextFired = false;
        public bool? isNumberIndexed;
        public object[] propertyIndex;
        public void RefreshIsInput()
        {
            this.isInput = false;
            if (this.Zavislaci.Count > 0)
            {
                this.isInput = true;
                return;
            }
            if (this.Detvaky.Count > 0)
            {
                this.isInput = true;
                return;
            }
        }
        /// <summary>
        /// vzorce ktore ovplyvnuju tuto propertu
        /// </summary>
        public LinkedList<Equation> ChleboDarcovia { get; set; } = new LinkedList<Equation>();
        /// <summary>
        /// retazec vzorcov ktore priamo zavisia od aktualnej property (su tam aj vzorce ktore zavisia od detvakov tejto property)
        /// </summary>
        public LinkedList<Equation> Zavislaci { get; set; } = new LinkedList<Equation>();
        /// <summary>
        /// ak je properta definovana propertyPathHolderom=OBJ a propertypath="cesta.ku.properte"
        /// tak sa vytvoria property:
        /// P1 propertyHolder=OBJ, propertyName="cesta", detvaky=P2,P3
        /// P2 propertyHolder=OBJ.cesta, propertyName="ku", detvaky=P3
        /// P3 propertyHolder=OBJ.cesta.ku, propertyName="properte", detvaky=nic
        /// detvaky je zoznam properiet ktore sa nachadzaju v objekte aktualnej property
        /// v pripade zmeny aktualnej property sa musia refreshnut propertyholdre detvakov
        /// </summary>
        public LinkedList<Property> Detvaky { get; set; } = new LinkedList<Property>();
        /// <summary>
        /// je to opak ako detvaky. v pripade, ze som vnorena properta,
        /// tak sa tu nachadza referencia na objekt ktory je definovany ako rodicovska properta
        /// </summary>
        public Property OticMat { get; set; }

        public bool NeedConfigure { get; private set; } = true;

        public Property(IKalkulatorowaciaKlassa propertyPathHolder, string propertyPath)
        {
            this.PropertyPathHolder = propertyPathHolder;
            this.PropertyPath = propertyPath;
        }
        public void Configure(EnginKalkulatorowy ek, IKalkulatorowaciaKlassa propertyPathHolder, IndexedPropertyInfo ipi, string propertyName)
        {
            this.EK = ek;
            this.PropertyPathHolder = propertyPathHolder;
            this.PropertyPath = ipi.PropertyPath;
            this.PropertyHolder = ipi.PropertyHolder as INotifyPropertyChanged;
            this.isNumberIndexed = ipi.NumberIndex;
            this.propertyIndex = ipi.PropertyIndex;
            this.Name = propertyName;

            this.RefreshPropertyName();

            if (this.PropertyPathHolder is null) throw new ArgumentNullException("PropertyPathHolder", "parameter cannot be null");
            if (string.IsNullOrEmpty(this.PropertyPath)) throw new ArgumentNullException("PropertyPath", "parameter cannot be empty");
            //PropertyHolder moze byt null, lebo sa v runtime obnovuje
            if (string.IsNullOrEmpty(this.Name)) throw new ArgumentNullException("PropertyName", "parameter cannot be empty");
            this.AddListeners(this.PropertyHolder, this.propertyIndex);

            this.NeedConfigure = false;
        }
        
        internal void ReInitPropertyHolderInternal()
        {
            var propertyPathHolder = this.PropertyPathHolder;
            if (propertyPathHolder is null)
            {
                //EK.AskForErase(this);
                return;
            }

            IndexedPropertyInfo ipi;
            if (this.OticMat is object)
            {
                var oticMatPropertyHolder = this.OticMat.PropertyHolder;
                if (oticMatPropertyHolder is null) ipi = PropertyHelper.CreatePropertyInfo(propertyPathHolder, this.PropertyPath);
                else ipi = PropertyHelper.CreatePropertyInfo(oticMatPropertyHolder, this.OticMat.Name + "." + this.Name);  
            }
            else ipi = PropertyHelper.CreatePropertyInfo(propertyPathHolder, this.PropertyPath);

            this.PropertyHolder = this.SetHolderAndOnPropertyChanged(this.PropertyHolder, ipi);
        }

        /// <summary>
        /// zistim, ci je tato properta funkcna a ci moze prijat propertyChangedEvent
        /// </summary>
        /// <returns></returns>
        public bool IsAlive()
        {
            return this.PropertyPathHolder is object && this.PropertyHolder is object;
        }

        public int CompareTo(IKalkulatorowaciaKlassa otherPropertyPathHolder, string otherPropertyPath)
        {
            var propertyPathHolder = this.PropertyPathHolder;
            if (ReferenceEquals(otherPropertyPathHolder, propertyPathHolder)) return this.PropertyPath.CompareTo(otherPropertyPath);
            if (otherPropertyPathHolder is null) return -1;
            return CompareTo(PropertyHelper.GetLptr(otherPropertyPathHolder), otherPropertyPath);
        }
        public int CompareTo(Property other)
        {
            if (other is null) return -1;
            if (ReferenceEquals(this, other)) return 0;
            return this.CompareTo(other.Lptr, other.PropertyPath);
        }
        public int CompareTo(ulong otherLptr, string otherPropertyPath)
        {
            var xlptr = this.Lptr;
            var ylptr = otherLptr;
            if (xlptr == ylptr) return this.PropertyPath.CompareTo(otherPropertyPath);
            if (xlptr > ylptr) return 1;
            return -1;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as Property);
        }
        public bool Equals(Property other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;
            if (!ReferenceEquals(other.PropertyPathHolder, this.PropertyPathHolder)) return false;
            if (!this.PropertyPath.Equals(other.PropertyPath)) return false;
            return true;
        }
        public bool Equals(INotifyPropertyChanged otherPropertyPathHolder, string otherPropertyPath)
        {
            if (otherPropertyPathHolder is null) return false;
            if (string.IsNullOrEmpty(otherPropertyPath)) return false;
            if (!ReferenceEquals(this.PropertyPathHolder, otherPropertyPathHolder)) return false;
            if (!this.PropertyPath.Equals(otherPropertyPath)) return false;
            return true;
        }
        /// <summary>
        /// kontroluje zhodu drzanej property, takze zhodu propertyHoldra a nazvu property
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool PEquals(Property other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return false; //! pozor. ak som rovnaky objekt, tak nemam zhodu
            return this.PEquals(other.PropertyHolder, other.Name);
        }
        /// <summary>
        /// kontroluje zhodu drzanej property, takze zhodu propertyHoldra a nazvu property
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool PEquals(INotifyPropertyChanged otherPropertyHolder, string otherPropertyName)
        {
            if (string.IsNullOrEmpty(otherPropertyName)) return false;
            if (string.IsNullOrEmpty(Name)) return false;

            var propertyHolder = this.PropertyHolder;
            if (propertyHolder is null) return false; //ak su obidva null, tiez vraciam false
            if (otherPropertyHolder is null) return false; //ak su obidva null, tiez vraciam false
            if (!ReferenceEquals(propertyHolder, otherPropertyHolder)) return false;
            if (!this.Name.Equals(otherPropertyName)) return false;
            return true;
        }
        public static bool Equals(Property a, Property b, bool onlyReference = false)
        {
            if (a is null) return b is null;
            if (onlyReference) return ReferenceEquals(a, b);
            return a.Equals(b);
        }
        public override int GetHashCode()
        {
            var propertyPathHolder = this.PropertyPathHolder;
            if (propertyPathHolder is object) return this.PropertyPathHolder.GetHashCode() + this.PropertyPath.GetHashCode();
            return this.PropertyPath.GetHashCode();
        }

        bool disposed = false;
        public void Dispose()
        {
            //kedze tu disposujem svoje deti, tak zamedzim moznej inf.slucke ak by som mal naprt inicializovane deti
            if (this.disposed) return;
            this.disposed = true;

            this.Zavislaci.Clear();
            this.RemoveAllListeners(this.PropertyHolder);
            this.PropertyHolder = null;
            this.PropertyPathHolder = null;
            foreach (var p in this.Detvaky) p.Dispose(); // rusim aj moje detvaky. preto by som mal spustat tento dispose len ak naozaj chcem
            this.Detvaky.Clear();
            if (this.OticMat is object)
            {
                if (!this.OticMat.disposed) this.OticMat.Detvaky.Remove(this);
                this.OticMat = null;
            }
            this.EK = null;
        }
        public override string ToString()
        {
            return this.PropertyPath + "{" + this.Zavislaci.Count + "}";
        }

        private INotifyPropertyChanged SetHolderAndOnPropertyChanged(INotifyPropertyChanged propertyHolder, IndexedPropertyInfo ipi)
        {
            if (propertyHolder is null && ipi.PropertyHolder is null) return null;
            if (propertyHolder is object && propertyHolder.Equals(ipi.PropertyHolder)) return propertyHolder;
            this.RemoveListeners(propertyHolder, ipi.PropertyIndex);
            propertyHolder = ipi.PropertyHolder as INotifyPropertyChanged;
            if (propertyHolder is null) return propertyHolder;
            this.AddListeners(propertyHolder, ipi.PropertyIndex);
            this.Changed();
            return propertyHolder;
        }
        private void RemoveListeners(INotifyPropertyChanged propertyHolder, object[] propertyIndex)
        {
            if (propertyHolder is null) return;
            if ((propertyHolder is INotifyDictionaryChanged observableDictionary) && propertyIndex is object && (propertyIndex.Length > 0))
            {
                observableDictionary.DictionaryChanged -= this.ObservableDictionaryChanged;
            }
            else if ((propertyHolder is INotifyCollectionChanged observableCollection) && propertyIndex is object && (propertyIndex.Length > 0))
            {
                observableCollection.CollectionChanged -= this.ObservableCollectionChanged;
            }
            else
            {
                propertyHolder.PropertyChanged -= this.OnPropertyChanged;
            }
        }
        private void RemoveAllListeners(INotifyPropertyChanged propertyHolder)
        {
            if (propertyHolder is null) return;
            if (propertyHolder is INotifyDictionaryChanged observableDictionary)
            {
                observableDictionary.DictionaryChanged -= this.ObservableDictionaryChanged;
            }
            if (propertyHolder is INotifyCollectionChanged observableCollection)
            {
                observableCollection.CollectionChanged -= this.ObservableCollectionChanged;
            }
            propertyHolder.PropertyChanged -= this.OnPropertyChanged;
        }
        private void AddListeners(INotifyPropertyChanged propertyHolder, object[] propertyIndex)
        {
            if (propertyHolder is null) return;
            if ((propertyHolder is INotifyDictionaryChanged observableDictionary) && propertyIndex is object && (propertyIndex.Length > 0))
            {
                observableDictionary.DictionaryChanged += this.ObservableDictionaryChanged;
            }
            else if ((propertyHolder is INotifyCollectionChanged observableCollection) && propertyIndex is object && (propertyIndex.Length > 0))
            {
                observableCollection.CollectionChanged += this.ObservableCollectionChanged;
            }
            else
            {
                propertyHolder.PropertyChanged += this.OnPropertyChanged;
            }
        }
        private void ObservableCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var myIndex = this.propertyIndex?[0] as int?;
            if (!myIndex.HasValue) return;
            if (e.NewStartingIndex != myIndex.Value) return;
            this.Changed();
            foreach (var p in this.Detvaky) EnginKalkulatorowyExtensions.ReInitPropertyHolder(p);
            this.EK?.OnPropertyChanged(this, null);
        }

        private void ObservableDictionaryChanged(object sender, NotifyDictionaryChangedEventArgs e)
        {
            if (!(this.propertyIndex?[0] is string myIndex)) return;
            if (!(e.AddedKey is string key)) return;
            if (key != myIndex) return;
            this.Changed();
            foreach (var p in this.Detvaky) EnginKalkulatorowyExtensions.ReInitPropertyHolder(p);
            this.EK?.OnPropertyChanged(this, null);
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!this._propertyName.Equals(e.PropertyName)) return;
            this.Changed();
            foreach (var p in this.Detvaky) EnginKalkulatorowyExtensions.ReInitPropertyHolder(p);
            this.EK?.OnPropertyChanged(this, e);
        }
        private void RefreshPropertyName()
        {
            int j = this.Name.IndexOf('(');
            if (j < 0) j = this.Name.IndexOf('[');
            if (j >= 0) this._propertyName = this.Name.Substring(0, j) + "[]";
            else this._propertyName = this.Name;
        }
        private void Changed()
        {
            foreach (Equation v in this.Zavislaci) v.AddMyInputChanged(this);
        }
    }
}
