﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using Logging;

namespace Kalkulatorowanie
{
    public class EnginKalkulatorowyWThreading : EnginKalkulatorowy
    {
        private readonly SynchronizationContext synchroContext = null;
        public int CalcTimeoutToVerbose = -1;
        /// <summary>
        /// have to be negative or larger than CalcTimeoutToVerbose
        /// </summary>
        public int CalcTimeoutToKill = -1;

        public EnginKalkulatorowyWThreading(SynchronizationContext SynchronizationContext)
        {
            this.synchroContext = SynchronizationContext;
        }

        /// <summary>
        /// znizi pocitadlo a ak sa vynuluje tak prepocita stack
        /// </summary>
        /// <param name="force">resetne pauzove pocitadlo takze vynuti prepocet stacku</param>
        public override void Start(bool force)
        {
            if (force) this.pauseCount = 0;
            else if (this.pauseCount > 0) this.pauseCount--;
            if ((this.pauseCount == 0) && (this.calculating <= 0))
            {
                Task.Factory.StartNew(() =>
                {
                    PropertyChangedEventArgs ea = new PropertyChangedEventArgs("Start");
                    this.Calculate(ea);
                });
            }
        }
        internal override void OnPropertyChanged(Property properta, PropertyChangedEventArgs ea)
        {
            if (properta is null) return;

            if ((this.calculating > 0) || (this.pauseCount != 0))
            {
                if (properta.isInput)
                {
                    //medzi zavislakmi su aj vzorce zavisle od deticiek tejto property. preto uz nemusim prechadzat jej detvaky
                    if (this.Verbose)
                    {
                        if (this.pauseCount != 0)
                        {
                            if ((this.VerboseMessages & EVerboseMessages.AddWhilePaused) != 0)
                                foreach (Equation v in properta.Zavislaci) Logger.Trace(VerbosePrefix + "awp: " + v.ToString());
                        }
                        else
                        {
                            if ((this.VerboseMessages & EVerboseMessages.AddWhileCalculating) != 0)
                                foreach (Equation v in properta.Zavislaci) Logger.Trace(VerbosePrefix + "awc: " + v.ToString());
                        }
                    }
                    this.NonOptimizedEquationChain.Add(properta.Zavislaci);
                }
                return;
            }

            this.NonOptimizedEquationChain.Clear();
            if (this.Verbose)
            {
                if ((this.VerboseMessages & EVerboseMessages.AddBeforeStart) != 0)
                    foreach (Equation v in properta.Zavislaci) Logger.Trace(VerbosePrefix + "abs: " + v.ToString());
            }
            this.NonOptimizedEquationChain.Add(properta.Zavislaci);
            //optimalizujem si jednu vrstvu (urobi mi to spravne poradie v prvej vrstve)
            //predpokladam ze sa mi zmeni vystup prvej vrstvy, tak si ulozim aj vzorce druhej vrstvy
            //foreach (Vzorec v in properta.Zavislaci) NonOptimizedNextVzorecChain.Add(v.output.Zavislaci);
            Task.Factory.StartNew(() =>
            {
                this.Calculate(ea);
            });
        }

        internal override Exception DoCalcNonOptim(PropertyChangedEventArgs ea)
        {
            //there can be only one task, because we have no feedback when the calculation ends

            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            this.StartCalculationNotify();
            var t = Task.Factory.StartNew<Exception>(() =>
            {
                while (this.NonOptimizedEquationChain.HaveNext())
                {
                    var v = this.NonOptimizedEquationChain.GetNext();
                    if (v is null) continue;
                    if (this.Verbose)
                    {
                        if ((this.VerboseMessages & EVerboseMessages.CalculatingNames) != 0)
                            Logger.Trace(VerbosePrefix + "cal: " + v.ToString());
                    }
                    var ex = v.Calculate(ea, synchroContext);
                    if (ex is object) return ex;
                    if (tokenSource.IsCancellationRequested == true)
                    {
                        this.NonOptimizedEquationChain.Clear();
                        return null;
                    }
                }
                return null;
            }, token);
            
            var verbose = Verbose;
            if (!this.WaitForVerbose(t, this.CalcTimeoutToVerbose))
            {
                this.WaitForKill(t, this.CalcTimeoutToKill - this.CalcTimeoutToVerbose, tokenSource);
            }
            this.Verbose = verbose;
            tokenSource.Dispose();

            this.EndCalculationNotify();
            return t.Result;
        }

        private bool WaitForVerbose(Task t, int time)
        {
            if (time <= 0) return false;
            if (t.Wait(time)) return true;
            this.Verbose = true;
            return false;
        }
        private void WaitForKill(Task t, int time, CancellationTokenSource cancellationToken)
        {
            if (time > 0)
            {
                if (!t.Wait(time))
                {
                    cancellationToken.Cancel();
                    t.Wait();
                }
            }
            else t.Wait();
        }
    }
}
