﻿using Kalkulatorowanie.PropertyPathParser;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Kalkulatorowanie
{
    public static class PropertyHelper
    {
        public static IndexedPropertyInfo CreatePropertyInfo(object propertyPathHolder, string propertyPath)
        {
            IndexedPropertyInfo ipi = new IndexedPropertyInfo();

            if (null == propertyPathHolder) return ipi;
            if (string.IsNullOrEmpty(propertyPath)) return ipi;

            var propParts = propertyPath.SplitPropPaths();
            ipi.PropertyHolder = propertyPathHolder;
            ipi.PropertyHolderType = ipi.PropertyHolder.GetType();
            ipi.PropertyInfo = null;

            var propPartNode = propParts.First;
            while (true)
            {
                PropertyHelper.SetPropertyNameIndexInfo(propPartNode, ipi, propertyPath);
                if (propPartNode.Next is object)
                {
                    if (ipi.PropertyHolder is object) ipi.PropertyHolder = PropertyHelper.GetPropertyValue(ipi);
                    ipi.PropertyIndex = null;
                }
                ipi.PropertyPath = propPartNode.Value.PathName;
                if (ipi.PropertyHolder is object) ipi.PropertyHolderType = ipi.PropertyHolder.GetType();
                else ipi.PropertyHolderType = ipi.PropertyInfo.PropertyType; //ziskame typ property pre pripadne dalsie iteracie

                if (propPartNode.Next is null) break;
                propPartNode = propPartNode.Next;
            }
            return ipi;
        }
        public static IEnumerable<IndexedPropertyInfo> EnumeratePropertyInfos(object propertyPathHolder, string propertyPath)
        {
            IndexedPropertyInfo ipi = new IndexedPropertyInfo();

            if (propertyPathHolder is null) yield break;
            if (string.IsNullOrEmpty(propertyPath)) yield break;

            var propParts = propertyPath.SplitPropPaths();
            ipi.PropertyHolder = propertyPathHolder;
            ipi.PropertyHolderType = ipi.PropertyHolder.GetType();
            ipi.PropertyInfo = null;

            var propPartNode = propParts.First;
            while (true)
            {
                PropertyHelper.SetPropertyNameIndexInfo(propPartNode, ipi, propertyPath);
                ipi.PropertyPath += ipi.PropertyName;
                yield return ipi;
                if (propPartNode.Next is null) break;
                propPartNode = propPartNode.Next;
                ipi.PropertyHolderType = ipi.PropertyInfo.PropertyType; //ziskame typ property pre pripadne dalsie iteracie
                ipi.PropertyPath += ".";
                if (ipi.PropertyHolder is object) ipi.PropertyHolder = PropertyHelper.GetPropertyValue(ipi);
                ipi.PropertyIndex = null;
            }
            yield break;
        }

        public static unsafe ulong GetReferenceValue(object item)
        {
            var tr = __makeref(item);
            var ptr = **(UIntPtr**)(&tr);
            return ptr.ToUInt64();
        }

        internal static ulong GetLptr(Property item)
        {
            return item.Lptr;
        }
        internal static ulong GetLptr(Equation item)
        {
            return item.lptr;
        }
        internal static ulong GetLptr(EquationFunc item)
        {
            var target =  item.Target;
            if (target is IKalkulatorowaciaKlassa) return GetLptr((IKalkulatorowaciaKlassa)target);
            return PropertyHelper.GetReferenceValue(target);
        }
        internal static ulong GetLptr(IKalkulatorowaciaKlassa item)
        {
            var nlptr = item.EKObjectLptr;
            if (nlptr.HasValue) return nlptr.Value;
            var lptr = GetReferenceValue(item);
            item.EKObjectLptr = lptr;
            return lptr;
        }

        private static void SetPropertyNameIndexInfo(LinkedListNode<PropertyPathPart> propPartNode, IndexedPropertyInfo ipi, string propertyPath)
        {
            var pname = propPartNode.Value.PathName; //ziskame meno property
            if (string.IsNullOrEmpty(pname)) throw new EntryPointNotFoundException("propertyValue.EnumeratePropertyInfos: in path " + propertyPath + " there is twoo dots");
            ipi.PropertyName = propPartNode.Value.WholePartName;
            ipi.PropertyIndex = propPartNode.Value.Indexes;

            var properties = ipi.PropertyHolderType.GetProperties();
            ipi.PropertyInfo = properties.FirstOrDefault(prop => prop.Name == pname); //ziskame propertyInfo
            if (ipi.PropertyInfo is null) throw new EntryPointNotFoundException("propertyValue.createProperyInfo: nonexisting property: " + pname);
        }
        private static bool IsListIndexWrong(IndexedPropertyInfo ipi, IList iListPropertyHolder)
        {
            bool isList = ipi.PropertyHolder is IList;
            if (ipi.PropertyIndex is null) return isList;
            if (ipi.PropertyIndex.Length == 0) return isList;
            if (!(ipi.PropertyIndex[0] is int index)) return true;
            return iListPropertyHolder.Count < index;
        }
        private static bool IsDictionaryIndexWrong(IndexedPropertyInfo ipi, IDictionary iDictionaryPropertyHolder)
        {
            bool isDict = ipi.PropertyHolder is IDictionary;
            if (ipi.PropertyIndex is null) return isDict;
            if (ipi.PropertyIndex.Length == 0) return isDict;
            if (!(ipi.PropertyIndex[0] is string index)) return true;
            return !iDictionaryPropertyHolder.Contains(index);
        }
        private static object GetPropertyValue(IndexedPropertyInfo ipi)
        {
            if (ipi.PropertyHolder is IList iList &&  PropertyHelper.IsListIndexWrong(ipi, iList)) return null;
            if (ipi.PropertyHolder is IDictionary iDictionary && PropertyHelper.IsDictionaryIndexWrong(ipi, iDictionary)) return null;
            //neviem inym sposobom odignorovat pripad, ze chcem indexovu propertu mimo rozsah.
            try
            {
                return ipi.PropertyInfo.GetValue(ipi.PropertyHolder, ipi.PropertyIndex); //ziskame objekt v ktorom sa ma nachadzat dalsia properta
            }
            catch (Exception)
            {
                //nepodarilo sa ziskat propertyholder.
                //nevadi.
                return null;
            }
        }
        private static LinkedList<PropertyPathPart> SplitPropPaths(this string propertyPath)
        {
            return new Parser().Parse(propertyPath);
        }
    }
}
