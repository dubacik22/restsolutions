﻿namespace Kalkulatorowanie
{
    public enum EVerboseMessages
    {
        AddWhilePaused = 0x01,
        AddWhileCalculating = 0x02,
        AddBeforeStart = 0x04,
        CalculatingNames = 0x08,

        All = 0xff
    }
}
