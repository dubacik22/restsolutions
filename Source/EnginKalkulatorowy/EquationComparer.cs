﻿using System.Collections.Generic;

namespace Kalkulatorowanie
{
    internal class EquationComparer : IEqualityComparer<Equation>
    {
        public bool Equals(Equation a, Equation b)
        {
            return Equation.Equals(a, b);
        }
        // If Equals() returns true for a pair of objects, 
        // GetHashCode must return the same value for these objects. 
        public int GetHashCode(Equation v)
        {
            // Check whether the object is null. 
            if (v is null) return 0;
            // Calculate the hash code for the Vzorec. 
            return v.GetHashCode();
        }
    }
}
