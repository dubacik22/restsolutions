﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ResourcesExt
{
    public class WeakEventSource<TEventArgs> where TEventArgs : EventArgs
    {
        private readonly List<WeakDelegate> handlers;

        public WeakEventSource()
        {
            this.handlers = new List<WeakDelegate>();
        }

        public void Raise(object sender, TEventArgs e)
        {
            lock (this.handlers)
            {
                foreach (WeakDelegate weakDelegate in ((IEnumerable<WeakDelegate>)this.handlers.ToArray()).Where<WeakEventSource<TEventArgs>.WeakDelegate>((Func<WeakEventSource<TEventArgs>.WeakDelegate, bool>)(h => !h.Invoke(sender, e))))
                    this.handlers.Remove(weakDelegate);
            }
        }

        public bool HasSubscribers()
        {
            int aliveHandlers = 0;
            lock (this.handlers)
            {
                foreach (WeakEventSource<TEventArgs>.WeakDelegate weakDelegate in ((IEnumerable<WeakEventSource<TEventArgs>.WeakDelegate>)this.handlers.ToArray()).Where<WeakEventSource<TEventArgs>.WeakDelegate>((Func<WeakEventSource<TEventArgs>.WeakDelegate, bool>)(h => !h.IsAlive())))
                    this.handlers.Remove(weakDelegate);
                aliveHandlers = handlers.Count;
            }
            return aliveHandlers > 0;
        }

        public void Subscribe(EventHandler<TEventArgs> handler)
        {
            var weakHandlers = handler
                .GetInvocationList()
                .Select(d => new WeakDelegate(d))
                .ToList();

            lock (this.handlers)
            {
                this.handlers.AddRange(weakHandlers);
            }
        }

        public void Unsubscribe(EventHandler<TEventArgs> handler)
        {
            lock (this.handlers)
            {
                int index = this.handlers.FindIndex((Predicate<WeakEventSource<TEventArgs>.WeakDelegate>)(h => h.IsMatch(handler)));
                if (index < 0) return;
                this.handlers.RemoveAt(index);
            }
        }

        class WeakDelegate
        {
            #region Open handler generation and cache
            private delegate void OpenEventHandler(object target, object sender, TEventArgs e);

            // ReSharper disable once StaticMemberInGenericType (by design)
            private static readonly ConcurrentDictionary<MethodInfo, OpenEventHandler> OpenHandlerCache =
                new ConcurrentDictionary<MethodInfo, OpenEventHandler>();

            private static OpenEventHandler CreateOpenHandler(MethodInfo method)
            {
                var target = Expression.Parameter(typeof(object), "target");
                var sender = Expression.Parameter(typeof(object), "sender");
                var e = Expression.Parameter(typeof(TEventArgs), "e");

                if (method.IsStatic)
                {
                    var expr = Expression.Lambda<OpenEventHandler>(
                        Expression.Call(
                            method,
                            sender, e),
                        target, sender, e);
                    return expr.Compile();
                }
                else
                {
                    var expr = Expression.Lambda<OpenEventHandler>(
                         Expression.Call(
                             Expression.Convert(target, method.DeclaringType),
                             method,
                             sender, e),
                         target, sender, e);
                    return expr.Compile();
                }
            }

            #endregion

            private readonly WeakReference weakTarget;
            private readonly MethodInfo method;
            private readonly OpenEventHandler openHandler;

            public WeakDelegate(Delegate handler)
            {
                this.weakTarget = handler.Target != null ? new WeakReference(handler.Target) : null;
                this.method = handler.Method;
                this.openHandler = OpenHandlerCache.GetOrAdd(this.method, CreateOpenHandler);
            }

            public bool Invoke(object sender, TEventArgs e)
            {
                object target = null;
                if (this.weakTarget != null)
                {
                    target = this.weakTarget.Target;
                    if (target == null) return false;
                }
                this.openHandler(target, sender, e);
                return true;
            }

            public bool IsAlive()
            {
                if (this.weakTarget is null) return false;
                return this.weakTarget.IsAlive;
            }

            public bool IsMatch(EventHandler<TEventArgs> handler)
            {
                if (object.ReferenceEquals(handler.Target, object.ReferenceEquals((object)this.weakTarget, (object)null) ? (object)null : this.weakTarget.Target))
                    return handler.Method.Equals((object)this.method);
                return false;
            }            
        }
    }
}

