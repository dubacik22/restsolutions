﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ResourcesExt
{
    public static class UncertainitiesHelperExt
    {
        private static readonly List<DispExp> exps = new List<DispExp>
        {
            new DispExp("p", -12),
            new DispExp("n", -9),
            new DispExp("u", -6),
            new DispExp("m", -3),
            new DispExp("", 0),
            new DispExp("k", 3),
            new DispExp("M", 6),
            new DispExp("G", 9),
            new DispExp("T", 12)
        };

        /// <summary>
        /// adapt DisplayExponent to number dividable by 3 (that means result will be in mili, kilo, mega etc..
        /// </summary>
        /// <param name="value"></param>
        public static void AdaptDispExpTo3(DispValue value)
        {
            if (DispValue.IsNullOrNaN(value) || value.Value == 0.0) return;
            DispValue from = new DispValue(value);
            bool negative = from.Value < 0.0;
            if (negative) from.Value = -from.Value;
            foreach (var exp in UncertainitiesHelperExt.exps)
            {
                from.DispExp = exp.Exp;
                if (from.DisplayValue < 1000) break;
            }
            value.CopyFrom(from);
            if (!negative) return;
            value.Value = -value.Value;
        }

        /// <summary>
        /// rounds to 2 actual places (that means finds first non zero digit and takes second one after that.)
        /// </summary>
        /// <param name="value"></param>
        public static bool RoundTo2ActPlcs(DispValue value)
        {
            if (!UncertainitiesHelperExt.RoundToXActPlcsInternal(value, 2, UncertainitiesHelperExt.exps)) return false;
            return UncertainitiesHelperExt.RoundToXActPlcsInternal(value, 2, UncertainitiesHelperExt.exps);
        }

        /// <summary>
        /// rounds to x actual places (that means finds first non zero digit and takes second one after that.)
        /// </summary>
        /// <param name="value"></param>
        /// <param name="x">places to round to</param>
        /// <param name="exponents">available exponents</param>
        public static bool RoundTo2ActPlcs(DispValue value, List<DispExp> exponents)
        {
            if (!UncertainitiesHelperExt.RoundToXActPlcsInternal(value, 2, exponents)) return false;
            return UncertainitiesHelperExt.RoundToXActPlcsInternal(value, 2, exponents);
        }

        /// <summary>
        /// rounds to x actual places (that means finds first non zero digit and takes second one after that.)
        /// </summary>
        /// <param name="value"></param>
        /// <param name="x">places to round to</param>
        public static bool RoundToXActPlcs(DispValue value, int x)
        {
            if (!UncertainitiesHelperExt.RoundToXActPlcsInternal(value, x, UncertainitiesHelperExt.exps)) return false;
            return UncertainitiesHelperExt.RoundToXActPlcsInternal(value, x, UncertainitiesHelperExt.exps);
        }

        /// <summary>
        /// rounds to x actual places (that means finds first non zero digit and takes second one after that.)
        /// </summary>
        /// <param name="value"></param>
        /// <param name="x">places to round to</param>
        /// <param name="exponents">available exponents</param>
        public static bool RoundToXActPlcs(DispValue value, int x, List<DispExp> exponents)
        {
            if (!UncertainitiesHelperExt.RoundToXActPlcsInternal(value, x, exponents)) return false;
            return UncertainitiesHelperExt.RoundToXActPlcsInternal(value, x, exponents);
        }
        private static bool RoundToXActPlcsInternal(DispValue value, int x, List<DispExp> exponents)
        {
            if (DispValue.IsNullOrNaN(value)) return true;
            if (double.IsPositiveInfinity(value.Value)) return false;
            if (double.IsNegativeInfinity(value.Value)) return false;
            if (value.DisplayValue >= 99.5) UncertainitiesHelperExt.AdaptDispExpToLessThan100(value, exponents);
            //if (value.DisplayValue >= 99.5) return false;
            bool negative = value.Value < 0.0;
            if (negative) value.Value = -value.Value;
            UncertainitiesHelperExt.RoundStuff rs = new UncertainitiesHelperExt.RoundStuff(value, x);
            rs.ActCharPosition++;
            while (rs.ActCharPosition < rs.RoundingString.Length)
            {
                if (!rs.TakeCareOfWholePart()) break;
                rs.ActCharPosition++;
            }
            while (rs.ActCharPosition < rs.RoundingString.Length)
            {
                if (!rs.TakeCareOfDot()) break;
                rs.ActCharPosition++;
            }
            while (rs.ActCharPosition < rs.RoundingString.Length)
            {
                if (!rs.TakeCareOfInsignificantZero()) break;
                rs.ActCharPosition++;
            }
            while (rs.ActCharPosition < rs.RoundingString.Length)
            {
                if (!rs.TakeCareOfSignificantDecimalPart()) break;
                rs.ActCharPosition++;
            }
            rs.AddLasZeros();

            double.TryParse(rs.ResultString(), NumberStyles.Any, CultureInfo.InvariantCulture, out double result);
            rs.InputValue.DisplayValue = result;
            rs.InputValue.DisplayDecPlaces = rs.DecPlaces;
            if (!negative) return true;
            rs.InputValue.Value = -rs.InputValue.Value;
            return true;
        }
        private static void AdaptDispExpToLessThan100(DispValue value, List<DispExp> exponents)
        {
            if (DispValue.IsNullOrNaN(value) || value.Value == 0.0) return;
            DispValue from = new DispValue(value);
            var origExp = value.DispExp;
            bool negative = from.Value < 0.0;
            if (negative) from.Value = -from.Value;
            foreach (var exp in exponents)
            {
                if (exp is null) continue;
                if (exp.Exp < origExp) continue;
                from.DispExp = exp.Exp;
                if (from.DisplayValue < 99.5) break;
            }
            value.CopyFrom(from);
            if (!negative) return;
            value.Value = -value.Value;
        }

        public static double GetUaAppx(IEnumerable<double> values)
        {
            return (values.Max() - values.Min()) / (2.0 * MathHelperExt.Sqrt3);
        }

        public static double GetUaAppx(params double[] values)
        {
            return UncertainitiesHelperExt.GetUaAppx((IEnumerable<double>)values);
        }

        public static double GetUaAppx(double div)
        {
            return div / MathHelperExt.Sqrt3;
        }

        public static double GetUr(double d, int DlDenom)
        {
            return d / (2.0 * (double)DlDenom * MathHelperExt.Sqrt3);
        }

        public static double GetUr(double d)
        {
            return d / (2.0 * MathHelperExt.Sqrt3);
        }

        private class RoundStuff
        {
            private char actCh;
            private readonly StringBuilder resultStringBuilder = new StringBuilder();
            private int actSignificantNumbers;
            private bool hasDot;

            public int FinalSignificantNumbers { get; set; } = 2;
            public DispValue InputValue { get; set; }
            public int DecPlaces { get; set; }

            private string roundingString;
            public string RoundingString
            {
                get => this.roundingString;
                set
                {
                    this.roundingString = value;
                    this.RenewCh();
                }
            }

            private int actCharPosition = -1;
            public int ActCharPosition
            {
                get => this.actCharPosition;
                set
                {
                    this.actCharPosition = value;
                    this.RenewCh();
                }
            }

            public RoundStuff() { }
            public RoundStuff(DispValue value, int finalSignificantNumber)
            {
                this.FinalSignificantNumbers = finalSignificantNumber;
                this.InputValue = value;
                this.RoundingString = value.DisplayValue.ToString("0.###################################################################################################################################################################################################################################################################################################################################################", (IFormatProvider)CultureInfo.InvariantCulture);
            }

            public bool TakeCareOfWholePart()
            {
                if (this.actCh == '.') return false;
                if ((this.actCh != '0') || (this.actSignificantNumbers > 0)) this.actSignificantNumbers++;
                if (this.actSignificantNumbers > this.FinalSignificantNumbers) this.actCh = '0';
                this.resultStringBuilder.Append(this.actCh);
                if (this.actSignificantNumbers == this.FinalSignificantNumbers && (this.GetNextDigit() > '4')) this.AddOne();
                return true;
            }
            public bool TakeCareOfDot()
            {
                if (this.actCh != '.') return false;
                this.hasDot = true;
                if (this.actSignificantNumbers < this.FinalSignificantNumbers) this.resultStringBuilder.Append(this.actCh);
                return true;
            }
            public bool TakeCareOfInsignificantZero()
            {
                if (this.actSignificantNumbers >= this.FinalSignificantNumbers) return false;
                if (this.actCh != '0') return false;
                if (this.actSignificantNumbers > 0) this.actSignificantNumbers++;
                this.DecPlaces++;
                this.resultStringBuilder.Append(this.actCh);
                return true;
            }
            public bool TakeCareOfSignificantDecimalPart()
            {
                if (this.actSignificantNumbers >= this.FinalSignificantNumbers) return false;
                this.DecPlaces++;
                this.actSignificantNumbers++;
                this.resultStringBuilder.Append(this.actCh);
                if (this.actSignificantNumbers < this.FinalSignificantNumbers) return true;
                if (this.actSignificantNumbers == this.FinalSignificantNumbers && (this.GetNextDigit() > '4')) this.AddOne();
                return true;
            }
            public void AddLasZeros()
            {
                for (; this.actSignificantNumbers < this.FinalSignificantNumbers; this.actSignificantNumbers++)
                {
                    if (!this.hasDot)
                    {
                        this.resultStringBuilder.Append(".");
                        this.hasDot = true;
                    }
                    this.resultStringBuilder.Append("0");
                    this.DecPlaces++;
                }
            }

            public string ResultString()
            {
                return this.resultStringBuilder.ToString();
            }
            public override string ToString()
            {
                string actCh;
                if (this.actCh == (char)0) actCh = string.Empty;
                else actCh = this.actCh.ToString();
                return this.RoundingString + ", " + actCh + ", " + resultStringBuilder.ToString();
            }

            private void RenewCh()
            {
                this.actCh = this.GetChar(this.actCharPosition);
            }
            private char GetChar(int pos)
            {
                if (pos < 0) return default;
                if (string.IsNullOrEmpty(this.roundingString) || (pos >= this.roundingString.Length)) return default;
                return this.RoundingString[pos];
            }
            private char GetNextDigit()
            {
                var ch = this.GetChar(this.actCharPosition + 1);
                if (ch != '.') return ch;
                return this.GetChar(this.actCharPosition + 2);
            }
            private void AddOne()
            {
                for (int index = this.resultStringBuilder.Length - 1; index >= 0; --index)
                {
                    char ch1 = this.resultStringBuilder[index];
                    if (ch1 == '.') continue;
                    if (ch1 == '9')
                    {
                        char ch2 = '0';
                        this.resultStringBuilder[index] = ch2;
                        if (index < 1)
                        {
                            this.resultStringBuilder.Insert(0, '1');
                            break;
                        }
                    }
                    else
                    {
                        this.resultStringBuilder[index] = (char)(ch1 + 1U);
                        break;
                    }
                }
            }
        }
    }
}
