﻿namespace ResourcesExt
{
    public interface IClearable
    {
        void Clear();
    }
}
