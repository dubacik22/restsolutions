﻿using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Complex32;
using MathNet.Numerics.LinearAlgebra.Factorization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ResourcesExt
{
    public static class MathHelperExt
    {
        private static double _sqrt2 = double.NaN;
        private static double _sqrt3 = double.NaN;
        private static double _sqrt6 = double.NaN;

        public static double Sqrt2
        {
            get
            {
                if (double.IsNaN(MathHelperExt._sqrt2))
                    MathHelperExt._sqrt2 = Math.Sqrt(2.0);
                return MathHelperExt._sqrt2;
            }
        }

        public static double Sqrt3
        {
            get
            {
                if (double.IsNaN(MathHelperExt._sqrt3))
                    MathHelperExt._sqrt3 = Math.Sqrt(3.0);
                return MathHelperExt._sqrt3;
            }
        }

        public static double Sqrt6
        {
            get
            {
                if (double.IsNaN(MathHelperExt._sqrt6))
                    MathHelperExt._sqrt6 = Math.Sqrt(6.0);
                return MathHelperExt._sqrt6;
            }
        }

        public static int? CntDecPl(this double value)
        {
            try
            {
                if (double.IsNaN(value) || double.IsInfinity(value)) return 0;
                return BitConverter.GetBytes(decimal.GetBits((decimal)value)[3])[2];
            }
            catch
            {
                return null;
            }
        }

        public static double GetStdev(IEnumerable<double> l)
        {
            var num = 0.0;
            if (l.Count() > 0)
            {
                double avg = l.Average();
                avg = Math.Round(avg, 12);
                num = Math.Sqrt(l.Sum(d => Math.Pow(d - avg, 2.0)) / (l.Count() - 1));
                num = Math.Round(num, 12);
            }
            return num;
        }

        private static List<double> GetD(List<DispValue> l)
        {
            int num1 = 0;
            List<double> doubleList = new List<double>(l.Count - 1);
            List<DispValue> dispValueList = l;
            int index1 = num1;
            int num2 = index1 + 1;
            double num3 = dispValueList[index1];
            for (int index2 = num2; index2 < l.Count<DispValue>(); ++index2)
            {
                double num4 = l[index2];
                double num5 = num4 - num3;
                doubleList.Add(num5);
                num3 = num4;
            }
            return doubleList;
        }

        private static int FindMaxD(List<DispValue> l)
        {
            double num1 = 0.0;
            int num2 = 1;
            List<double> d = MathHelperExt.GetD(l);
            for (int index = 0; index < d.Count(); ++index)
            {
                double num3 = d[index];
                if (num1 < num3)
                {
                    num1 = num3;
                    num2 = index;
                }
            }
            return num2;
        }

        private static List<DispValue> SplitInterv(List<DispValue> field, int interv, int count)
        {
            double num1 = field[interv + 1].Value;
            double num2 = field[interv].Value;
            List<DispValue> dispValueList = new List<DispValue>(field);
            for (int index = 1; index < count; ++index)
                dispValueList.Add(new DispValue((num1 - num2) / count * index + num2, field[interv].DecPlaces, field[interv].DispExp));
            dispValueList.Sort((v1, v2) =>
            {
                if (v1 > v2)
                    return 1;
                return v1 < v2 ? -1 : 0;
            });
            return dispValueList;
        }

        public static List<DispValue> DivideOnTo(List<DispValue> l, int minCount)
        {
            if (l != null && l.Count < minCount && l.Count > 1)
            {
                List<DispValue> dispValueList1 = new List<DispValue>(l);

                do
                {
                    int num1 = 2;
                    MathHelperExt.GetD(dispValueList1);
                    int maxD = MathHelperExt.FindMaxD(dispValueList1);
                    double num2 = double.MaxValue;
                    List<DispValue> l1 = dispValueList1;
                    List<DispValue> dispValueList2;
                    double num3;
                    do
                    {
                        dispValueList2 = l1;
                        num3 = num2;
                        l1 = MathHelperExt.SplitInterv(dispValueList1, maxD, num1++);
                        num2 = MathHelperExt.GetStdev(MathHelperExt.GetD(l1));
                    }
                    while (num3 - num2 >= -10.0 && l1.Count < minCount);
                    dispValueList1 = num3 - num2 >= -10.0 ? l1 : dispValueList2;
                }
                while (dispValueList1.Count < minCount);
                l = dispValueList1;

            }
            return l;
        }

        public static List<DispValue> SortDistinct(List<DispValue> l)
        {
            return SortDistinct(l, new DispValueComparer());
        }
        public static List<DispValue> SortDistinct(List<DispValue> l, IEqualityComparer<DispValue> comparer)
        {
            IEnumerable<DispValue> dispValues = l.Distinct(comparer);
            List<DispValue> dispValueList = new List<DispValue>();
            foreach (DispValue dispValue in dispValues) dispValueList.Add(dispValue);
            dispValueList.Sort((v1, v2) =>
            {
                if (v1 > v2)
                    return 1;
                return v1 < v2 ? -1 : 0;
            });
            return dispValueList;
        }

        /// <summary>
        /// ziskanie koeficientov poynomu n-teho radu pre nozinu hodnot x a y
        /// code from http://rosettacode.org/wiki/Polynomial_regression
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="degree">rad polynomu</param>
        /// <returns></returns>
        public static double[] Polyfit(double[] x, double[] y, int degree)
        {
            bool problem = false;
            double[] d;
            foreach (double a in x) if (double.IsNaN(a)) problem = true;
            foreach (double a in y) if (double.IsNaN(a)) problem = true;
            if (x.Length <= degree) problem = true;
            if (problem)
            {
                degree++;
                d = new double[degree];
                for (int i = 0; i < degree; i++) d[i] = double.NaN;
                return d;
            }
            // Vandermonde matrix
            DenseMatrix v = new DenseMatrix(x.Length, degree + 1);
            Complex32[] c = new Complex32[y.Length];
            for (int i = 0; i < v.RowCount; i++)
            {
                c[i] = new Complex32((float)y[i], 0.0f);
                for (int j = 0; j <= degree; j++) v[i, j] = (Complex32)Math.Pow(x[i], j);
            }

            Matrix<Complex32> yv = new DenseVector(c).ToColumnMatrix();
            QR<Complex32> qr = v.QR();
            // Math.Net doesn't have an "economy" QR, so:
            // cut R short to square upper triangle, then recompute Q

            Matrix<Complex32> r = qr.R.SubMatrix(0, degree + 1, 0, degree + 1);
            Matrix<Complex32> inv = r.Inverse();
            Matrix<Complex32> q = v.Multiply(inv);
            Matrix<Complex32> p = inv.Multiply(q.TransposeThisAndMultiply(yv));

            c = p.Column(0).ToArray();
            d = new double[c.Length];
            for (int i = 0; i < d.Length; i++) d[i] = c[i].Real;
            return d;
        }

        public static double GetWTFNom4n(int n)
        {
            switch (n)
            {
                case 0:
                    return 0.0;
                case 1:
                    return 0.0;
                case 2:
                    return 7.0;
                case 3:
                    return 2.3;
                case 4:
                    return 1.7;
                case 5:
                    return 1.4;
                case 6:
                    return 1.3;
                case 7:
                    return 1.3;
                case 8:
                    return 1.2;
                case 9:
                    return 1.2;
                default:
                    return 1.0;
            }
        }

        public static double GetUaUcKrit(int n)
        {
            switch (n)
            {
                case 0:
                    return 0.0;
                case 1:
                    return 0.0;
                case 2:
                    return 0.38;
                case 3:
                    return 0.45;
                case 4:
                    return 0.49;
                case 5:
                    return 0.53;
                case 6:
                    return 0.56;
                case 7:
                    return 0.59;
                case 8:
                    return 0.61;
                case 9:
                    return 0.63;
                default:
                    return 1.0;
            }
        }

        public static double GetVef(double uc, double ua, double cnt)
        {
            if (double.IsNaN(uc) || double.IsNaN(ua) || double.IsNaN(cnt)) return double.NaN;
            if (ua == 0.0) return double.PositiveInfinity;
            double uc2 = uc * uc;
            double ua2 = ua * ua;
            return uc2 * uc2 * (cnt - 1.0) / (ua2 * ua2);
        }

        public static double GetK4Vef(double vef)
        {
            if (double.IsNaN(vef)) return double.NaN;
            if (vef < 1.0) return double.NaN;
            if (vef < 2.0) return 13.97;
            if (vef < 3.0) return 4.53;
            if (vef < 4.0) return 3.31;
            if (vef < 5.0) return 2.87;
            if (vef < 6.0) return 2.65;
            if (vef < 7.0) return 2.52;
            if (vef < 8.0) return 2.43;
            if (vef < 10.0) return 2.37;
            if (vef < 11.0) return 2.28;
            if (vef < 12.0) return 2.25;
            if (vef < 13.0) return 2.23;
            if (vef < 14.0) return 2.21;
            if (vef < 15.0) return 2.2;
            if (vef < 16.0) return 2.18;
            if (vef < 17.0) return 2.17;
            if (vef < 18.0) return 2.16;
            if (vef < 19.0) return 2.15;
            if (vef < 20.0) return 2.14;
            if (vef < 25.0) return 2.13;
            if (vef < 30.0) return 2.11;
            if (vef < 35.0) return 2.09;
            if (vef < 40.0) return 2.07;
            if (vef < 47.0) return 2.06;
            if (vef < 57.0) return 2.05;
            if (vef < 73.0) return 2.04;
            if (vef < 102.0) return 2.03;
            if (vef < 168.0) return 2.02;
            if (vef < 502.0) return 2.01;
            return 2.0;
        }

        public static double NaN2Zero(this double number)
        {
            if (double.IsNaN(number)) return 0.0;
            return number;
        }
    }
}