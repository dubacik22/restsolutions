﻿using System;

namespace ResourcesExt
{
    public class DispExp : IComparable, IComparable<DispExp>
    {
        public string StrExp { get; set; }
        public int Exp { get; set; }
        public bool Default { get; set; } = false;
        public DispExp(string strExp, int exp)
        {
            this.StrExp = strExp;
            this.Exp = exp;
        }
        public DispExp(string strExp, int exp, bool def)
        {
            this.StrExp = strExp;
            this.Exp = exp;
            this.Default = def;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is DispExp dispExp)) return false;
            if (!this.EqualsExp(dispExp)) return false;
            if (dispExp.StrExp != this.StrExp) return false;
            return true;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public bool EqualsExp(DispExp dispExp)
        {
            if (dispExp.Exp != this.Exp) return false;
            return true;
        }
        public int CompareTo(object obj)
        {
            if (!(obj is DispExp dispExp)) return -1;
            return this.CompareTo(dispExp);
        }

        public int CompareTo(DispExp other)
        {
            if (other is null) return -1;

            var dispExpEquals = this.Exp.CompareTo(other.Exp);
            if (dispExpEquals == 0) return this.StrExp.CompareTo(other.StrExp);
            return dispExpEquals;
        }
    }
}
