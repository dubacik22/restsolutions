﻿namespace ResourcesExt
{
    public enum EventStarter
    {
        Undef = 0,
        PredictNeeded = 8,
        ChildrenProperty = 512, // 0x00000200
        ParentProperty = 1024, // 0x00000400
        ForeignStarter = 2048, // 0x00000800
        InternalProperty = 4096, // 0x00001000
        UserComit1 = 4096, // 0x00001000
        UserComit2 = 8192, // 0x00002000
        UserComit = 12288, // 0x00003000
        UserMouse1 = 16384, // 0x00004000
        UserMouse2 = 32768, // 0x00008000
        UserMouse = 49152, // 0x0000C000
    }
}