﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ResourcesExt
{
    public class SelectorItem : INotifyPropertyChanged
    {
        private bool isSelected;
        public bool IsSelected
        {
            get => this.isSelected;
            set => this.SetField(ref this.isSelected, value, nameof(this.IsSelected));
        }

        internal SelectorItem(bool value)
        {
            this.isSelected = value;
        }

        public void Select()
        {
            this.IsSelected = true;
        }
        public void UnSelect()
        {
            this.IsSelected = false;
        }

        #region inotifypropertychanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name, Object oldVal, Object newVal)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        protected bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            T oldValue = field;
            field = value;
            this.OnPropertyChanged(propertyName, oldValue, value);
            return true;
        }
        #endregion
    }
}
