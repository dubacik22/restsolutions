﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ResourcesExt
{
    public class Selector : INotifyPropertyChanged
    {
        private Dictionary<string, SelectorItem> itemsDict = new Dictionary<string, SelectorItem>();
        private List<SelectorItem> itemsList = new List<SelectorItem>();
        private readonly bool doUnselection;
        private bool ignore;

        private int selectedIndex;
        public int SelectedIndex
        {
            get => this.selectedIndex;
            set
            {
                this.SelectNth(value);
                this.SetField(ref selectedIndex, value, nameof(this.SelectedIndex));
            }
        }

        public Selector(bool doUnselection)
        {
            this.doUnselection = doUnselection;
        }

        private void HookEvents(SelectorItem item)
        {
            item.PropertyChanged += new PropertyChangedEventHandler(this.Item_PropertyChanged);
        }

        private void UnhookEvents(SelectorItem item)
        {
            item.PropertyChanged -= new PropertyChangedEventHandler(this.Item_PropertyChanged);
        }

        private void SetItemsSelectionBut(SelectorItem value, bool restState, bool activeState)
        {
            this.ignore = true;
            var selectedIndex = -1;
            var index = 0;
            foreach (var item in this.itemsDict.Values)
            {
                if (!ReferenceEquals(value, item)) item.IsSelected = restState;
                else
                {
                    item.IsSelected = activeState;
                    if (selectedIndex == -1) selectedIndex = index;
                }
                index++;
            }
            foreach (var item in this.itemsList)
            {
                if (!ReferenceEquals(value, item)) item.IsSelected = restState;
                else
                {
                    item.IsSelected = activeState;
                    if (selectedIndex == -1) selectedIndex = index;
                }
                index++;
            }
            this.SelectedIndex = selectedIndex;
            this.ignore = false;
        }

        private void SelectNth(int value)
        {
            if (this.ignore) return;
            this.ignore = true;
            var index = 0;
            foreach (var item in this.itemsDict.Values)
            {
                if (index == value) item.IsSelected = true;
                else item.IsSelected = false;
                index++;
            }
            foreach (var item in this.itemsList)
            {
                if (index == value) item.IsSelected = true;
                else item.IsSelected = false;
                index++;
            }
            this.ignore = false;
        }

        private void Item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.ignore) return;
            if (!(sender is SelectorItem selectorItem)) return;
            this.SetItemsSelectionBut(sender as SelectorItem, this.doUnselection, selectorItem.IsSelected);
        }

        public void Add(SelectorItem value, string key)
        {
            if (value is null || string.IsNullOrEmpty(key)) return;
            if (this.itemsDict.ContainsKey(key))
            {
                this.UnhookEvents(this.itemsDict[key]);
                this.itemsDict[key] = value;
            }
            else
            {
                this.itemsDict.Add(key, value);
            }
            this.HookEvents(value);
        }

        public void Add(SelectorItem value)
        {
            if (this.itemsList.Contains(value)) return;
            this.itemsList.Add(value);
            this.HookEvents(value);
        }

        public void Select(SelectorItem item)
        {
            if (!this.Contains(item)) return;
            this.SetItemsSelectionBut(item, this.doUnselection, !this.doUnselection);
        }

        public SelectorItem CreateNext(string key)
        {
            if (string.IsNullOrEmpty(key)) return null;
            var ret = new SelectorItem(false);
            this.Add(ret, key);
            return ret;
        }

        public SelectorItem CreateNext()
        {
            var ret = new SelectorItem(false);
            this.Add(ret);
            return ret;
        }

        public void Remove(SelectorItem value)
        {
            if (this.itemsList.Remove(value) || !this.itemsDict.Values.Contains(value)) return;
            this.itemsDict.Remove(this.itemsDict.FirstOrDefault(item => item.Value == value).Key);
        }

        public void Remove(string key)
        {
            this.itemsDict.Remove(key);
        }

        private SelectorItem GetSelected()
        {
            return this.itemsDict.Values.FirstOrDefault(item => item.IsSelected) ?? this.itemsList.FirstOrDefault(item => item.IsSelected);
        }

        private bool Contains(SelectorItem value)
        {
            return this.itemsList.Contains(value) || this.itemsDict.ContainsValue(value);
        }


        #region inotifypropertychanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name, Object oldVal, Object newVal)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        protected bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            T oldValue = field;
            field = value;
            this.OnPropertyChanged(propertyName, oldValue, value);
            return true;
        }
        #endregion
    }
}
