﻿using System;
using System.Text;

namespace ResourcesExt
{
    public class StringValueAndUnitSplitter
    {
        public virtual Tuple<string, string> Split(string value, string defaultvalue, string defaultExp)
        {
            if (string.IsNullOrEmpty(value))
            {
                return new Tuple<string, string>(defaultvalue, defaultExp);
            }

            Tuple<string, string> ret;
            //ret = this.TryDivideInSpace(value);
            //if (ret is object)
            //{
            //    return ret;
            //}
            //ret = this.TryDivideByUnits(value);
            //if (ret is object)
            //{
            //    return ret;
            //}
            return this.TryDivideNumCharChange(value);
        }
        //private Tuple<string, string> TryDivideInSpace(string value)
        //{
        //    var spaceIndex = value.IndexOf(' ');
        //    string strNumber;
        //    if (spaceIndex > 0)
        //    {
        //        var unit = value.Substring(spaceIndex + 1, value.Length - spaceIndex - 1);
        //        strNumber = value.Substring(0, spaceIndex);
        //        return new Tuple<string, string>(strNumber, unit);
        //    }
        //    return null;
        //}
        //private Tuple<string, string> TryDivideByUnits(string value)
        //{
        //    var unit = string.Empty;
        //    var valueL = value.Length;
        //    var lastMatchL = 0;
        //    foreach (var newUnit in this.ExpList)
        //    {
        //        var unitL = newUnit.StrExp.Length;
        //        if ((unitL > lastMatchL) && (valueL >= unitL))
        //        {
        //            if (value.EndsWith(newUnit.StrExp))
        //            {
        //                lastMatchL = unitL;
        //                unit = newUnit.StrExp;
        //            }
        //        }
        //    }
        //    if (lastMatchL > 0)
        //    {
        //        var strNumber = value.Substring(0, valueL - lastMatchL);
        //        return new Tuple<string, string>(strNumber, unit);
        //    }
        //    return null;
        //}
        private Tuple<string, string> TryDivideNumCharChange(string value)
        {
            var strNumber = string.Empty;
            var unit = new StringBuilder();
            var wasChar = false;
            foreach (var ch in value)
            {
                if (this.IsPartOfNum(ch, strNumber) && !wasChar)
                {
                    strNumber += ch;
                }
                else
                {
                    unit.Append(ch);
                    wasChar = true;
                }
            }
            return new Tuple<string, string>(strNumber.Trim(), unit.ToString().Trim());
        }
        private bool IsPartOfNum(char ch, string alreadyInNum)
        {
            if ((ch >= '0') && (ch <= '9')) return true;
            if ((ch == '.') || (ch == ','))
            {
                if (alreadyInNum.Contains(".") || alreadyInNum.Contains(",")) return false;
                return true;
            }
            if (ch == '-')
            {
                if (string.IsNullOrEmpty(alreadyInNum)) return true;
                return false;
            }
            return false;
        }
    }
}
