﻿using System;

namespace ResourcesExt.EventArguments
{
    public class ObjectEventArgs : EventArgs
    {
        public object NewO { get; set; }
        public object OldO { get; set; }
        public EventStarter Starter { get; set; }
        public bool Handled { get; set; }
        public bool Success { get; set; }
    }
}
