﻿using System.Windows;

namespace ResourcesExt.EventArguments
{
    public class ObjectRoutedEventArgs : RoutedEventArgs
    {
        public object O { get; set; }

        public ObjectRoutedEventArgs(RoutedEvent routedEvent)
          : base(routedEvent)
        {
            this.Handled = false;
        }

        public ObjectRoutedEventArgs(RoutedEvent routedEvent, object obj)
          : base(routedEvent)
        {
            this.O = obj;
            this.Handled = false;
        }
    }
}
