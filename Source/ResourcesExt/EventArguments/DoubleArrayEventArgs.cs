﻿using System;

namespace ResourcesExt.EventArguments
{
    public class DoubleArrayEventArgs : EventArgs
    {
        public double[] Value { get; set; }
        public EventStarter Starter { get; set; }
    }
}
