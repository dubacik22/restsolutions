﻿using System;

namespace ResourcesExt.EventArguments
{
    public class IntEventArgs : EventArgs
    {
        public int Value { get; set; }
        public EventStarter Starter { get; set; }
        public EventHandler<IntEventArgs> Handler { get; set; }
    }
}
