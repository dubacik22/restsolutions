﻿using System;

namespace ResourcesExt.EventArguments
{
    public class TextEventArgs : EventArgs
    {
        public string Value { get; set; }
        public EventStarter Starter { get; set; }
        public EventHandler<TextEventArgs> Handler { get; set; }
    }
}
