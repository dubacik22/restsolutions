﻿using System;

namespace ResourcesExt.EventArguments
{
    public class DispValueArrayEventArgs : EventArgs
    {
        private DispValue[] value;
        public DispValue[] Value
        {
            get
            {
                return value;
            }
            set
            {
                this.value = value;
                if (this.value != null)
                {
                    this.Values = new double[this.value.Length];
                    this.DecPlaces = new int?[this.value.Length];
                    for (int i = 0; i < this.value.Length; i++)
                    {
                        this.Values[i] = this.value[i].Value;
                        this.DecPlaces[i] = this.value[i].DecPlaces;
                    }
                }
            }
        }
        public double[] Values { get; private set; }
        public int?[] DecPlaces { get; private set; }
        public EventStarter Starter { get; set; }
    }
}
