﻿using System;
using System.Collections;

namespace ResourcesExt.EventArguments
{
    public class IEnumerableEventArgs : EventArgs
    {
        public IEnumerable Value { get; set; }
        public EventStarter Starter { get; set; }
    }
}
