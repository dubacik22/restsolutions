﻿using System;

namespace ResourcesExt.EventArguments
{
    public class DoubleEventArgs : EventArgs
    {
        public double Value { get; set; }
        public EventStarter Starter { get; set; }
        public EventHandler<DoubleEventArgs> Handler { get; set; }
    }
}
