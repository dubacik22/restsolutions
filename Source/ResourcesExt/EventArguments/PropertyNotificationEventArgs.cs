﻿using System.ComponentModel;

namespace ResourcesExt.EventArguments
{
    public class PropertyNotificationEventArgs : PropertyChangedEventArgs
    {
        public object NewValue { get; }
        public object OldValue { get; }

        public PropertyNotificationEventArgs(string propertyName)
          : this(propertyName, (object)null, (object)null)
        {
        }

        public PropertyNotificationEventArgs(string propertyName, object oldValue, object newValue)
          : base(propertyName)
        {
            this.OldValue = oldValue;
            this.NewValue = newValue;
        }
    }
}
