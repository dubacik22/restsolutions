﻿using System.ComponentModel;

namespace ResourcesExt.EventArguments
{
    public class PropertyChangedEventArgsEx : PropertyChangedEventArgs
    {
        public object OldValue { get; set; }
        public object NewValue { get; set; }

        public PropertyChangedEventArgsEx(string propertyName)
          : base(propertyName)
        {
        }

        public PropertyChangedEventArgsEx(string propertyName, object OldValue, object NewValue)
          : base(propertyName)
        {
            this.OldValue = OldValue;
            this.NewValue = NewValue;
        }
    }
}
