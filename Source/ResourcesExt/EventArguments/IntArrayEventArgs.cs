﻿using System;

namespace ResourcesExt.EventArguments
{
    public class IntArrayEventArgs : EventArgs
    {
        public int[] Value { get; set; }
        public EventStarter Starter { get; set; }
    }
}
