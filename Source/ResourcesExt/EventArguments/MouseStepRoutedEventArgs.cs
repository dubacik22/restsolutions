﻿using System.Windows;

namespace ResourcesExt.EventArguments
{
    public class MouseStepRoutedEventArgs : RoutedEventArgs
    {
        public object Value { get; set; }
        public double Delta { get; set; }
        public bool Success { get; set; }
        public new bool Handled
        {
            get => base.Handled;
            set => base.Handled = value;
        }

        public MouseStepRoutedEventArgs(RoutedEvent routedEvent)
          : base(routedEvent)
        {
            this.Handled = false;
        }
    }
}
