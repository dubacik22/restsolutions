﻿using System.ComponentModel;

namespace ResourcesExt.EventArguments
{
    public class CancelPropertyNotificationEventArgs : PropertyChangingEventArgs
    {
        public bool Cancel { get; set; }
        public object NewValue { get; }
        public object OldValue { get; }

        public CancelPropertyNotificationEventArgs(string propertyName)
          : base(propertyName)
        {
        }

        public CancelPropertyNotificationEventArgs(
          string propertyName,
          object oldValue,
          object newValue)
          : base(propertyName)
        {
            this.OldValue = oldValue;
            this.NewValue = newValue;
        }
    }
}
