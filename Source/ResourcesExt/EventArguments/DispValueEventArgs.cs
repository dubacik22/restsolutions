﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResourcesExt.EventArguments
{
    public class DispValueEventArgs : EventArgs
    {
        public DispValue Value { get; set; }
        public DispValue OldValue { get; set; }
        public EventStarter Starter { get; set; }
        public bool Handled { get; set; } = false;
        public bool Success { get; set; } = false;
        public EventHandler<DispValueEventArgs> Handler { get; set; }
    }
}
