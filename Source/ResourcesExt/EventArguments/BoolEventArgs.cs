﻿using System;

namespace ResourcesExt.EventArguments
{
    public class BoolEventArgs : EventArgs
    {
        public bool Value { get; set; }
        public EventStarter Starter { get; set; }
        public EventHandler<BoolEventArgs> Handler { get; set; }
    }
}
