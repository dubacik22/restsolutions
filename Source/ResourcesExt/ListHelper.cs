﻿using System.Collections.Generic;
using System.Linq;

namespace ResourcesExt
{
    public static class ListHelper
    {
        public static bool ListEquals<T>(IEnumerable<T> f1, IEnumerable<T> f2, IEqualityComparer<T> comparer = null)
        {
            if (ReferenceEquals(f1, f2)) return true;
            if (f1 is null) return false;
            if (f2 is null) return false;
            if (comparer is null) return Enumerable.SequenceEqual(f1, f2);
            else return Enumerable.SequenceEqual(f1, f2, comparer);
        }

        public static void ExchangeItems<T>(this IList<T> where, int index1, int index2)
        {
            T i1 = where[index1];
            T i2 = where[index2];
            where[index1] = i2;
            where[index2] = i1;
        }

        public static void ExchangeItems<T>(IList<T> l1, IList<T> l2)
        {
            var dl1 = new List<T>(l1);
            var dl2 = new List<T>(l2);
            l1.Clear();
            l2.Clear();
            foreach (var item2 in dl2) l1.Add(item2);
            foreach (var item1 in dl1) l2.Add(item1);
        }

        public static int IndexOfReference<T>(this IList<T> where, T what)
        {
            for (int i = 0; i < where.Count; i++)
            {
                if (ReferenceEquals(where[i], what)) return i;
            }
            return -1;
        }

        public static int IndexOf<T>(this IList<T> where, T what, IEqualityComparer<T> comparer)
        {
            for (int i = 0; i < where.Count; i++)
            {
                if (comparer.Equals(where[i], what)) return i;
            }
            return -1;
        }

        public static void RemoveList<T>(this IList<T> from, IList<T> what)
        {
            foreach (var whatItem in what)
            {
                var fromIndex = from.IndexOf(whatItem);
                if (fromIndex >= 0) from.RemoveAt(fromIndex);
            }
        }

        public static void RemoveList<T>(this IList<T> from, IList<T> what, IEqualityComparer<T> comparer)
        {
            foreach (var whatItem in what)
            {
                var fromIndex = from.IndexOf(whatItem, comparer);
                if (fromIndex >= 0) from.RemoveAt(fromIndex);
            }
        }
    }
}
