﻿namespace ResourcesExt
{
    public interface ICopyable
    {
        object MakeCopy();
    }
    public interface ICopyable<T> : ICopyable
    {
        new T MakeCopy();
    }
}
