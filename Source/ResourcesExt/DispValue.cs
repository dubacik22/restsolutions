﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ResourcesExt
{
    /// dispvalue obsahuje 3 hodnoty.
    /// Value - ciselna hodnota ktoru chcem vyjadrit
    /// decPlaces - pocet desatinnych miest na ktore chcem zobrazit hodnotu Value
    /// dispExp - exponent s ktorym zobrazujem hodnotu. nevplyva na Value a ani na decPlaces.
    ///           vplyva iba na zobrazenie, teda na dispValue a dispDecPlaces
    /// -----------------------------------------------------------------------------
    /// | Value | decPlaces | dispExp | vyjadrenie bez dispExp | konecne vyjadrenie |
    /// | 0.01  | 3         | 0       | 0.010                  | 0.010              |
    /// | 0.01  | 3         | 3       | 0.010                  | 0.000010           |
    /// | 100   | 2         | 3       | 100.00                 | 0.1                |
    /// <summary>
    /// </summary>
    [Serializable]
    public class DispValue : IComparable<DispValue>, IComparable, ICopyable, IEquatable<DispValue>, IXmlSerializable
    {
        public static List<DispExp> exps = new List<DispExp>
        {
            new DispExp("Y", 24),
            new DispExp("Z", 21),
            new DispExp("E", 18),
            new DispExp("P", 15),
            new DispExp("T", 12),
            new DispExp("G", 9),
            new DispExp("M", 6),
            new DispExp("k", 3),
            new DispExp("h", 2),
            new DispExp("D", 1),
            new DispExp(string.Empty, 0, true),
            new DispExp("-", 0),
            new DispExp(null, 0),
            new DispExp("d", -1),
            new DispExp("c", -2),
            new DispExp("%", -2),
            new DispExp("m", -3),
            new DispExp("u", -6),
            new DispExp("µ", -6),
            new DispExp("ppm", -6),
            new DispExp("n", -9),
            new DispExp("p", -12),
            new DispExp("f", -15),
            new DispExp("a", -18),
            new DispExp("z", -21),
            new DispExp("y", -24)
        };

        [XmlAttribute]
        public double Value { get; set; }
        [XmlAttribute]
        public int? DecPlaces { get; set; } = null;
        [XmlAttribute]
        public int DispExp { get; set; }

        [XmlIgnore]
        public double DisplayValue
        {
            get => this.Value / Math.Pow(10.0, (double)this.DispExp);
            set => this.Value = value * Math.Pow(10.0, (double)this.DispExp);
        }

        [XmlIgnore]
        public double DisplayValueWDecPl
        {
            get
            {
                var num = this.DisplayValue;
                var dispDecPlaces = this.GetPositiveDispDecPlaces();
                if (dispDecPlaces.HasValue) return Math.Round(num, dispDecPlaces.Value);
                return num;
            }
        }

        [XmlIgnore]
        public int? DisplayDecPlaces
        {
            get => this.DecPlaces + this.DispExp;
            set => this.DecPlaces = value - this.DispExp;
        }

        public DispValue()
        {
            this.Reset();
        }

        public DispValue(DispValue cpyValue)
        {
            this.CopyFrom(cpyValue);
        }

        public DispValue(double value, int? decimalPlaces, int displayExponent)
        {
            this.Set(value, decimalPlaces, displayExponent);
        }

        public static bool operator ==(DispValue a, DispValue b)
        {
            return DispValue.Equals(a, b);
        }

        public static bool operator !=(DispValue a, DispValue b)
        {
            return !DispValue.Equals(a, b);
        }

        public static bool ValueEquals(DispValue a, DispValue b)
        {
            if (object.ReferenceEquals(a, b)) return true;
            if (a is null && b is null) return true;
            if (a is null || b is null) return false;
            if (double.IsNaN(a.Value) && double.IsNaN(b.Value)) return true;
            return a.Value == b.Value;
        }

        public static bool Equals(DispValue a, DispValue b)
        {
            if (!DispValue.ValueEquals(a, b)) return false;
            if (object.ReferenceEquals(a, b)) return true;
            if (a is null && b is null) return true;
            if (a is null || b is null) return false;
            if (a.DecPlaces == b.DecPlaces) return a.DispExp == b.DispExp;
            return false;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is DispValue dv)) return false;
            return DispValue.Equals(this, dv);
        }

        public bool Equals(DispValue dv)
        {
            return DispValue.Equals(this, dv);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator <(DispValue a, DispValue b)
        {
            if (object.ReferenceEquals(a, b) || a is null || b is null) return false;
            return a.Value < b.Value;
        }

        public static bool operator <=(DispValue a, DispValue b)
        {
            return !(b < a);
        }

        public static bool operator >(DispValue a, DispValue b)
        {
            return b < a;
        }

        public static bool operator >=(DispValue a, DispValue b)
        {
            return !(a < b);
        }
        #region iComparable
        public int CompareTo(DispValue dv)
        {
            if (dv is null)
                throw new InvalidOperationException("Cannot compare to null");
            return this.Value.CompareTo(dv.Value);
        }

        public int CompareTo(object obj)
        {
            if (obj is null)
                throw new InvalidOperationException("Cannot compare to null");
            if ((obj as DispValue) == null)
                throw new InvalidOperationException("Cannot compare dispValue to object of type " + obj.GetType().ToString());
            return this.CompareTo((DispValue)obj);
        }

        #endregion

        public static void AdaptDecPlacesToValue(DispValue dv)
        {
            dv.DisplayDecPlaces = dv.DisplayValue.CntDecPl();
        }

        public static bool IsNullOrNaN(DispValue value)
        {
            return value is null || double.IsNaN(value.Value);
        }

        public static DispValue SqSum(DispValue a, DispValue b)
        {
            if (a is null) return new DispValue();
            if (b is null) return new DispValue();
            var dp = GetBiggerDecPl(a.DecPlaces, b.DecPlaces);
            return new DispValue(a.Value * a.Value + b.Value * b.Value, dp, a.DispExp > b.DispExp ? a.DispExp : b.DispExp);
        }

        public static DispValue HalfSqSum(DispValue a, DispValue b)
        {
            if (a is null) return new DispValue();
            if (b is null) return new DispValue();
            var dp = GetBiggerDecPl(a.DecPlaces, b.DecPlaces);
            return new DispValue(a.Value + b.Value * b.Value, dp, a.DispExp > b.DispExp ? a.DispExp : b.DispExp);
        }

        public static DispValue operator +(DispValue a, DispValue b)
        {
            if (a is null) return new DispValue();
            if (b is null) return new DispValue();
            var dp = GetBiggerDecPl(a.DecPlaces, b.DecPlaces);
            return new DispValue(a.Value + b.Value, dp, a.DispExp > b.DispExp ? a.DispExp : b.DispExp);
        }

        public static DispValue operator -(DispValue a, DispValue b)
        {
            if (a is null) return new DispValue();
            if (b is null) return new DispValue();
            var dp = GetBiggerDecPl(a.DecPlaces, b.DecPlaces);
            return new DispValue(a.Value - b.Value, dp, a.DispExp < b.DispExp ? a.DispExp : b.DispExp);
        }

        public static DispValue operator /(DispValue a, DispValue b)
        {
            if (a is null) return new DispValue();
            var ret = new DispValue(a);
            if (double.IsNaN(a.Value)) return ret;
            if (double.IsNaN(b.Value)) return ret;
            ret.Value = a.Value / b.Value;
            ret.DecPlaces = null;
            if (!a.DecPlaces.HasValue || !b.DecPlaces.HasValue) return ret;
            var multDecPlcs = a.DecPlaces.Value * b.DecPlaces.Value;
            var resDecPlcs = ret.Value.CntDecPl();
            if (resDecPlcs > multDecPlcs) ret.DecPlaces = multDecPlcs;
            return ret;
        }

        public static DispValue operator /(DispValue a, double b)
        {
            if (a is null) return new DispValue();
            var ret = new DispValue(a);
            if (double.IsNaN(a.Value)) return ret;
            ret.Value = a.Value / b;
            ret.DecPlaces = null;
            return ret;
        }

        public static DispValue operator *(DispValue a, DispValue b)
        {
            if (a is null) return new DispValue();
            var ret = new DispValue(a);
            if (double.IsNaN(a.Value)) return ret;
            if (double.IsNaN(b.Value)) return ret;
            ret.Value = a.Value * b.Value;
            ret.DecPlaces = null;
            if (!a.DecPlaces.HasValue || !b.DecPlaces.HasValue) return ret;
            var multDecPlcs = a.DecPlaces.Value * b.DecPlaces.Value;
            var resDecPlcs = ret.Value.CntDecPl();
            if (resDecPlcs > multDecPlcs) ret.DecPlaces = multDecPlcs;
            return ret;
        }

        public static DispValue operator *(DispValue a, double b)
        {
            if (a is null) return new DispValue();
            var ret = new DispValue(a);
            if (double.IsNaN(a.Value)) return ret;
            ret.Value = a.Value * b;
            ret.DecPlaces = null;
            return ret;
        }

        public void Reset()
        {
            this.Value = double.NaN;
            this.DecPlaces = null;
            this.DispExp = 0;
        }

        public void Set(double val, int? dp, int dExp)
        {
            this.Value = val;
            this.DecPlaces = dp;
            this.DispExp = dExp;
        }

        public void CopyFrom(DispValue from)
        {
            if (from is null) this.Reset();
            else
            {
                this.Value = from.Value;
                this.DecPlaces = from.DecPlaces;
                this.DispExp = from.DispExp;
            }
        }

        public string ExpToString()
        {
            var exp = this.DispExp;
            foreach (var _exp in exps)
            {
                if (_exp.Exp == exp) return _exp.StrExp;
            }
            return string.Empty;
        }

        public override string ToString()
        {
            if (double.IsNaN(this.Value)) return string.Empty;
            var decPl = this.GetPositiveDispDecPlaces();
            if (decPl.HasValue) return string.Format("{0:F" + decPl.Value + "}", this.DisplayValue);
            return this.DisplayValue.ToString();
        }

        public string ToScientificString()
        {
            if (double.IsNaN(this.Value)) return string.Empty;
            string str = string.Format("{0:0.#########e-00;;0}", this.DisplayValue);
            if (str.EndsWith("e00")) str = str.Substring(0, str.Length - 3);
            return str;
        }

        public string ToStringWExp()
        {
            var ret = new StringBuilder();
            ret.Append(this.ToString());
            var exp = this.ExpToString();
            if (!string.IsNullOrEmpty(exp))
            {
                ret.Append(" ");
                ret.Append(this.ExpToString());
            }
            return ret.ToString();
        }

        public string ToScientificStringWExp()
        {
            var ret = new StringBuilder();
            ret.Append(this.ToScientificString());
            var exp = this.ExpToString();
            if (!string.IsNullOrEmpty(exp))
            {
                ret.Append(" ");
                ret.Append(this.ExpToString());
            }
            return ret.ToString();
        }

        public static implicit operator double(DispValue dv)
        {
            if (dv is null) return double.NaN;
            return dv.Value;
        }

        public static implicit operator DispValue(int val)
        {
            return new DispValue(val, 0, 0);
        }

        public static implicit operator DispValue(double val)
        {
            return new DispValue(val, null, 0);
        }

        public object MakeCopy()
        {
            return new DispValue(this);
        }
        public static DispValue MakeCopy(DispValue from)
        {
            if (from is null) return null;
            return (DispValue)from.MakeCopy();
        }
        private static int? GetBiggerDecPl(int? a, int? b)
        {
            if (!a.HasValue) return b;
            if (!b.HasValue) return a;
            return a.Value > b.Value ? a.Value : b.Value;
        }
        private static int? GetSmallerDecPl(int? a, int? b)
        {
            if (!a.HasValue) return b;
            if (!b.HasValue) return a;
            return a.Value < b.Value ? a.Value : b.Value;
        }
        private int? GetPositiveDispDecPlaces()
        {
            var decPl = this.DisplayDecPlaces;
            if (!decPl.HasValue) return null;
            return decPl.Value < 0 ? 0 : decPl.Value;
        }

        #region xml
        public XmlSchema GetSchema()
        {
            return null;
        }
        public void ReadXml(XmlReader reader)
        {
            var strValue = reader.GetAttribute(nameof(this.Value));
            strValue = strValue.Replace(',', '.');
            double.TryParse(strValue, NumberStyles.Any, CultureInfo.InvariantCulture, out double value);
            this.Value = value;
            strValue = reader.GetAttribute(nameof(this.DecPlaces));
            if (!string.IsNullOrEmpty(strValue))
            {
                int.TryParse(strValue, out int decPlaces);
                this.DecPlaces = decPlaces;
            }
            strValue = reader.GetAttribute(nameof(this.DispExp));
            int.TryParse(strValue, out int dispExp);
            this.DispExp = dispExp;
            reader.Read();
        }
        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString(nameof(this.Value), this.Value.ToString(CultureInfo.InvariantCulture));
            if (this.DecPlaces is object) writer.WriteAttributeString(nameof(this.DecPlaces), this.DecPlaces.ToString());
            writer.WriteAttributeString(nameof(this.DispExp), this.DispExp.ToString());
        }

        #endregion
    }
}
