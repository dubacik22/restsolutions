﻿namespace ResourcesExt.DispValueParser
{
    public interface IExponentFetcher
    {
        int FetchExponent(string text);
    }
}
