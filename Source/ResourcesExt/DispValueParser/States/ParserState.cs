﻿namespace ResourcesExt.DispValueParser.States
{
    internal abstract class ParserState
    {
        public abstract ParserState OnComa(char ch);
        public abstract ParserState OnDot(char ch);
        public abstract ParserState OnSpace(char ch);
        public abstract ParserState OnDigit(char ch);
        public abstract ParserState OnOther(char ch);
    }
}
