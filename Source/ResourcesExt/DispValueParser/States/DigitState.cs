﻿namespace ResourcesExt.DispValueParser.States
{
    internal class DigitState : ParserState
    {
        public override ParserState OnDigit(char ch)
        {
            Parser.context.PushToContext(ch);
            return this;
        }
        public override ParserState OnComa(char ch)
        {
            Parser.context.PushToContext('.');
            return this;
        }
        public override ParserState OnDot(char ch)
        {
            Parser.context.PushToContext('.');
            return this;
        }
        public override ParserState OnSpace(char ch)
        {
            Parser.context.CreateNum();
            return new CharState();
        }
        public override ParserState OnOther(char ch)
        {
            Parser.context.CreateNum();
            Parser.context.PushToContext(ch);
            return new CharState();
        }
    }
}
