﻿namespace ResourcesExt.DispValueParser.States
{
    internal class CharState : ParserState
    {
        public override ParserState OnDigit(char ch)
        {
            Parser.context.PushToContext(ch);
            return this;
        }
        public override ParserState OnComa(char ch)
        {
            Parser.context.PushToContext(ch);
            return this;
        }
        public override ParserState OnDot(char ch)
        {
            Parser.context.PushToContext(ch);
            return this;
        }
        public override ParserState OnSpace(char ch)
        {
            if (Parser.context.IsEmptyBuilder()) return this;
            Parser.context.PushToContext(ch);
            return this;
        }
        public override ParserState OnOther(char ch)
        {
            Parser.context.PushToContext(ch);
            return this;
        }
    }
}
