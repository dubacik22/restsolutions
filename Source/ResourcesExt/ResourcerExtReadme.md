Library with other unclassifiable stuff like:

DispValue
this is something like double, buth it have properties: Vaue, DecPlaces and DispExp
lets say that you want to store value that represents weight. and the value shoul be 128.432 exactly. lets say that this walue will have units. like "g". and you want to display this value in "kg", with just 2 decimal places. so you want to display 0.13
so you can store one DispValue like DispValu.Value=128.342, DispValue.DecPlaces=-1, DispValue.DispExp=3
so that means, you take your value, display it with desired decimal places (128.342 with -1 decimal place, is basically 130) and you shifts decimal point by 3 places to left. (130 will then be 0.13)
you can get this representation with ToString() method
the DispValue have allso another properties like:
DisplayValue - this just shifts decimal places
DisplayValueWDecPl - this just shifts decimal places and restricts decimal places (rounds)
DisplayDecPlaces - when you wand to show 5 places and shift point by 3 places, result will be 2 (you will have jus 2 places in resulting value)
methods that returns string:
ToString() returns string that is represented by Value, DecPlaces and DispExp
ToScientificString() returns same as ToString but when ToString returns 0.002, this method returns 2e-3
the DispValue have programmed its own arithmetic to manipulate with its properties (DecPlaces and DispExp):
lets assume that you multiply one DispValue(1.1122, 2, 0) with another DispValue(1.1122, 2, 0)
multiplication of 1.1122 * 1.1122 is 1.23698884
but multiplication of 1.11 * 1.11 is 1.2321
that means, that you probably want to have correct arithmetic result (1.23698884) but you want to display it with 4 decimal places (like 1.2321)
same with division

----------------------------------------------------------------------------------

ListHelper methods and extension methods
ListEquals<T>(IEnumerable<T> f1, IEnumerable<T> f2, IEqualityComparer<T> comparer = null) - checks if items of f1 is same as items in f2 with comparer
ExchangeItems<T>(this IList<T> where, int index1, int index2) - exchange item between each other
ExchangeItems<T>(IList<T> l1, IList<T> l2) - exchange items in lists
IndexOfReference<T>(this IList<T> where, T what) - find index of item in list (checks only by object.ReferenceEquaks())
IndexOf<T>(this IList<T> where, T what, IEqualityComparer<T> comparer) - find index of item in list (checks with comparer)
RemoveList<T>(this IList<T> from, IList<T> what) - removes list from list
RemoveList<T>(this IList<T> from, IList<T> what, IEqualityComparer<T> comparer) - removes list from list (checks with comparer)

----------------------------------------------------------------------------------

RelayCommand
just another implementation of ICommand



and few other stuff that is better to 