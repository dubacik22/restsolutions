﻿// See https://aka.ms/new-console-template for more information
using DisplayValue;
using Equationer;
using Test;

var parser = new EquationParser
{
    FetchFunction = name => new DummyFunc(),
    FetchVariable = name => new DummyValue(),
};

Console.WriteLine("Hello, World!");
var dd = new DispValue(60, 16, 0);
var aa = dd.DisplayValueWDecPl;

string input;
ICalculatable ret;
string retstr;
object res;

input = "f1(0, (a == 0) ? a : f2(0, -3))";
//input = "(d == 0) ? 0 : -1";
ret = parser.Parse(input);
res = ret.GetCalculatedValue();

input = "Dv((test.ratesGroup[$groupI_ratesGroup].rateLen == 0) ? NaN : (test.ratesGroup[$groupI_ratesGroup].rateTime / test.ratesGroup[$groupI_ratesGroup].rateLen), null, 0)";
input = "(d == 0) ? 0 : -1";
//input = "1 + -1";
ret = parser.Parse(input);
res = ret.GetCalculatedValue();
input = "test.pointsGroup[$groupI_pointsGroup:0]";
ret = parser.Parse(input);
res = ret.GetCalculatedValue();
input = "true and (true and false and true and true and !false)";
ret = parser.Parse(input);

input = "(a) ? (b) ? f1() : f2('t') : f3(''l)";
ret = parser.Parse(input);

input = "(a)?(b)?f1():f2('t'):f3(''l)";
ret = parser.Parse(input);


input = "(a+1)>1";
ret = parser.Parse(input);

input = "a gt 1";
ret = parser.Parse(input);

input = "fa(a>1)";
ret = parser.Parse(input);

input = "(a) ? (b > (c)) ? Dummy(GetGreen(), 1) : Dummy(GetErr('cas je primoc maly'), 2) : Dummy(GetOk(), 3)";
ret = parser.Parse(input);

input = "(a) ? (b > (c)) ? Dummy(GetGreen(), 1) : Dummy(GetErr('cas je primoc maly'), 2) : Dummy(GetOk(), 3)";
ret = parser.Parse(input);

var a = 0;