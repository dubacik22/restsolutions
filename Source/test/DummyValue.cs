﻿using Equationer;

namespace Test
{
    public class DummyValue : IGetVariable
    {
        public object Variable { get => double.NaN; }
    }
}
