﻿using Equationer;
using System.Collections.Generic;
using System.Linq;

namespace Test
{
    public class DummyFunc : IFunction
    {
        public object Calculate(IEnumerable<object> arguments)
        {
            var args = arguments.ToList();
            var pos = args[0] as double? ?? 0;
            if (args.Count <= pos) return args.First();
            var ret = args[(int)pos + 1];
            return ret;
        }
    }
}
