﻿namespace ErrorResult
{
    public static class ErrorLevelHelper
    {
        public static void CopyFrom(this ErrorLevel errorLevel, ErrorLevel from)
        {
            if (from is null || errorLevel is null) return;
            errorLevel.ErrLevel = from.ErrLevel;
            errorLevel.Message = from.Message;
        }

        public static void SetErr(this ErrorLevel errorLevel, string message)
        {
            if (errorLevel is null) return;
            errorLevel.ErrLevel = ErrorLevelEnum.Err;
            errorLevel.Message = message;
        }
        public static void SetErr(this ErrorLevel errorLevel)
        {
            if (errorLevel is null) return;
            errorLevel.ErrLevel = ErrorLevelEnum.Err;
        }

        public static void SetProblem(this ErrorLevel errorLevel, string message)
        {
            if (errorLevel is null) return;
            errorLevel.ErrLevel = ErrorLevelEnum.Problem;
            errorLevel.Message = message;
        }
        public static void SetProblem(this ErrorLevel errorLevel)
        {
            if (errorLevel is null) return;
            errorLevel.ErrLevel = ErrorLevelEnum.Problem;
        }

        public static void SetOk(this ErrorLevel errorLevel)
        {
            if (errorLevel is null) return;
            errorLevel.ErrLevel = ErrorLevelEnum.Ok;
        }
        public static void SetOk(this ErrorLevel errorLevel, string message)
        {
            if (errorLevel is null) return;
            errorLevel.ErrLevel = ErrorLevelEnum.Ok;
            errorLevel.Message = message;
        }

        public static ErrorLevel GetErr(string message)
        {
            return new ErrorLevel(ErrorLevelEnum.Err)
            {
                Message = message
            };
        }
        public static ErrorLevel GetErr()
        {
            return new ErrorLevel(ErrorLevelEnum.Err);
        }

        public static ErrorLevel GetProblem(string message)
        {
            return new ErrorLevel(ErrorLevelEnum.Problem)
            {
                Message = message
            };
        }
        public static ErrorLevel GetProblem()
        {
            return new ErrorLevel(ErrorLevelEnum.Problem);
        }

        public static ErrorLevel GetOk(string message)
        {
            return new ErrorLevel
            {
                Message = message
            };
        }
        public static ErrorLevel GetOk()
        {
            return new ErrorLevel();
        }
    }
}
