﻿namespace ErrorResult
{
    public class ResultValue<T> : ErrorLevel
    {
        private T _value;

        public T Value
        {
            get
            {
                return this._value;
            }
            set
            {
                this._value = value;
            }
        }
    }
}