﻿namespace ErrorResult
{
    public static class ResultValueHelper
    {
        public static void CopyFrom<T>(this ResultValue<T> resVal, ResultValue<T> from)
        {
            if ((ErrorLevel)from == (ErrorLevel)null || (ErrorLevel)resVal == (ErrorLevel)null)
                return;
            resVal.ErrLevel = from.ErrLevel;
            resVal.Message = from.Message;
            resVal.Value = from.Value;
        }

        public static ResultValue<T> Get<T>(ErrorLevel errLevel, T value)
        {
            ResultValue<T> errorLevel = new ResultValue<T>();
            errorLevel.CopyFrom(errLevel);
            errorLevel.Value = value;
            return errorLevel;
        }

        public static ResultValue<T> Get<T>(ErrorLevel errLevel)
        {
            ResultValue<T> errorLevel = new ResultValue<T>();
            errorLevel.CopyFrom(errLevel);
            return errorLevel;
        }

        public static ResultValue<T> GetErr<T>(string message)
        {
            ResultValue<T> errorLevel = new ResultValue<T>();
            errorLevel.SetErr(message);
            return errorLevel;
        }

        public static ResultValue<T> GetErr<T>()
        {
            ResultValue<T> errorLevel = new ResultValue<T>();
            errorLevel.SetErr();
            return errorLevel;
        }

        public static ResultValue<T> GetProblem<T>(string message)
        {
            ResultValue<T> errorLevel = new ResultValue<T>();
            errorLevel.SetProblem(message);
            return errorLevel;
        }

        public static ResultValue<T> GetProblem<T>(string message, T value)
        {
            ResultValue<T> errorLevel = new ResultValue<T>();
            errorLevel.SetProblem(message);
            errorLevel.Value = value;
            return errorLevel;
        }

        public static ResultValue<T> GetOk<T>(T value)
        {
            return new ResultValue<T>() { Value = value };
        }
    }
}
