﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
using System.Xml.Serialization;

namespace ErrorResult
{
    [Serializable]
    public class ErrorLevel : IEquatable<ErrorLevel>, INotifyPropertyChanged
    {
        private ErrorLevelEnum errLevel;
        [XmlAttribute(nameof(ErrLevel))]
        public ErrorLevelEnum ErrLevel
        {
            get => this.errLevel;
            set => this.SetField(ref this.errLevel, value, nameof(ErrLevel));
        }

        private string message;
        [XmlAttribute(nameof(Message))]
        public string Message
        {
            get => this.message;
            set => this.SetField(ref this.message, value, nameof(Message));
        }

        public bool IsOk
        {
            get => this.errLevel == ErrorLevelEnum.Ok;
        }

        public bool IsErr
        {
            get => this.errLevel == ErrorLevelEnum.Err;
        }

        public ErrorLevel()
        {
        }
        public ErrorLevel(ErrorLevelEnum initValue)
        {
            this.errLevel = initValue;
        }

        public ErrorLevel(ErrorLevel initValue)
        {
            this.errLevel = initValue.errLevel;
            this.message = initValue.message;
        }

        public static bool operator ==(ErrorLevel a, ErrorLevel b)
        {
            return ErrorLevel.Equals(a, b);
        }

        public static bool operator !=(ErrorLevel a, ErrorLevel b)
        {
            return !ErrorLevel.Equals(a, b);
        }

        public static bool Equals(ErrorLevel a, ErrorLevel b)
        {
            if (object.ReferenceEquals(a, b)) return true;
            if (a is null || b is null) return false;
            return a.errLevel == b.errLevel && a.message == b.message;
        }

        public static ErrorLevel operator +(ErrorLevel a, ErrorLevel b)
        {
            if (a is null && b is null) return new ErrorLevel();
            if (a is null) return b;
            if (b is null) return a;
            ErrorLevel errorLevel = new ErrorLevel();
            if (a.errLevel > b.errLevel)
            {
                errorLevel.errLevel = a.errLevel;
                errorLevel.message = a.message;
            }
            else
            {
                errorLevel.errLevel = b.errLevel;
                errorLevel.message = b.message;
            }
            return errorLevel;
        }
        public event PropertyChangedEventHandler PropertyChanged;

        protected bool SetField<FieldType>(ref FieldType field, FieldType value, string propertyName)
        {
            if (EqualityComparer<FieldType>.Default.Equals(field, value))
                return false;
            field = value;
            this.OnPropertyChanged(propertyName);
            return true;
        }

        protected virtual void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged == null) return;
            propertyChanged((object)this, new PropertyChangedEventArgs(name));
        }

        public override string ToString()
        {
            return this.errLevel.ToString() + ": " + this.message;
        }

        public override bool Equals(object obj)
        {
            return ErrorLevel.Equals(this, obj as ErrorLevel);
        }

        public bool Equals(ErrorLevel other)
        {
            return ErrorLevel.Equals(this, other);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
