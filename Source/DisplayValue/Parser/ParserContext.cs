﻿using System.Text;

namespace DisplayValue.Parser
{
    internal class ParserContext
    {
        private readonly StringBuilder contextBuilder = new StringBuilder();
        private readonly IExponentFetcher exponentFetcher;
        private readonly Parser parser;
        private string NumberStr { get; set; } = null;
        public DispValue ResultValue { get; private set; }

        public ParserContext(IExponentFetcher exponentFetcher, Parser parser)
        {
            this.exponentFetcher = exponentFetcher;
            this.parser = parser;
        }
        public void PushToContext(char ch)
        {
            this.contextBuilder.Append(ch);
        }
        public void CreateNum()
        {
            this.NumberStr = this.contextBuilder.ToString();
            this.contextBuilder.Clear();
        }
        public bool IsEmptyBuilder()
        {
            return this.contextBuilder.Length <= 0;
        }
        public void Finish()
        {
            this.ResultValue = this.GetResultValue();
        }
        private DispValue GetResultValue()
        {
            DispValue ret;
            if (string.IsNullOrEmpty(this.NumberStr))
            {
                this.CreateNum();
                if (string.IsNullOrEmpty(this.NumberStr)) return null;
                ret = parser.Parse(this.NumberStr, string.Empty);
                if (ret is object) return ret;
                return new DispValue();
            }
            ret = parser.Parse(this.NumberStr, string.Empty);
            if (ret is null) ret = new DispValue();
            if (this.IsEmptyBuilder()) return ret;
            if (this.exponentFetcher is null) return null;
            var text = this.contextBuilder.ToString();
            var exponent = this.exponentFetcher.FetchExponent(text);
            var parsed = ret;
            return new DispValue
            {
                DispExp = exponent,
                DisplayValue = parsed.Value,
                DisplayDecPlaces = parsed.DecPlaces,
            };
        }
    }
}
