﻿using DisplayValue.Parser.States;
using System;
using System.Globalization;

namespace DisplayValue.Parser
{
    public class Parser
    {
        #region Class data
        const char mySpaceChar = ' ';
        const char myComaChar = ',';
        const char myDotChar = '.';
        internal static ParserContext context;

        #endregion
        public DispValue Parse(Tuple<string, string> value)
        {
            return this.Parse(value.Item1, value.Item2);
        }
        public DispValue Parse(string text, IExponentFetcher exponentFetcher)
        {
            if (string.IsNullOrEmpty(text)) return null;
            Parser.context = new ParserContext(exponentFetcher, this);
            ParserState state = new DigitState();
            foreach (var ch in text) state = this.DoState(state, ch);
            Parser.context.Finish();
            return Parser.context.ResultValue;
        }
        public DispValue Parse(string str, string SIexp)
        {
            str = str?.Replace(',', '.').Trim();
            if (!double.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out double doubleResult)) return null;
            var num1 = str.IndexOf('.');
            var num2 = str.Length - (num1 < 0 ? str.Length : num1 + 1);
            var exp = this.ParseSiExp(SIexp);
            if (exp is null) return new DispValue(doubleResult, num2, 0);
            return new DispValue(0.0, 0, exp.Value)
            {
                DisplayValue = doubleResult,
                DisplayDecPlaces = num2
            };
        }
        public int? ParseSiExp(string SIexp)
        {
            if (SIexp is null) return null;
            foreach (var actExp in DispValue.exps)
            {
                if (actExp.StrExp == SIexp) return actExp.Exp;
            }
            return null;
        }
        public static int? TryParseLowerCaseSiExp(string SIexp)
        {
            if (SIexp is null) return null;
            SIexp = SIexp.ToLower();
            foreach (var actExp in DispValue.exps)
            {
                if (actExp.StrExp?.ToLower() == SIexp) return actExp.Exp;
            }
            return null;
        }




        private ParserState DoState(ParserState state, char ch)
        {
            if ((ch >= '0') && (ch <= '9')) return state.OnDigit(ch);
            switch (ch)
            {
                case Parser.mySpaceChar: return state.OnSpace(ch);
                case Parser.myDotChar: return state.OnDot(ch);
                case Parser.myComaChar: return state.OnComa(ch);
                default: return state.OnOther(ch);
            }
        }
    }
}
