﻿namespace DisplayValue.Parser
{
    public interface IExponentFetcher
    {
        int FetchExponent(string text);
    }
}
