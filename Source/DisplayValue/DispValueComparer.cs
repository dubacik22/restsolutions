﻿using System.Collections.Generic;

namespace DisplayValue
{
    public class DispValueComparer : IEqualityComparer<DispValue>
    {
        private bool compareJustValues;
        public bool Equals(DispValue a, DispValue b)
        {
            if (this.compareJustValues) return DispValue.ValueEquals(a, b);
            return DispValue.Equals(a, b);
        }

        public int GetHashCode(DispValue dv)
        {
            if (object.ReferenceEquals((object)dv, (object)null))
                return 0;
            return dv.Value.GetHashCode();
        }

        public DispValueComparer() { }
        public DispValueComparer(bool compareJustValues)
        {
            this.compareJustValues = compareJustValues;
        }
    }
}
