﻿using System;

namespace Messaging
{
    public interface IMessenger
    {
        void Register(string message, Action callback);
        void Register<T>(Action<T> callback);
        void Register<T>(string message, Action<T> callback);

        void NotifyColleagues(string message);
        void NotifyColleagues<T>(T parameter);
        void NotifyColleagues<T>(string message, T parameter);
    }
}
