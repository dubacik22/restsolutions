﻿using System;
using System.Reflection;

namespace Messaging
{
    /// <summary>
    /// This class is an implementation detail of the MessageToActionsMap class.
    /// </summary>
    internal class WeakAction
    {
        internal readonly Type ParameterType;
        private readonly Type delegateType;
        private readonly MethodInfo method;
        private readonly WeakReference targetRef;

        /// <summary>
        /// Constructs a WeakAction.
        /// </summary>
        /// <param name="target">The object on which the target method is invoked, or null if the method is static.</param>
        /// <param name="method">The MethodInfo used to create the Action.</param>
        /// <param name="parameterType">The type of parameter to be passed to the action. Pass null if there is no parameter.</param>
        internal WeakAction(object target, MethodInfo method, Type parameterType)
        {
            this.targetRef = target != null ? new WeakReference(target) : (WeakReference)null;

            this.method = method;

            this.ParameterType = parameterType;

            if (parameterType == null)
            {
                this.delegateType = typeof(Action);
            }
            else
            {
                this.delegateType = typeof(Action<>).MakeGenericType(parameterType);
            }
        }

        /// <summary>
        /// Creates a "throw away" delegate to invoke the method on the target, or null if the target object is dead.
        /// </summary>
        internal Delegate CreateAction()
        {
            // Rehydrate into a real Action object, so that the method can be invoked.
            if (this.targetRef == null)
            {
                return Delegate.CreateDelegate(this.delegateType, this.method);
            }
            try
            {
                object target = this.targetRef.Target;
                if (target != null)
                    return Delegate.CreateDelegate(this.delegateType, target, this.method);
            }
            catch
            {
            }
            return null;
        }
    }
}
