﻿using System.Collections.Generic;

namespace Messaging
{
    public static class MessengerFactory
    {
        private static Dictionary<string, IMessenger> messengers = new Dictionary<string, IMessenger>();

        public static IMessenger Get(string key)
        {
            if (messengers.ContainsKey(key)) return messengers[key];

            IMessenger ret = new Messenger();
            messengers.Add(key, ret);
            return ret;
        }

        public static IMessenger DefaultMessenger { get; } = MessengerFactory.Get(nameof(DefaultMessenger));
    }
}
