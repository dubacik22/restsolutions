﻿using System;

namespace Messaging
{
    /// <summary>
    /// Provides loosely-coupled messaging between
    /// various colleague objects.  All references to objects
    /// are stored weakly, to prevent memory leaks.
    /// </summary>
    class Messenger : IMessenger
    {
        private readonly MessageToActionsMap messageToActionsMap = new MessageToActionsMap();
        private readonly TypeToActionsMap typeToActionsMap = new TypeToActionsMap();

        public Messenger()
        {
        }


        /// <summary>
        /// Registers a callback method, with no parameter, to be invoked when a specific message is broadcasted.
        /// </summary>
        /// <param name="message">The message to register for.</param>
        /// <param name="callback">The callback to be called when this message is broadcasted.</param>
        public void Register(string message, Action callback)
        {
            if (string.IsNullOrEmpty(message))
                throw new ArgumentException("'message' cannot be null or empty.");

            if (callback == null)
                throw new ArgumentNullException(nameof(callback));

            this.messageToActionsMap.AddAction(message, callback.Target, callback.Method);
        }

        /// <summary>
        /// Registers a callback method, with a parameter, to be invoked when a specific message is broadcasted.
        /// </summary>
        /// <param name="callback">The callback to be called when this message is broadcasted.</param>
        public void Register<T>(Action<T> callback)
        {
            if (callback == null)
                throw new ArgumentNullException(nameof(callback));

            this.typeToActionsMap.AddAction(typeof(T), callback.Target, callback.Method);
        }

        /// <summary>
        /// Registers a callback method, with a parameter, to be invoked when a specific message is broadcasted.
        /// </summary>
        /// <param name="message">totally useless. you can pass it just for readaility</param>
        /// <param name="callback">The callback to be called when this message is broadcasted.</param>
        public void Register<T>(string message, Action<T> callback)
        {
            if (callback == null)
                throw new ArgumentNullException(nameof(callback));

            this.typeToActionsMap.AddAction(typeof(T), callback.Target, callback.Method);
        }

        /// <summary>
        /// Notifies all registered parties that a message is being broadcasted.
        /// </summary>
        /// <param name="parameter">The parameter to pass together with the message.</param>
        public void NotifyColleagues<T>(T parameter)
        {
            var actionType = typeof(T);
            //if (this.typeToActionsMap.HaveActionOfType(typeof(T)))
            //    throw new TargetParameterCountException(string.Format("dont have message for type {0}.", actionType));

            this.typeToActionsMap.GetActions(actionType)?.ForEach(action => action.DynamicInvoke(parameter));
        }

        /// <summary>
        /// Notifies all registered parties that a message is being broadcasted.
        /// </summary>
        /// <param name="message">totally useless. you can pass it just for readaility</param>
        /// <param name="parameter">The parameter to pass together with the message.</param>
        public void NotifyColleagues<T>(string message, T parameter)
        {
            var actionType = typeof(T);
            //if (this.typeToActionsMap.HaveActionOfType(typeof(T)))
            //    throw new TargetParameterCountException(string.Format("dont have message for type {0}.", actionType));

            this.typeToActionsMap.GetActions(actionType)?.ForEach(action => action.DynamicInvoke(parameter));
        }

        /// <summary>
        /// Notifies all registered parties that a message is being broadcasted.
        /// </summary>
        /// <param name="message">The message to broadcast.</param>
        public void NotifyColleagues(string message)
        {
            if (string.IsNullOrEmpty(message))
                throw new ArgumentException("'message' cannot be null or empty.");

            this.messageToActionsMap.GetActions(message)?.ForEach(action => action.DynamicInvoke());
        }


    }
}
