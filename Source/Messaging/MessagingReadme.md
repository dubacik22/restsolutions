just another messenger
this messenger is different in one little thing. in ordinary messenger yo can broadcast messages coupled to some type.
this messenger can broadcast messages coupled to some specific string. like:

Messenger.Register("someMessage", Action);
this action is called only in case when:
Messenger.NotifyColleagues("someMessage")

Messenger is acquired by:
MessengerFactory.Get(string) or MessengerFactory.Default