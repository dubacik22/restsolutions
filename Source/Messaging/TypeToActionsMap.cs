﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Messaging
{
    /// <summary>
    /// This class is an implementation detail of the Messenger class.
    /// </summary>
    internal class TypeToActionsMap
    {
        private readonly Dictionary<Type, List<WeakAction>> map = new Dictionary<Type, List<WeakAction>>();

        internal TypeToActionsMap()
        {
        }

        /// <summary>
        // Adds an action to the list.
        /// </summary>
        /// <param name="message">The message to register.</param>
        /// <param name="target">The target object to invoke, or null.</param>
        /// <param name="method">The method to invoke.</param>
        /// <param name="actionType">The type of the Action delegate.</param>
        internal void AddAction(Type messageType, object target, MethodInfo method)
        {
            if (messageType is null)
                throw new ArgumentNullException(nameof(messageType));
            if (method is null)
                throw new ArgumentNullException(nameof(method));

            lock (this.map)
            {
                if (!this.map.ContainsKey(messageType))
                    this.map[messageType] = new List<WeakAction>();
                this.map[messageType].Add(new WeakAction(target, method, messageType));
            }
        }

        /// <summary>
        /// Gets the list of actions to be invoked for the specified message
        /// </summary>
        /// <param name="messageType">The message to get the actions for</param>
        /// <returns>Returns a list of actions that are registered to the specified message</returns>
        internal List<Delegate> GetActions(Type messageType)
        {
            if (messageType is null)
                throw new ArgumentNullException(nameof(messageType));
            List<Delegate> delegateList;
            lock (this.map)
            {
                if (!this.map.ContainsKey(messageType))
                    return null;
                List<WeakAction> weakActionList = this.map[messageType];
                delegateList = new List<Delegate>(weakActionList.Count);
                for (int index = weakActionList.Count - 1; index > -1; --index)
                {
                    WeakAction weakAction = weakActionList[index];
                    if (weakAction is object)
                    {
                        Delegate action = weakAction.CreateAction();
                        if (action is object)
                            delegateList.Add(action);
                        else
                            weakActionList.Remove(weakAction);
                    }
                }
                if (weakActionList.Count == 0)
                    this.map.Remove(messageType);
            }
            delegateList.Reverse();
            return delegateList;
        }

        /// <summary>
        /// Check for existence of type
        /// </summary>
        /// <param name="messageType">The type to check.</param>
        /// <returns>true if any actions were registered for the type</returns>
        internal bool HaveActionOfType(Type messageType)
        {
            if (messageType == null)
                throw new ArgumentNullException(nameof(messageType));
            
            lock (this.map)
            {
                return this.map.ContainsKey(messageType);
            }
        }
    }
}
