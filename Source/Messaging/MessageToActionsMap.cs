﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Messaging
{
    /// <summary>
    /// This class is an implementation detail of the Messenger class.
    /// </summary>
    internal class MessageToActionsMap
    {
        private readonly Dictionary<string, List<WeakAction>> _map = new Dictionary<string, List<WeakAction>>();

        internal MessageToActionsMap()
        {
        }

        /// <summary>
        // Adds an action to the list.
        /// </summary>
        /// <param name="message">The message to register.</param>
        /// <param name="target">The target object to invoke, or null.</param>
        /// <param name="method">The method to invoke.</param>
        /// <param name="actionType">The type of the Action delegate.</param>
        internal void AddAction(string message, object target, MethodInfo method)
        {
            if (message is null)
                throw new ArgumentNullException(nameof(message));
            if (method is null)
                throw new ArgumentNullException(nameof(method));
            lock (this._map)
            {
                if (!this._map.ContainsKey(message))
                    this._map[message] = new List<WeakAction>();
                this._map[message].Add(new WeakAction(target, method, null));
            }
        }

        /// <summary>
        /// Gets the list of actions to be invoked for the specified message
        /// </summary>
        /// <param name="message">The message to get the actions for</param>
        /// <returns>Returns a list of actions that are registered to the specified message</returns>
        internal List<Delegate> GetActions(string message)
        {
            if (message is null)
                throw new ArgumentNullException(nameof(message));
            List<Delegate> delegateList;
            lock (this._map)
            {
                if (!this._map.ContainsKey(message))
                    return null;
                List<WeakAction> weakActionList = this._map[message];
                delegateList = new List<Delegate>(weakActionList.Count);
                for (int index = weakActionList.Count - 1; index > -1; --index)
                {
                    WeakAction weakAction = weakActionList[index];
                    if (weakAction is object)
                    {
                        Delegate action = weakAction.CreateAction();
                        if (action is object)
                            delegateList.Add(action);
                        else
                            weakActionList.Remove(weakAction);
                    }
                }
                if (weakActionList.Count == 0)
                    this._map.Remove(message);
            }
            delegateList.Reverse();
            return delegateList;
        }
    }
}
