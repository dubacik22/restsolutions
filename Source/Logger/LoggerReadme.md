library for creating and writing log messages

The Logger class is static for simplicity.
The logger distinguishes between multiple layers of logs:
Trace
Debug
Info
Result
Fatal

It is possible to create any number of target files for any layer. Logger writes messages to files according to:

file:    Trace Debug Info Result Fatal
message:
Trace      +
Debug      +     +
Info       +     +    +
Result     +     +    +     +
Fatal      +     +    +     +      +

at the beginning it is necessary to initialize some files (it is possible to initialize any number of any files):
Logger.CreateTraceFile(string path, string name);
Logger.CreateDebugFile(string path, string name);
Logger.CreateInfoFile(string path, string name);
Logger.CreateResultFile(string path, string name);
Logger.CreateFatalFile(string path, string name);

in case we want to automatically add to the end of logging messages \ n:
Logger.AddCrLf (true);

in case we want the logger to add a timestamp to the beginning of the log:
Logger.ShowTime (true);

log entry itself:
Logger.Trace ("trace message");
...
Logger.Fatal ("error message");

the log looks like this:

5/5/2016 14: 42: 15.481016 T: trace message
5/5/2016 14: 42: 16.001765 FATAL: error message
