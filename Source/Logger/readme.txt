kniznica pre vytvaranie a zapisovanie logovacich sprav

Trieda Logger je pre jednoduchost staticka.
Logger rozlisuje viac vrstiev logov:
Trace
Debug
Info
Result
Fatal

Je mozne vytvorit lubovolny pocet target suborov pre lubovolnu vrstvu. Logger zapisuje spravy do suborov podla:

subor: Trace Debug Info Result Fatal
sprava:
Trace    +
Debug    +     +
Info     +     +     +
Result   +     +     +    +
Fatal    +     +     +    +      +

na zaciatok je potrebne inicializovat niektore subory (je mozne inicializovat lubovolny pocet lubovolnych suborov):
Logger.createTraceFile(string path, string name);
Logger.createDebugFile(string path, string name);
Logger.createInfoFile(string path, string name);
Logger.createResultFile(string path, string name);
Logger.createFatalFile(string path, string name);

v pripade ak chceme na koniec logovacich sprav automaticky pridavat \n:
Logger.addCrLf(true);

v pripade ak chceme, aby logger na zaciatok logu pridaval casovu znacku:
Logger.showTime(true);

samotny zapis logu:
Logger.Trace("tracova sprava");
...
Logger.Fatal("chybova sprava");

log vyzera nasledovne:

5/5/2016 14:42:03.413664 FATAL: licencia ma neplatny kluc
5/5/2016 14:42:15.400901 T: start1
5/5/2016 14:42:15.481016 T: start2
5/5/2016 14:42:16.001765 I: Subor c:...csv nenajdeny

