﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Reflection;
/*
 * 
 * aby bolo mozne logovat vo viacerych vlaknach, nesmie sa pocas behu programu prenastavovat minSave, teda volat metoda setMin
 * 
*/

namespace Logging
{
    /// <summary>
    /// 
    /// </summary>
    [ClassInterface(ClassInterfaceType.AutoDual)]
    public static class Logger
    {
        private static readonly Dictionary<string, StreamWriter> trace_list = new Dictionary<string, StreamWriter>();
        private static readonly Dictionary<string, StreamWriter> debug_list = new Dictionary<string, StreamWriter>();
        private static readonly Dictionary<string, StreamWriter> info_list = new Dictionary<string, StreamWriter>();
        private static readonly Dictionary<string, StreamWriter> result_list = new Dictionary<string, StreamWriter>();
        private static readonly Dictionary<string, StreamWriter> fatal_list = new Dictionary<string, StreamWriter>();
        private static eLog minSave = eLog.TRACElog;
        private static bool showTD = false;
        private static bool addCrLf = false;
        private static readonly ManualResetEvent logging = new ManualResetEvent(true);
        public enum eLog
        {
            TRACElog = 0,
            DEBUGlog = 1,
            INFOlog = 2,
            RESULTlog = 3,
            FATALlog = 4,
        }

        public static void CloseAll()
        {
            Logger.CloseStreamList(Logger.trace_list);
            Logger.CloseStreamList(Logger.debug_list);
            Logger.CloseStreamList(Logger.info_list);
            Logger.CloseStreamList(Logger.result_list);
            Logger.CloseStreamList(Logger.fatal_list);

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        public static void Close(string name)
        {
            Logger.CloseStreamList(Logger.trace_list, name);
            Logger.CloseStreamList(Logger.debug_list, name);
            Logger.CloseStreamList(Logger.info_list, name);
            Logger.CloseStreamList(Logger.result_list, name);
            Logger.CloseStreamList(Logger.fatal_list, name);

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        /// <summary>
        /// vytvori / otvori subor pre trace, debug, info, result a fatal vypisy
        /// </summary>
        /// <param name="path">cesta k suboru</param>
        /// <param name="fileName">nazov suboru</param>
        public static void CreateTraceFile(string path, string fileName)
        {
            var f = new StreamWriter(path + fileName, false);
            Logger.AddTrace(string.Empty, f);
            Logger.AddDebug(string.Empty, f);
            Logger.AddInfo(string.Empty, f);
            Logger.AddResult(string.Empty, f);
            Logger.AddFatal(string.Empty, f);
        }
        /// <summary>
        /// vytvori / otvori subor pre trace, debug, info, result a fatal vypisy
        /// </summary>
        /// <param name="path">cesta k suboru</param>
        /// <param name="fileName">nazov suboru</param>
        /// <param name="name">nazov loggeru</param>
        public static void CreateTraceFile(string path, string fileName, string name)
        {
            var f = new StreamWriter(path + fileName, false);
            Logger.AddTrace(name, f);
            Logger.AddDebug(name, f);
            Logger.AddInfo(name, f);
            Logger.AddResult(name, f);
            Logger.AddFatal(name, f);
        }
        /// <summary>
        /// vytvori / otvori subor pre debug, info, result a fatal vypisy
        /// </summary>
        /// <param name="path">cesta k suboru</param>
        /// <param name="fileName">nazov suboru</param>
        public static void CreateDebugFile(string path, string fileName)
        {
            var f = new StreamWriter(path + fileName, false);
            Logger.AddDebug(string.Empty, f);
            Logger.AddInfo(string.Empty, f);
            Logger.AddResult(string.Empty, f);
            Logger.AddFatal(string.Empty, f);
        }
        /// <summary>
        /// vytvori / otvori subor pre debug, info, result a fatal vypisy
        /// </summary>
        /// <param name="path">cesta k suboru</param>
        /// <param name="fileName">nazov suboru</param>
        /// <param name="name">nazov loggeru</param>
        public static void CreateDebugFile(string path, string fileName, string name)
        {
            var f = new StreamWriter(path + fileName, false);
            Logger.AddDebug(name, f);
            Logger.AddInfo(name, f);
            Logger.AddResult(name, f);
            Logger.AddFatal(name, f);
        }
        /// <summary>
        /// vytvori / otvori subor pre info, result a fatal vypisy
        /// </summary>
        /// <param name="path">cesta k suboru</param>
        /// <param name="fileName">nazov suboru</param>
        public static void CreateInfoFile(string path, string fileName)
        {
            var f = new StreamWriter(path + fileName, false);
            Logger.AddInfo(string.Empty, f);
            Logger.AddResult(string.Empty, f);
            Logger.AddFatal(string.Empty, f);
        }
        /// <summary>
        /// vytvori / otvori subor pre info, result a fatal vypisy
        /// </summary>
        /// <param name="path">cesta k suboru</param>
        /// <param name="fileName">nazov suboru</param>
        /// <param name="name">nazov loggeru</param>
        public static void CreateInfoFile(string path, string fileName, string name)
        {
            var f = new StreamWriter(path + fileName, false);
            Logger.AddInfo(name, f);
            Logger.AddResult(name, f);
            Logger.AddFatal(name, f);
        }
        /// <summary>
        /// vytvori / otvori subor pre result a fatal vypisy
        /// </summary>
        /// <param name="path">cesta k suboru</param>
        /// <param name="fileName">nazov suboru</param>
        public static void CreateResultFile(string path, string fileName)
        {
            var f = new StreamWriter(path + fileName, false);
            Logger.AddResult(string.Empty, f);
            Logger.AddFatal(string.Empty, f);
        }
        /// <summary>
        /// vytvori / otvori subor pre result a fatal vypisy
        /// </summary>
        /// <param name="path">cesta k suboru</param>
        /// <param name="fileName">nazov suboru</param>
        /// <param name="name">nazov loggeru</param>
        public static void CreateResultFile(string path, string fileName, string name)
        {
            var f = new StreamWriter(path + fileName, false);
            Logger.AddResult(name, f);
            Logger.AddFatal(name, f);
        }
        /// <summary>
        /// vytvori / otvori subor pre fatal vypisy
        /// </summary>
        /// <param name="path">cesta k suboru</param>
        /// <param name="fileName">nazov suboru</param>
        public static void CreateFatalFile(string path, string fileName)
        {
            var f = new StreamWriter(path + fileName, false);
            Logger.AddFatal(string.Empty, f);
        }
        /// <summary>
        /// vytvori / otvori subor pre fatal vypisy
        /// </summary>
        /// <param name="path">cesta k suboru</param>
        /// <param name="fileName">nazov suboru</param>
        /// <param name="name">nazov loggeru</param>
        public static void CreateFatalFile(string path, string fileName, string name)
        {
            var f = new StreamWriter(path + fileName, false);
            Logger.AddFatal(name, f);
        }
        /// <summary>
        /// vypise trace log do trace-ovych suborov
        /// </summary>
        /// <param name="what"></param>
        public static void Trace(string what)
        {
            Logger.Trace(null, what);
        }
        /// <summary>
        /// vypise trace log do trace-ovych suborov nejakeho nazvu
        /// </summary>
        /// <param name="what"></param>
        /// <param name="name">nazov loggeru</param>
        public static void Trace(string name, string what)
        {
            if (Logger.minSave > eLog.TRACElog) return;
            Logger.logging.WaitOne();
            Logger.logging.Reset();
            var result = "";
            Logger.MakeLogString(ref what, eLog.TRACElog, ref result);
            foreach (var kvp in Logger.trace_list)
            {
                if (name is object && (kvp.Key != name)) continue;
                kvp.Value.Write(result);
                kvp.Value.Flush();
            }
            Logger.logging.Set();
        }
        /// <summary>
        /// vypise debug log do trace-ovych a debug-ovych suborov
        /// </summary>
        /// <param name="what"></param>
        public static void Debug(string what)
        {
            Logger.Debug(null, what);
        }
        /// <summary>
        /// vypise debug log do trace-ovych a debug-ovych suborov nejakeho nazvu
        /// </summary>
        /// <param name="what"></param>
        /// <param name="name">nazov loggeru</param>
        public static void Debug(string name, string what)
        {
            if (Logger.minSave > eLog.DEBUGlog) return;
            Logger.logging.WaitOne();
            Logger.logging.Reset();
            var result = "";
            Logger.MakeLogString(ref what, eLog.DEBUGlog, ref result);
            foreach (var kvp in Logger.debug_list)
            {
                if (name is object && (kvp.Key != name)) continue;
                kvp.Value.Write(result);
                kvp.Value.Flush();
            }
            Logger.logging.Set();
        }
        /// <summary>
        /// vypise info log do trace-ovych, debug-ovych a info suborov
        /// </summary>
        /// <param name="what"></param>
        public static void Info(string what)
        {
            Logger.Info(null, what);
        }
        /// <summary>
        /// vypise info log do trace-ovych, debug-ovych a info suborov nejakeho nazvu
        /// </summary>
        /// <param name="what"></param>
        /// <param name="name">nazov loggeru</param>
        public static void Info(string name, string what)
        {
            if (Logger.minSave > eLog.INFOlog) return;
            Logger.logging.WaitOne();
            Logger.logging.Reset();
            var result = "";
            Logger.MakeLogString(ref what, eLog.INFOlog, ref result);
            foreach (var kvp in Logger.info_list)
            {
                if (name is object && (kvp.Key != name)) continue;
                kvp.Value.Write(result);
                kvp.Value.Flush();
            }
            Logger.logging.Set();
        }
        /// <summary>
        /// vypise result log do trace-ovych, debug-ovych, info a result-ovych suborov
        /// </summary>
        /// <param name="what"></param>
        public static void Result(string what)
        {
            Logger.Result(null, what);
        }
        /// <summary>
        /// vypise result log do trace-ovych, debug-ovych, info a result-ovych suborov nejakeho nazvu
        /// </summary>
        /// <param name="what"></param>
        /// <param name="name">nazov loggeru</param>
        public static void Result(string name, string what)
        {
            if (Logger.minSave > eLog.RESULTlog) return;
            Logger.logging.WaitOne();
            Logger.logging.Reset();
            var result = "";
            Logger.MakeLogString(ref what, eLog.RESULTlog, ref result);
            foreach (var kvp in Logger.result_list)
            {
                if (name is object && (kvp.Key != name)) continue;
                kvp.Value.Write(result);
                kvp.Value.Flush();
            }
            Logger.logging.Set();
        }
        /// <summary>
        /// vypise fatal log do trace-ovych, debug-ovych, info, result-ovych a fatal suborov
        /// </summary>
        /// <param name="what"></param>
        public static void Fatal(string what)
        {
            Logger.Fatal(null, what);
        }
        /// <summary>
        /// vypise fatal log do trace-ovych, debug-ovych, info, result-ovych a fatal suborov nejakeho nazvu
        /// </summary>
        /// <param name="what"></param>
        /// <param name="name">nazov loggeru</param>
        public static void Fatal(string name, string what)
        {
            if (Logger.minSave > eLog.FATALlog) return;
            Logger.logging.WaitOne();
            Logger.logging.Reset();
            var result = "";
            Logger.MakeLogString(ref what, eLog.FATALlog, ref result);
            foreach (var kvp in Logger.fatal_list)
            {
                if (name is object && (kvp.Key != name)) continue;
                kvp.Value.Write(result);
                kvp.Value.Flush();
            }
            Logger.logging.Set();
        }

        /// <summary>
        /// povoli pridavat na konci kazdeho logu novy riadok
        /// </summary>
        /// <param name="add"></param>
        public static void AddCrLf(bool add)
        {
            Logger.addCrLf = add;
        }
        /// <summary>
        /// povoli zobrazovanie casu na zaciatku kazdeho logu
        /// </summary>
        /// <param name="show"></param>
        public static void ShowTime(bool show)
        {
            Logger.showTD = show;
        }
        /// <summary>
        /// nastavi minimalnu vaznost logu ktory sa uklada
        /// </summary>
        /// <param name="newMin"></param>
        public static void SetMin(eLog newMin)
        {
            if (newMin < eLog.TRACElog) return;
            if (newMin > eLog.FATALlog) return;
		    Logger.minSave = newMin;
        }

        /// <summary>
        /// Vlozi do tracov nazov metody, z ktorej bola zavolana
        /// </summary>
        /// <param name=""></param>
        public static void TraceIn()
        {
            StackTrace stackTrace = new StackTrace();
            StackFrame stackFrame = stackTrace.GetFrame(1);
            MethodBase methodBase = stackFrame.GetMethod();
            Logger.Trace(methodBase.ReflectedType.FullName + ": " + methodBase.ToString() + "Enter");
        }

        /// <summary>
        /// Vlozi do tracov nazov metody, z ktorej bola zavolana
        /// </summary>
        /// <param name=""></param>
        public static void TraceOut()
        {
            StackTrace stackTrace = new StackTrace();
            StackFrame stackFrame = stackTrace.GetFrame(1);
            MethodBase methodBase = stackFrame.GetMethod();
            Logger.Trace(methodBase.ReflectedType.FullName + ": " + methodBase.ToString() + "Leave");
        }

        #region private
        private static void CloseStreamList(Dictionary<string, StreamWriter> list)
        {
            try
            {
                foreach (var f in list)
                {
                    Logger.CloseStreamWriter(f.Value);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                list.Clear();
            }
        }
        private static void CloseStreamList(Dictionary<string, StreamWriter> list, string name)
        {
            try
            {
                foreach (var f in list)
                {
                    if (f.Key != name) continue;
                    Logger.CloseStreamWriter(f.Value);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                list.Remove(name);
            }
        }
        private static void CloseStreamWriter(StreamWriter writer)
        {
            if (writer.BaseStream is null) return;
            writer.Flush();
            writer.Close();
            writer.Dispose();
        }
        private static void AddTrace(string name, StreamWriter f)
        {
            if (f is null) throw new NullReferenceException();
            Logger.trace_list.Add(name, f);
        }
        private static void AddDebug(string name, StreamWriter f)
        {
            if (f is null) throw new NullReferenceException();
            Logger.debug_list.Add(name, f);
        }
        private static void AddInfo(string name, StreamWriter f)
        {
            if (f is null) throw new NullReferenceException();
            Logger.info_list.Add(name, f);
        }
        private static void AddResult(string name, StreamWriter f)
        {
            if (f is null) throw new NullReferenceException();
            Logger.result_list.Add(name, f);
        }
        private static void AddFatal(string name, StreamWriter f)
        {
            if (f is null) throw new NullReferenceException();
            Logger.fatal_list.Add(name, f);
        }

        private static void MakeLogString(ref string what, eLog type, ref string result)
        {
            //if (showtd) result = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " ";
            if (Logger.showTD) result = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToString("HH:mm:ss.ffffff") + " ";
            switch (type)
            {
                case eLog.TRACElog:
                    result += "T: ";
                    break;
                case eLog.DEBUGlog:
                    result += "D: ";
                    break;
                case eLog.INFOlog:
                    result += "I: ";
                    break;
                case eLog.RESULTlog:
                    result += "Result: ";
                    break;
                case eLog.FATALlog:
                    result += "FATAL: ";
                    break;
            }
            result += what;
            if (Logger.addCrLf) result += Environment.NewLine;
        }

        #endregion
    }
}

