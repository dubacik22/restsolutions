﻿using System.Xml;
using System.Collections.Generic;

namespace Fielder
{
    public class KeyValuePairSerializer : FieldSerializer
    {
        private static bool HaveKeyValuePairAttribute(XmlNode what)
        {
            if (what.Attributes is null) return false;
            if (what.Attributes.Count > 2) return false;
            XmlAttribute myTypeAttr = what.Attributes[MY_TYPE_STR];
            if (myTypeAttr is null) return false;
            if (myTypeAttr.Value == LIST_PREFIX) return true;
            return false;
        }
        private static bool HaveKeyValuePairName(XmlNode what)
        {
            if (what.Name.StartsWith(ARRAY_PREFIX)) return true;
            return false;
        }
        public static bool IsKeyValuePairObject(XmlNode what)
        {
            if (HaveKeyValuePairAttribute(what)) return true;
            if (HaveKeyValuePairName(what)) return true;
            return false;
        }
        public override ParsedObject Parse(XmlNode value)
        {
            var instance = FieldSerializer.CreateObject();
            instance.ThisType = DataType.OBJECT;
            instance.Obj = new Dictionary<string, ParsedObject>();
            instance.ValueOrType = KEY_VALUE_PAIR_PREFIX;

            Fielder.ParseWhatteverAtribute(value, instance);

            var children = value.ChildNodes;
            for (int i = 0; i < children.Count; i++)
            {
                var child = children[i];
                if (child.NodeType.Equals(XmlNodeType.Element))
                {
                    instance.Obj.Add(child.Name, Fielder.ParseWhatteverNode(child));
                }
            }
            return instance;
        }

        private XmlElement CreateElementWithoutType(object what, string name, XmlDocument creator)
        {
            name = this.NormalizeName(name);
            var keyValuePairElement = creator.CreateElement("", name, "");

            var type = what.GetType();
            var key = type.GetProperty("Key").GetValue(what, null);
            var value = type.GetProperty("Value").GetValue(what, null);

            var keySerializer = this.Fielder.FindSerializer(key.GetType());
            var valueSerializer = this.Fielder.FindSerializer(value.GetType());
            if (keySerializer != null)
            {
                if (key is null)
                {
                    keySerializer.Serialize(null, "Value", keyValuePairElement, creator, false, false);
                }
                else
                {
                    keySerializer.Serialize(key, "Key", keyValuePairElement, creator, false, false);
                }
            }
            if (valueSerializer != null)
            {
                if (value is null)
                {
                    valueSerializer.Serialize(null, "Value", keyValuePairElement, creator, false, false);
                }
                else
                {
                    valueSerializer.Serialize(value, "Value", keyValuePairElement, creator, false, false);
                }
            }

            return keyValuePairElement;
        }
        public override void Serialize(object what, string name, XmlNode into, XmlDocument creator, bool toElement, bool withoutTypeString)
        {
            var innerNode = this.CreateElementWithoutType(what, name, creator);
            innerNode.SetAttribute(MY_TYPE_STR, KEY_VALUE_PAIR_PREFIX);
            into.AppendChild(innerNode);
        }
    }
}
