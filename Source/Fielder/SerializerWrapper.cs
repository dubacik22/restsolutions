﻿using System;

namespace Fielder
{
    internal class SerializerWrapper
    {
        internal bool ForDerivedClasses { get; set; }
        internal Type ForType { get; set; }
        internal FieldSerializer Serializer { get; set; }
    }
}
