﻿using Logging;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Fielder
{
    public class LS_Dictionary<Key, Value> : Dictionary<Key, Value>, IXmlSerializable
    {
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        static readonly XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
        static readonly XmlSerializer serializer = new XmlSerializer(typeof(LS_Dictionary<Key, Value>));
        static readonly XmlSerializer keySerializer = new XmlSerializer(typeof(Key), new XmlRootAttribute(KEY_NAME));
        static readonly XmlSerializer valueSerializer = new XmlSerializer(typeof(Value), new XmlRootAttribute(VALUE_NAME));
        private const string ITEM_NAME = "Item";
        private const string KEY_NAME = "Key";
        private const string VALUE_NAME = "Value";

        private string filename = "LS_Dictionary.xml";
        public bool Loaded { get; set; } = true;

        #region IXmlSerializable
        public XmlSchema GetSchema()
        {
            return null;
        }

        private void ShiftWhitespace(XmlReader reader)
        {
            while (reader.NodeType == XmlNodeType.Whitespace) reader.Read();
        }
        private bool IsItem(XmlReader reader)
        {
            if (reader.NodeType != XmlNodeType.Element) return false;
            if (reader.LocalName != ITEM_NAME) return false;
            return true;
        }
        private T GetInner<T>(string name, XmlReader reader, XmlSerializer serializer)
        {
            this.ShiftWhitespace(reader);
            if (reader.Name != name)
            {
                reader.Skip();
                return default;
            }
            var inner = reader.ReadSubtree();
            var ret = (T)serializer.Deserialize(inner);
            inner.Close();
            reader.Read();
            return ret;
        }
        public virtual void ReadXml(XmlReader reader)
        {
            bool wasEmpty = reader.IsEmptyElement;
            if (wasEmpty) return;
            try
            {
                var keyAttribute = this.SerializeAsAttribute(typeof(Key));
                var valueAttribute = this.SerializeAsAttribute(typeof(Value));
                while (!this.IsItem(reader))
                {
                    reader.Read();
                    if (reader.NodeType == XmlNodeType.None) return;
                }
                while (this.IsItem(reader))
                {
                    Key key = default;
                    Value value = default;
                    
                    if (keyAttribute) key = (Key)this.ParseAttribute(typeof(Key), reader.GetAttribute(KEY_NAME));
                    if (valueAttribute) value = (Value)this.ParseAttribute(typeof(Value), reader.GetAttribute(VALUE_NAME));
                    if (!keyAttribute || !valueAttribute)
                    {
                        reader.Read();
                        if (!keyAttribute) key = this.GetInner<Key>(KEY_NAME, reader, keySerializer);
                        if (!valueAttribute) value = this.GetInner<Value>(VALUE_NAME, reader, valueSerializer);
                        this.ShiftWhitespace(reader);
                    }
                    reader.Read();
                    this.ShiftWhitespace(reader);
                    if (key is object) this.Add(key, value);
                }
                reader.Read();
            }
            catch
            {
            }
        }
        public void WriteXml(XmlWriter writer)
        {
            var keyAttribute = this.SerializeAsAttribute(typeof(Key));
            var valueAttribute = this.SerializeAsAttribute(typeof(Value));
            foreach (var key in this.Keys)
            {
                writer.WriteStartElement(ITEM_NAME);
                Value value = this[key];
                if (keyAttribute) writer.WriteAttributeString(KEY_NAME, Convert.ToString(key, CultureInfo.InvariantCulture));
                if (valueAttribute) writer.WriteAttributeString(VALUE_NAME, Convert.ToString(value, CultureInfo.InvariantCulture));
                if (!keyAttribute) keySerializer.Serialize(writer, key, ns);
                if (!valueAttribute) valueSerializer.Serialize(writer, value, ns);
                writer.WriteEndElement();
            }
        }
        #endregion

        private bool SerializeAsAttribute(Type type)
        {
            if (type.Equals(typeof(string))) return true;
            if (type.Equals(typeof(int))) return true;
            if (type.Equals(typeof(long))) return true;
            if (type.Equals(typeof(double))) return true;
            if (type.Equals(typeof(decimal))) return true;
            if (type.Equals(typeof(short))) return true;
            if (type.Equals(typeof(char))) return true;
            if (type.Equals(typeof(Enum))) return true;
            return false;
        }
        private object ParseAttribute(Type type, string text)
        {
            var converter = TypeDescriptor.GetConverter(type);
            return converter.ConvertFrom(text);
        }
        public Value Get(Key key)
        {
            if (base.TryGetValue(key, out Value ret)) return ret;
            else return default;
        }

        public static LS_Dictionary<Key, Value> Load(string filename)
        {
            LS_Dictionary<Key, Value> ret = null;
            FileStream file = null;
            try
            {
                if (File.Exists(filename))
                {
                    file = new FileStream(filename, FileMode.Open);
                    XmlReaderSettings readerSettings = new XmlReaderSettings
                    {
                        IgnoreComments = true
                    };
                    using (var reader = XmlReader.Create(file, readerSettings))
                    {
                        if (serializer.CanDeserialize(reader)) ret = (LS_Dictionary<Key, Value>)serializer.Deserialize(reader);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Fatal(e.Message);
            }

            if (ret is null)
            {
                ret = new LS_Dictionary<Key, Value>
                {
                    Loaded = false
                };
            }
            ret.filename = filename;
            if (file is object) file.Close();
            return ret;
        }

        public void Save()
        {
            if (ns.Count == 0) ns.Add("", "");
            StreamWriter file = null;
            try
            {
                file = new StreamWriter(this.filename);
                serializer.Serialize(file, this, ns);
            }
            catch (Exception e)
            {
                Logger.Info("ERR: EXC: LS_Dictionary<Key,Value>.Save: " + e.Message);
            }
            finally
            {
                if (file != null) file.Close();
            }
        }
        public void Save(string fileName)
        {
            this.filename = fileName;
            this.Save();
        }
    }
}
