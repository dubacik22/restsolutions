﻿using Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Fielder
{
    public class LS_List<T> : List<T>
    {
        static readonly XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
        static readonly XmlSerializer serializer = new XmlSerializer(typeof(LS_List<T>));
        string filename = "LS_List.xml";
        public bool Loaded { get; set; } = true;

        public static LS_List<T> Load(string filename)
        {
            LS_List<T> ret = null;
            FileStream file = null;
            try
            {
                if (File.Exists(filename))
                {
                    file = new FileStream(filename, FileMode.Open);
                    XmlReaderSettings readerSettings = new XmlReaderSettings
                    {
                        IgnoreComments = true
                    };
                    using (var reader = XmlReader.Create(file, readerSettings))
                    {
                        if (serializer.CanDeserialize(reader)) ret = (LS_List<T>)serializer.Deserialize(reader);
                    }
                }
            }
            catch
            {
            }
            if (ret is null)
            {
                ret = new LS_List<T>
                {
                    Loaded = false
                };
            }
            ret.filename = filename;
            if (file is object) file.Close();
            return ret;
        }

        public void Save()
        {
            //Serialization.XmlSerializer serializer = LS_TList<T>.serializer;
            if (ns.Count == 0) ns.Add("", "");
            StreamWriter file = null;
            try
            {
                file = new StreamWriter(filename);
                serializer.Serialize(file, this, ns);
            }
            catch (Exception e)
            {
                Logger.Info("ERR: EXC: LS_TList<T>.Save: " + e.Message);
            }
            finally
            {
                if (file != null) file.Close();
            }
        }
        public void Save(string fileName)
        {
            this.filename = fileName;
            this.Save();
        }
    }
}
