﻿using System.Collections.Generic;
using DisplayValue;

namespace Fielder
{
    public class ParsedObject
    {
        public DataType ThisType { get; set; }
        public string ValueOrType { get; set; }
        public Dictionary<string, ParsedObject> Obj { get; set; }
        public List<ParsedObject> Enu { get; set; }

        internal ParsedObject() { }
        internal ParsedObject(string value)
        {
            this.ThisType = DataType.STRING;
            ValueOrType = value;
        }

        public bool IsObject()
        {
            return this.ThisType == DataType.OBJECT;
        }
        public bool IsArray()
        {
            return this.ThisType == DataType.ARRAY;
        }
        public bool IsString()
        {
            return this.ThisType == DataType.STRING;
        }

        public string GetStrProperty(string name, string def)
        {
            if (!this.IsObject()) return def;
            return this.GetStrPropertyWithoutCheck(name, def);
        }
        public string GetStrProperty(string name)
        {
            return this.GetStrProperty(name, null);
        }
        public string GetStrPropertyWithoutCheck(string name, string def)
        {
            if (name is null || !this.Obj.ContainsKey(name)) return def;
            var prop = this.Obj[name];
            if (prop.ThisType != DataType.STRING) return def;
            string ret = prop.ValueOrType;
            if (ret is null) return def;
            return ret;
        }
        public string GetStrPropertyWithoutCheck(string name)
        {
            return this.GetStrPropertyWithoutCheck(name, null);
        }

        public bool GetBoolProperty(string name, bool def)
        {
            if (!this.IsObject()) return def;
            return this.GetBoolPropertyWithoutCheck(name, def);
        }
        public bool GetBoolProperty(string name)
        {
            return this.GetBoolProperty(name, false);
        }
        public bool GetBoolPropertyWithoutCheck(string name, bool def)
        {
            if (name is null || !this.Obj.ContainsKey(name)) return def;
            var prop = this.Obj[name];
            if (prop.ThisType != DataType.STRING) return def;
            string strRet = prop.ValueOrType;
            if (strRet is null) return def;
            if (!bool.TryParse(strRet, out bool ret)) return def;
            return ret;
        }
        public bool GetBoolPropertyWithoutCheck(string name)
        {
            return this.GetBoolPropertyWithoutCheck(name, false);
        }

        public int GetIntProperty(string name, int def)
        {
            if (!this.IsObject()) return def;
            return this.GetIntPropertyWithoutCheck(name, def);
        }
        public int GetIntProperty(string name)
        {
            return this.GetIntProperty(name, -1);
        }
        public int GetIntPropertyWithoutCheck(string name, int def)
        {
            if (name is null || !this.Obj.ContainsKey(name)) return def;
            var prop = this.Obj[name];
            if (prop.ThisType != DataType.STRING) return def;
            string strRet = prop.ValueOrType;
            if (strRet is null) return def;
            if (!int.TryParse(strRet, out int ret)) return def;
            return ret;
        }
        public int? GetInt_PropertyWithoutCheck(string name, int? def)
        {
            if (name is null || !this.Obj.ContainsKey(name)) return def;
            var prop = this.Obj[name];
            if (prop.ThisType != DataType.STRING) return def;
            string strRet = prop.ValueOrType;
            if (strRet is null) return def;
            if (!int.TryParse(strRet, out int ret)) return def;
            return ret;
        }
        public int GetIntPropertyWithoutCheck(string name)
        {
            return this.GetIntPropertyWithoutCheck(name, -1);
        }

        public double GetDoubleProperty(string name, double def)
        {
            if (!this.IsObject()) return def;
            return this.GetDoublePropertyWithoutCheck(name, def);
        }
        public double GetDoubleProperty(string name)
        {
            return this.GetDoubleProperty(name, double.NaN);
        }
        public double GetDoublePropertyWithoutCheck(string name, double def)
        {
            if (name is null || !this.Obj.ContainsKey(name)) return def;
            var prop = this.Obj[name];
            if (prop.ThisType != DataType.STRING) return def;
            string strRet = prop.ValueOrType;
            if (strRet is null) return def;
            if (!double.TryParse(strRet, out double ret)) return def;
            return ret;
        }
        public double GetDoublePropertyWithoutCheck(string name)
        {
            return this.GetDoublePropertyWithoutCheck(name, double.NaN);
        }

        public DispValue GetDispValueProperty(string name, DispValue def)
        {
            if (!this.IsObject()) return def;
            return this.GetDispValuePropertyWithoutCheck(name, def);
        }
        public DispValue GetDispValueProperty(string name)
        {
            return this.GetDispValueProperty(name, null);
        }
        public DispValue GetDispValuePropertyWithoutCheck(string name, DispValue def)
        {
            if (name is null || !this.Obj.ContainsKey(name)) return def;
            var prop = this.Obj[name];
            return prop.GetDispValue(def);
        }
        public DispValue GetDispValuePropertyWithoutCheck(string name)
        {
            return this.GetDispValuePropertyWithoutCheck(name, null);
        }
        public DispValue GetDispValue(DispValue def)
        {
            if (!this.IsObject()) return def;
            return this.GetDispValueWithoutCheck(def);
        }
        public DispValue GetDispValue()
        {
            return this.GetDispValue(null);
        }
        public DispValue GetDispValueWithoutCheck(DispValue def)
        {
            double value = this.GetDoublePropertyWithoutCheck(nameof(DispValue.Value), (def is null) ? double.NaN : def.Value);
            int? decPlaces = this.GetInt_PropertyWithoutCheck(nameof(DispValue.DecPlaces), (def is null) ? 0 : def.DecPlaces);
            int dispExp = this.GetIntPropertyWithoutCheck(nameof(DispValue.DispExp), (def is null) ? 0 : def.DispExp);
            return new DispValue(value, decPlaces, dispExp);
        }
        public DispValue GetDispValueWithoutCheck()
        {
            return this.GetDispValueWithoutCheck(null);
        }

        public ParsedObject GetObjectProperty(string name)
        {
            if (!this.IsObject()) return null;
            return this.GetObjectPropertyWithoutCheck(name);
        }
        public ParsedObject GetObjectPropertyWithoutCheck(string name)
        {
            if (name is null || !this.Obj.ContainsKey(name)) return null;
            var prop = this.Obj[name];
            if (prop.ThisType != DataType.OBJECT) return null;
            return prop;
        }
        public List<ParsedObject> GetArrayProperty(string name)
        {
            if (!this.IsObject()) return null;
            return this.GetArrayPropertyWithoutCheck(name);
        }
        public List<ParsedObject> GetArrayPropertyWithoutCheck(string name)
        {
            if (name is null || !this.Obj.ContainsKey(name)) return null;
            var prop = this.Obj[name];
            if (prop.ThisType != DataType.ARRAY) return null;
            return prop.Enu;
        }
        public ParsedObject GetProperty(string name)
        {
            if (!this.IsObject()) return null;
            return this.GetPropertyWithoutCheck(name);
        }
        public ParsedObject GetPropertyWithoutCheck(string name)
        {
            if (name is null || !this.Obj.ContainsKey(name)) return null;
            var prop = this.Obj[name];
            return prop;
        }
    }
}
