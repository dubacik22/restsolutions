﻿using System.Reflection;
using System.Xml;
using System.Collections.Generic;
using System;

namespace Fielder
{
    internal class ObjectSerializer : FieldSerializer
    {
        public override ParsedObject Parse(XmlNode value)
        {
            var instance = FieldSerializer.CreateObject();
            instance.ThisType = DataType.OBJECT;
            instance.Obj = new Dictionary<string, ParsedObject>();

            Fielder.ParseWhatteverAtribute(value, instance);

            var children = value.ChildNodes;
            for (int i = 0; i < children.Count; i++)
            {
                var child = children[i];
                if (child.NodeType == XmlNodeType.Element)
                {
                    instance.Obj.Add(child.Name, Fielder.ParseWhatteverNode(child));
                }
            }
            return instance;
        }
        private bool IsDictObject(XmlNode child)
        {
            if (child.Attributes is null) return false;
            if (child.Attributes.Count != 1) return false;
            XmlAttribute typeAttr = child.Attributes[TYPE_STR];
            if (typeAttr is null) return false;
            if (DICTIONARY_TYPE.Equals(typeAttr.Value)) return true;
            return false;
        }

        private XmlElement CreateElementWithoutType(object what, string name, XmlDocument creator)
        {
            name = this.NormalizeName(name);
            var underlyingClass = what.GetType();

            var thisElement = creator.CreateElement("", name, "");

            var properties = underlyingClass.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var innerField in properties)
            {
                object innerElement = innerField.GetValue(what);
                if (innerElement == null) continue;
                string dataName = null;
                var attributeAttribute = Attribute.GetCustomAttribute(innerField, typeof(System.Xml.Serialization.XmlAttributeAttribute)) as System.Xml.Serialization.XmlAttributeAttribute;
                if (attributeAttribute is object) dataName = attributeAttribute.AttributeName;
                var elementAttribute = Attribute.GetCustomAttribute(innerField, typeof(System.Xml.Serialization.XmlElementAttribute)) as System.Xml.Serialization.XmlElementAttribute;
                if (elementAttribute is object) dataName = elementAttribute.ElementName;
                if (string.IsNullOrEmpty(dataName)) dataName = innerField.Name;
                var fieldSerializer = this.Fielder.FindSerializer(innerField.PropertyType);
                if (fieldSerializer is object) fieldSerializer.Serialize(innerElement, dataName, thisElement, creator, false, false);
            }
            var fields = underlyingClass.GetFields(BindingFlags.Public | BindingFlags.Instance);
            foreach (var innerField in fields)
            {
                object innerElement = innerField.GetValue(what);
                if (innerElement == null) continue;
                string dataName = null;
                var attributeAttribute = Attribute.GetCustomAttribute(innerField, typeof(System.Xml.Serialization.XmlAttributeAttribute)) as System.Xml.Serialization.XmlAttributeAttribute;
                if (attributeAttribute is object) dataName = attributeAttribute.AttributeName;
                var elementAttribute = Attribute.GetCustomAttribute(innerField, typeof(System.Xml.Serialization.XmlElementAttribute)) as System.Xml.Serialization.XmlElementAttribute;
                if (elementAttribute is object) dataName = elementAttribute.ElementName;
                if (string.IsNullOrEmpty(dataName)) dataName = innerField.Name;
                var fieldSerializer = this.Fielder.FindSerializer(innerField.FieldType);
                if (fieldSerializer is object) fieldSerializer.Serialize(innerElement, dataName, thisElement, creator, false, false);
            }

            return thisElement;
        }
        public override void Serialize(object what, string name, XmlNode into, XmlDocument creator, bool toElement, bool withoutTypeString)
        {
            var innerNode = this.CreateElementWithoutType(what, name, creator);
            if (!withoutTypeString)
            {
                var typeName = this.GetTypeName(what);
                innerNode.SetAttribute(TYPE_STR, typeName);
            }
            into.AppendChild(innerNode);
        }
    }
}