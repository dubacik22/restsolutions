﻿using Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Fielder
{
    public class LS_VariableTypeList<T> : List<T>, IXmlSerializable
    {
        static readonly XmlSerializer serializer = new XmlSerializer(typeof(LS_VariableTypeList<T>));
        static readonly XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
        private const string ITEM_NAME = "Item";
        private const string TYPE_NAME = "Type";
        string filename = "LS_VariableTypeList.xml";
        public bool Loaded { get; set; } = true;
        #region IXmlSerializable
        public XmlSchema GetSchema()
        {
            return null;
        }

        private void ShiftWhitespace(XmlReader reader)
        {
            while (reader.NodeType == XmlNodeType.Whitespace) reader.Read();
        }
        private bool IsItem(XmlReader reader)
        {
            if (reader.NodeType != XmlNodeType.Element) return false;
            if (reader.LocalName != ITEM_NAME) return false;
            return true;
        }
        private T GetInner(XmlReader reader, XmlSerializer serializer)
        {
            reader.Read();
            this.ShiftWhitespace(reader);
            var inner = reader.ReadSubtree();
            T ret = (T)serializer.Deserialize(inner);
            inner.Close();
            reader.Skip();
            this.ShiftWhitespace(reader);
            reader.Read();
            return ret;
        }
        public void ReadXml(XmlReader reader)
        {
            bool wasEmpty = reader.IsEmptyElement;
            if (wasEmpty) return;
            try
            {
                while (!this.IsItem(reader))
                {
                    reader.Read();
                    if (reader.NodeType == XmlNodeType.None) return;
                }
                while (this.IsItem(reader))
                {
                    var typeName = reader.GetAttribute(TYPE_NAME);
                    var type = Type.GetType(typeName);
                    if (type is null) throw new InvalidDataException("unknown type " + typeName);
                    var serializer = new XmlSerializer(type);
                    if (serializer is null) throw new InvalidOperationException("cannot get serializer for " + typeName);
                    T item = this.GetInner(reader, serializer);
                    this.Add(item);
                    this.ShiftWhitespace(reader);
                }
                reader.Read();
            }
            catch
            {
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (var item in this)
            {
                writer.WriteStartElement(ITEM_NAME);
                var itemType = item.GetType();
                var itemTypeName = itemType.AssemblyQualifiedName;
                writer.WriteAttributeString(TYPE_NAME, itemTypeName);
                var serializer = new XmlSerializer(itemType);
                var namespaces = new XmlSerializerNamespaces();
                if (namespaces.Count == 0) namespaces.Add("", "");
                serializer.Serialize(writer, item, namespaces);
                writer.WriteEndElement();
            }
        }
        #endregion


        public static LS_VariableTypeList<T> Load(string filename)
        {
            LS_VariableTypeList<T> ret = null;
            FileStream file = null;
            try
            {
                if (File.Exists(filename))
                {
                    file = new FileStream(filename, FileMode.Open);
                    XmlReaderSettings readerSettings = new XmlReaderSettings
                    {
                        IgnoreComments = true
                    };
                    using (var reader = XmlReader.Create(file, readerSettings))
                    {
                        if (serializer.CanDeserialize(reader)) ret = (LS_VariableTypeList<T>)serializer.Deserialize(reader);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Fatal(e.Message);
            }

            if (ret is null)
            {
                ret = new LS_VariableTypeList<T>
                {
                    Loaded = false
                };
            }
            ret.filename = filename;
            if (file is object) file.Close();
            return ret;
        }
        public void Save()
        {
            if (ns.Count == 0) ns.Add("", "");
            StreamWriter file = null;
            try
            {
                file = new StreamWriter(this.filename);
                serializer.Serialize(file, this, ns);
            }
            catch (Exception e)
            {
                Logger.Info("ERR: EXC: LS_VariableTypeList<T>.Save: " + e.Message);
            }
            finally
            {
                if (file != null) file.Close();
            }
        }
        public void Save(string fileName)
        {
            this.filename = fileName;
            this.Save();
        }
    }
}
