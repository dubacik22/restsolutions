﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Fielder
{
    public class Fielder
    {
        private const string ROOT_ELEMENT = "root";
        private readonly List<SerializerWrapper> serializers = new List<SerializerWrapper>();
        private readonly FieldSerializer nullSerializer;
        #region props
        private static StringSerializer stringSerializer = null;
        internal static StringSerializer StringSerializer
        {
            get
            {
                if (stringSerializer is null) stringSerializer = new StringSerializer();
                return stringSerializer;
            }
        }
        private static ListSerializer listSerializer = null;
        internal static ListSerializer ListSerializer
        {
            get
            {
                if (listSerializer is null) listSerializer = new ListSerializer();
                return listSerializer;
            }
        }
        private static KeyValuePairSerializer keyValuePairSerializer = null;
        internal static KeyValuePairSerializer KeyValuePairSerializer
        {
            get
            {
                if (keyValuePairSerializer is null) keyValuePairSerializer = new KeyValuePairSerializer();
                return keyValuePairSerializer;
            }
        }
        private static ObjectSerializer objectSerializer = null;
        internal static ObjectSerializer ObjectSerializer
        {
            get
            {
                if (objectSerializer is null) objectSerializer = new ObjectSerializer();
                return objectSerializer;
            }
        }

        #endregion
        public void AddSerializerForDerivedClassed(Type forType, FieldSerializer serializer)
        {
            this.InternalAddSerializer(forType, serializer, true);
        }
        public void AddSerializer(Type forType, FieldSerializer serializer)
        {
            this.InternalAddSerializer(forType, serializer, false);
        }
        private void InternalAddSerializer(Type forType, FieldSerializer serializer, bool forDerivedClasses)
        {
            if (serializer.Fielder is object) throw new InvalidOperationException("this serializer is already added to another fielder");
            serializer.Fielder = this;
            var wrapper = new SerializerWrapper()
            {
                Serializer = serializer,
                ForDerivedClasses = forDerivedClasses,
                ForType = forType
            };
            this.serializers.Add(wrapper);
        }
        internal FieldSerializer FindSerializer(Type type)
        {
            if (type is null) return nullSerializer;
            Type genericBaseType = null;
            if (type.IsGenericType) genericBaseType = type.GetGenericTypeDefinition();
            for (int i = this.serializers.Count - 1; i >= 0; i--)
            {
                var serializerWrapper = this.serializers[i];
                var forType = serializerWrapper.ForType;
                if (serializerWrapper.ForDerivedClasses)
                {
                    if (forType.IsAssignableFrom(type)) return serializerWrapper.Serializer;
                    if (genericBaseType is object) if (forType.IsAssignableFrom(genericBaseType)) return serializerWrapper.Serializer;
                }
                else
                {
                    if (forType.Equals(type)) return serializerWrapper.Serializer;
                    if (genericBaseType is object) if (forType.Equals(genericBaseType)) return serializerWrapper.Serializer;
                }
            }
            return null;
        }
        internal FieldSerializer FindParser(XmlNode child)
        {
            if (ListSerializer.IsArrayObject(child)) return ListSerializer;
            else if (KeyValuePairSerializer.IsKeyValuePairObject(child)) return KeyValuePairSerializer;
            else if ((child.ChildNodes.Count == 1) && (child.FirstChild.NodeType == XmlNodeType.Text)) return StringSerializer;
            else return ObjectSerializer;
        }
        internal static ParsedObject ParseWhatteverNode(XmlNode child)
        {
            ParsedObject childNode;
            if (ListSerializer.IsArrayObject(child))
            {
                childNode = ListSerializer.Parse(child);
            }
            else if (KeyValuePairSerializer.IsKeyValuePairObject(child))
            {
                childNode = KeyValuePairSerializer.Parse(child);
            }
            else if ((child.ChildNodes.Count == 1) && (child.FirstChild.NodeType == XmlNodeType.Text))
            {
                childNode = StringSerializer.Parse(child);
            }
            else
            {
                childNode = ObjectSerializer.Parse(child);
                childNode.ValueOrType = child.Name;
            }
            return childNode;
        }
        internal static void ParseWhatteverAtribute(XmlNode child, ParsedObject where)
        {
            var attributes = child.Attributes;
            if (attributes is object && attributes.Count > 0)
            {
                for (int i = 0; i < attributes.Count; i++)
                {
                    var attribute = attributes[i];
                    if (attribute.Name == FieldSerializer.MY_TYPE_STR) continue;
                    if (attribute.Name == FieldSerializer.TYPE_STR) where.ValueOrType = attribute.Value;
                    else
                    {
                        if (where.Obj is null) where.Obj = new Dictionary<string, ParsedObject>();
                        where.Obj.Add(attribute.Name, Fielder.StringSerializer.Parse(attribute));
                    }
                }
            }
        }
        public Fielder()
        {
            this.nullSerializer = new StringSerializer();
            this.AddSerializerForDerivedClassed(typeof(object), new ObjectSerializer());
            this.AddSerializerForDerivedClassed(typeof(System.Collections.IEnumerable), new ListSerializer());
            this.AddSerializerForDerivedClassed(typeof(IDictionary<,>), new ListSerializer());
            this.AddSerializerForDerivedClassed(typeof(bool), new StringSerializer());
            this.AddSerializerForDerivedClassed(typeof(int), new StringSerializer());
            this.AddSerializerForDerivedClassed(typeof(double), new StringSerializer());
            this.AddSerializerForDerivedClassed(typeof(string), new StringSerializer());
            this.AddSerializerForDerivedClassed(typeof(Boolean), new StringSerializer());
            this.AddSerializerForDerivedClassed(typeof(UInt16), new StringSerializer());
            this.AddSerializerForDerivedClassed(typeof(UInt32), new StringSerializer());
            this.AddSerializerForDerivedClassed(typeof(UInt64), new StringSerializer());
            this.AddSerializerForDerivedClassed(typeof(UIntPtr), new StringSerializer());
            this.AddSerializerForDerivedClassed(typeof(Char), new StringSerializer());
            this.AddSerializerForDerivedClassed(typeof(Byte), new StringSerializer());
            this.AddSerializerForDerivedClassed(typeof(SByte), new StringSerializer());
            this.AddSerializerForDerivedClassed(typeof(Decimal), new StringSerializer());
            this.AddSerializerForDerivedClassed(typeof(String), new StringSerializer());
            this.AddSerializerForDerivedClassed(typeof(KeyValuePair<,>), new KeyValuePairSerializer());
        }
        public static bool Serialize(string filePath, object what, Fielder fielder)
        {
            if (what is null) return true;
            try
            {
                var doc = new XmlDocument();
                var serializer = fielder.FindSerializer(what.GetType());
                string className = ROOT_ELEMENT;
                if (what is object)
                {
                    var typeAttribute = Attribute.GetCustomAttribute(what.GetType(), typeof(XmlTypeAttribute)) as XmlTypeAttribute;
                    if (typeAttribute is object) className = typeAttribute.TypeName;
                    else className = serializer.GetTypeNameOf(what);
                }
                serializer.Serialize(what, className, doc, doc, true, true);
                doc.Save(filePath);
                Console.WriteLine(doc.ToString());
                return true;
            }
            catch
            {
                Console.WriteLine("cannot serialize to xml(" + filePath + ")");
                return false;
            }
        }
        public static bool Serialize(string filePath, object what)
        {
            return Serialize(filePath, what, new Fielder());
        }
        public static bool NaviteSerialize(string filePath, object what)
        {
            try
            {
                var serializer = new XmlSerializer(what.GetType());
                TextWriter writer = new StreamWriter(filePath);
                serializer.Serialize(writer, what);
                writer.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
        private static ParsedObject GetParsedData(XmlNode xmlNode, string xmlFile)
        {
            try
            {
                var child = xmlNode.FirstChild;
                while (child is object && (child.NodeType == XmlNodeType.XmlDeclaration)) child = child.NextSibling;
                if (child is object)
                {
                    var ret = Fielder.ParseWhatteverNode(child);
                    return ret;
                }
                return null;
            }
            catch
            {
                Console.WriteLine("could not parse " + xmlFile);
                return null;
            }
        }
        public static ParsedObject ParseToDict(string xmlFile)
        {
            if (!File.Exists(xmlFile)) return null;
            var doc = new XmlDocument();
            doc.Load(xmlFile);
            return GetParsedData(doc, xmlFile);
        }
        public static ParsedObject ParseContentToDict(string xmlFileContent)
        {
            if (string.IsNullOrEmpty(xmlFileContent)) return null;
            var doc = new XmlDocument();
            doc.LoadXml(xmlFileContent);
            return GetParsedData(doc, "content");
        }
    }
}