﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Fielder
{
    public abstract class FieldSerializer
    {
        internal const string TYPE_STR = "_innerType";
        internal const string MY_TYPE_STR = "_myType";
        internal const string ARRAY_PREFIX = "ArrayOf";
        internal const string LIST_PREFIX = "List";
        internal const string KEY_VALUE_PAIR_PREFIX = "KeyValuePair";
        internal const string ARRAY_TYPE = "[]";
        internal const string DICTIONARY_TYPE = "dictionary";
        internal const string STRING_TYPE = "string";

        internal Fielder Fielder { get; set; }

        public abstract ParsedObject Parse(XmlNode value);
        public abstract void Serialize(object fromWhat, string name, XmlNode into, XmlDocument creator, bool toElement, bool withoutTypeString);

        protected string StringFromField(object what)
        {
            string nullStr = "null";
            if (what is null) return nullStr;
            else return what.ToString();
        }
        internal static ParsedObject CreateObject()
        {
            var o = new ParsedObject();
            return o;
        }
        public static FieldSerializer GetFieldSerializerFromElement(object o)
        {
            if (o is null) return new StringSerializer();
            if (o is string) return new StringSerializer();
            if (o is IEnumerable) return new ListSerializer();
            if (o is ICollection) return new ListSerializer();
            var valueType = o.GetType();
            if (valueType.IsGenericType)
            {
                Type baseType = valueType.GetGenericTypeDefinition();
                if (baseType == typeof(KeyValuePair<,>)) return new KeyValuePairSerializer();
            }
            if (o.GetType().IsClass) return new ObjectSerializer();
            return new StringSerializer();
        }
        private static XmlNode escapingNode;
        private XmlNode GetEscapingNode()
        {
            if (escapingNode is null)
            {
                XmlDocument doc = new XmlDocument();
                escapingNode = doc.CreateElement("root");
            }
            return escapingNode;
        }
        internal string NormalizeName(string name)
        {
            //<xxx>k_BackingField chcem zmenit na xxx (take su nazvy ak chcem serializovat auto propertu)
            var ltp = name.IndexOf("<");
            var gtp = name.IndexOf(">");
            if ((ltp >= 0) && (gtp > 0) && (ltp < gtp)) name = name.Substring(ltp + 1, gtp - ltp - 1);
            var indexOfApostrof = name.IndexOf("`");
            if (indexOfApostrof >= 0) name = name.Substring(0, indexOfApostrof);
            name = this.XmlEscape(name);
            return name;
        }
        internal string XmlEscape(string unescaped)
        {
            var node = this.GetEscapingNode();
            node.InnerText = unescaped;
            return node.InnerXml;
        }
        internal string XmlUnescape(string escaped)
        {
            var node = this.GetEscapingNode();
            node.InnerXml = escaped;
            return node.InnerText;
        }
        public virtual string GetTypeName(object ofObject)
        {
            return this.GetTypeName(ofObject.GetType());
        }
        public virtual string GetTypeNameOf(object ofObject)
        {
            var ret = this.GetTypeName(ofObject.GetType());
            ret = ret.Replace("[", "Of");
            ret = ret.Replace("]", "");
            ret = ret.Replace(",", "");
            return ret;
        }
        protected string GetTypeName(Type ofType)
        {
            var name = this.NormalizeName(ofType.Name);
            if (ofType.IsGenericType)
            {
                name += "[";
                var genericTypes = ofType.GenericTypeArguments;
                var counOfGenerics = genericTypes.Length;
                for (int i = 0; i < counOfGenerics; i++)
                {
                    var genericType = genericTypes[i];
                    name += this.GetTypeName(genericType);
                    if (i != counOfGenerics - 1) name += ",";
                }
                name += "]";
            }
            return name;
        }
    }
}
