﻿using System.Xml;

namespace Fielder
{
    public class StringSerializer : FieldSerializer
    {
        public override ParsedObject Parse(XmlNode value)
        {
            var text = value.Value;
            if (text is null) text = value.InnerText;
            var instance = new ParsedObject
            {
                ThisType = DataType.STRING,
                ValueOrType = text
            };
            return instance;
        }
        public override void Serialize(object what, string name, XmlNode where, XmlDocument creator, bool toElement, bool withoutTypeString)
        {
            name = this.NormalizeName(name);
            if (toElement)
            {
                var thisElement = creator.CreateElement("", name, "");
                if (what is object) thisElement.SetAttribute("value", what.ToString());
                if (!withoutTypeString) thisElement.SetAttribute(TYPE_STR, STRING_TYPE);
                where.AppendChild(thisElement);
            }
            else
            {
                if (where is XmlElement) ((XmlElement)where).SetAttribute(name, this.StringFromField(what));
            }
           // where.SetAttribute(name, stringFromField(what));
        }
    }
}
