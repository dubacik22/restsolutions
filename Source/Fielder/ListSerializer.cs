﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Fielder
{
    public class ListSerializer : FieldSerializer
    {
        private static bool HaveArrayAttribute(XmlNode what)
        {
            if (what.Attributes is null) return false;
            if (what.Attributes.Count > 2) return false;
            XmlAttribute myTypeAttr = what.Attributes[MY_TYPE_STR];
            if (myTypeAttr is null) return false;
            if (myTypeAttr.Value == LIST_PREFIX) return true;
            return false;
        }
        private static bool HaveArrayName(XmlNode what)
        {
            if (what.Name.StartsWith(ARRAY_PREFIX)) return true;
            return false;
        }
        public static bool IsArrayObject(XmlNode what)
        {
            if (HaveArrayAttribute(what)) return true;
            if (HaveArrayName(what)) return true;
            return false;
        }
        public override ParsedObject Parse(XmlNode value)
        {
            var instance = FieldSerializer.CreateObject();
            instance.ThisType = DataType.ARRAY;
            instance.Enu = new List<ParsedObject>();

            Fielder.ParseWhatteverAtribute(value, instance);

            var children = value.ChildNodes;
            for (int i = 0; i < children.Count; i++)
            {
                var child = children[i];
                if (child.NodeType.Equals(XmlNodeType.Element)) instance.Enu.Add(Fielder.ParseWhatteverNode(child));
            }
            return instance;
        }

        private XmlElement CreateElementWithoutType(object what, string name, XmlDocument creator)
        {
            name = this.NormalizeName(name);
            var listElement = creator.CreateElement("", name, "");

            var list = (IEnumerable)what;
            foreach (object element in list)
            {
                FieldSerializer serializer = this.Fielder.FindSerializer(element.GetType());
                if (serializer != null)
                {
                    if (element is null) serializer.Serialize(null, "null", listElement, creator, true, false);
                    else
                    {
                        string className;
                        var typeAttribute = Attribute.GetCustomAttribute(element.GetType(), typeof(System.Xml.Serialization.XmlTypeAttribute)) as System.Xml.Serialization.XmlTypeAttribute;
                        if (typeAttribute is object) className = typeAttribute.TypeName;
                        else className = this.GetTypeNameOf(element);
                        serializer.Serialize(element, className, listElement, creator, false, true);
                    }
                }
            }

            return listElement;
        }
        public override void Serialize(object what, string name, XmlNode into, XmlDocument creator, bool toElement, bool withoutType)
        {
            var innerNode = this.CreateElementWithoutType(what, name, creator);
            if (!withoutType)
            {
                var typeName = this.GetTypeName(what);
                innerNode.SetAttribute(TYPE_STR, typeName);
            }
            innerNode.SetAttribute(MY_TYPE_STR, LIST_PREFIX);
            into.AppendChild(innerNode);
        }
    }
}
