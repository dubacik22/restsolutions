﻿
namespace Fielder
{
    public enum DataType
    {
        STRING,
        ARRAY,
        OBJECT
    }
}
