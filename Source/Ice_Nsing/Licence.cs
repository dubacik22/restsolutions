﻿using System;
using System.IO;

namespace Ice_Nsing
{
    public class Licence
    {
        private DateTime? licDate = null;
        private bool _isValid = false;
        public bool IsValid { get => this._isValid; }
        public DateTime? LicDate { get => this.licDate; }

        private string _message = "nenacitana licencia";
        public string Message { get => this._message; }
        
        
        public bool CheckLicDate()
        {
            return LicencyHelper.CheckLicDate(this.licDate, out this._message);
        }

        public static Licence GetLicence(string filename, string appendix)
        {
            Licence ret = new Licence();

            try
            {
                if (File.Exists(filename))
                {
                    var licKey = File.ReadAllText(filename);
                    return Licence.GetLicenceFromKey(licKey, appendix);
                }
                else
                {
                    ret._message = "nenasla sa licencia";
                    return ret;
                }
            }
            catch
            {
                ret._message = "nepodarilo sa precitat licenciu";
                return ret;
            }
        }
        public static Licence GetLicenceFromKey(string licKey, string appendix)
        {
            Licence ret = new Licence();

            try
            {
                if (!LicencyHelper.CheckLicValidity(licKey, appendix, out ret._message, ref ret.licDate)) return ret;
            }
            catch
            {
                ret._message = "nepodarilo sa precitat licenciu";
                return ret;
            }
            ret._isValid = true;
            return ret;
        }
    }
}
