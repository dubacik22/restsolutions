﻿using System;
using System.IO;
using System.Linq;
using System.Management;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace Ice_Nsing
{
    public class LicencyHelper
    {
        internal static bool CheckLicValidity(string licKey, string licAppendix, out string message, ref DateTime? licDate)
        {
            var s = new StringBuilder(licKey);
            var appendix = new StringBuilder();
            try
            {
                LicencyHelper.Unzip2Strings(s, appendix, 3, 1);
                string licHash = new string(s.ToString().ToCharArray());
                //vytvorime hash
                //najskor Hardware Info
                var HI = LicencyHelper.GetHI();
                s.Clear();
                s.Append(HI.Item1);
                s.Append(HI.Item2);
                s.Append(licAppendix);
                s.Append("ka.-");
                LicencyHelper.CreateHash(s, out double sum);
                //skontrolujem licencny kluc
                if (s.ToString() != licHash)
                {
                    message = "licencia ma neplatny kluc";
                    return false;
                }
                //ziskam datum
                double.TryParse(appendix.ToString(), out double ddate);
                //odpocitam sumu
                ddate -= sum;
                if (ddate == 0)
                {
                    licDate = null;
                    message = string.Empty;
                    return true;
                }
                //teraz by som mal mat datum
                string sdate = ((long)ddate).ToString();
                if (sdate.Length == 7) sdate = "0" + sdate;
                if (sdate.Length != 8)
                {
                    message = "licencia ma neplatny datum";
                    return false;
                }
                //porovnam datum
                int.TryParse(sdate.Substring(0, 2), out int day);
                int.TryParse(sdate.Substring(2, 2), out int month);
                int.TryParse(sdate.Substring(4, 4), out int year);

                licDate = new DateTime(year, month, day);
                return LicencyHelper.CheckLicDate(licDate, out message);
            }
            catch
            {
                message = "licencia je neplatna";
                return false;
            }
        }
        internal static bool CheckLicDate(DateTime? licDate, out string message)
        {
            if (licDate.HasValue && licDate < DateTime.UtcNow)
            {
                message = "licencia vyprsala";
                return false;
            }
            message = string.Empty;
            return true;
        }


        /// <summary>
        /// creates hash from arguments appended in one string, except last one.
        /// last have to be string like "x.y.z" where x is increment of days, y is imcrement of weeks and z is increment of years.
        /// today will be incremented with desired increment and result date (string representation) will be "zipsed" to hash
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public string CreateKey(string[] args)
        {
            //connect arguments to one string
            var sb = new StringBuilder();
            var appendDate = string.Empty;
            //at first, append HI
            var HI = LicencyHelper.GetHI();
            sb.Append(HI.Item1);
            sb.Append(HI.Item2);
            for (int i = 0; i < args.Length - 1; i++) sb.Append(args[i]);
            if (args.Length > 1) appendDate = args.Last();
            sb.Append("ka.-");

            //vytvorime hash
            LicencyHelper.CreateHash(sb, out double sum);
            //sreate few stuff with string
            //prepare date
            appendDate = appendDate.Replace(',', '.');
            var partDate = appendDate.Split('.');
            var appendDay = 0;
            var appendMonth = 0;
            var appendYear = 0;
            if (partDate.Length > 0) int.TryParse(partDate[0], out appendDay);
            if (partDate.Length > 1) int.TryParse(partDate[1], out appendMonth);
            if (partDate.Length > 2) int.TryParse(partDate[2], out appendYear);
            if ((appendDay == 0) && (appendMonth == 0) && (appendYear == 0)) appendDate = "00000000";
            else
            {
                var licValidity = DateTime.UtcNow.AddDays(appendDay).AddMonths(appendMonth).AddYears(appendYear);
                appendDate = licValidity.ToString("ddMMyyy");
            }
            double.TryParse(LicencyHelper.ClearDotsNStuff(appendDate), out double ddate);
            sum += ddate;
            appendDate = ((long)sum).ToString();
            //insert date
            LicencyHelper.Zip2Strings(sb, appendDate, 3, 1);
            var licKey = new string(sb.ToString().ToCharArray());
            return licKey;
        }

        private static Tuple<string, string> GetHI()
        {
            var volumeSerial = "cannotGetDiskSerial";
            var volumeSerialThread = new Thread(() =>
            {
                try
                {
                    volumeSerial = GetVolumeSerial(Path.GetPathRoot(Environment.GetFolderPath(Environment.SpecialFolder.System)).Substring(0, 1));
                }
                catch
                {
                }
            });
            volumeSerialThread.Start();

            var cpuId = "cannotGetCpuId";
            var cpuIdThread = new Thread(() =>
            {
                try
                {
                    cpuId = GetCpuId();
                }
                catch
                {
                }
            });
            cpuIdThread.Start();

            var wait = LicencyHelper.KillThread(volumeSerialThread, true);
            wait = LicencyHelper.KillThread(cpuIdThread, wait);

            return new Tuple<string, string>(volumeSerial, cpuId);
        }
        private static string GetVolumeSerial(string strDriveLetter)
        {
            try
            {
                var ret = string.Empty;
                if (strDriveLetter == "" || strDriveLetter == null) strDriveLetter = "C";
                var disk = new ManagementObject("win32_logicaldisk.deviceid=\"" + strDriveLetter + ":\"");
                disk.Get();
                if (disk is object) ret = disk["VolumeSerialNumber"].ToString();
                return ret;
            }
            catch
            {
                return null;
            }
        }
        private static string GetMacAddress()
        {
            try
            {
                var ret = string.Empty;
                foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (nic.Description.ToLower().Contains("wi"))
                    {
                        ret = nic.GetPhysicalAddress().ToString();
                        break;
                    }
                }
                return ret;
            }
            catch
            {
                return null;
            }
        }
        private static string GetCpuId()
        {
            try
            {
                string ret = null;
                var mo = new ManagementObjectSearcher("select * from Win32_Processor");
                foreach (var item in mo.Get())
                {
                    ret = item["ProcessorId"].ToString();
                }
                return ret;
            }
            catch
            {
                return null;
            }
        }
        private static bool KillThread(Thread thread, bool wait)
        {
            if (wait && thread.Join(2000)) return true;
            thread.Interrupt();
            return false;
        }

        /// <summary>
        /// create hash from string an returns its sum or whatewer
        /// </summary>
        /// <param name="s"></param>
        /// <param name="sum"></param>
        private static void CreateHash(StringBuilder s, out double sum)
        {
            var x = new MD5CryptoServiceProvider();
            var bs = Encoding.UTF32.GetBytes(s.ToString());
            //vypocitame hash
            bs = x.ComputeHash(bs);
            //vytvorime trochu zaujimavejsi string z hashu
            s.Clear();
            var select = 1;
            sum = 0;
            foreach (var b in bs)
            {
                sum += b * Math.Pow(10.0, select);
                switch (select)
                {
                    case 1:
                        s.Append(b.ToString("x4").ToLower());
                        break;
                    case 2:
                        s.Append(b.ToString("x3").ToLower());
                        break;
                    case 3:
                        s.Append(b.ToString("x2").ToLower());
                        break;
                    case 4:
                        s.Append(b.ToString("x1").ToLower());
                        break;
                    default:
                        break;
                }
                select++;
                if (select > 4) select = 1;
            }
            LicencyHelper.NormaiseZeros(s);
            LicencyHelper.NormaliseLength(s);
        }
        /// <summary>
        /// erases zeros from string only if there is more immediately behind each other
        /// </summary>
        /// <param name="where"></param>
        private static void NormaiseZeros(System.Text.StringBuilder where)
        {
            var prevZero = false;
            char ch;
            var s = where.ToString();
            where.Clear();
            for (var i = 0; i < s.Length; i++)
            {
                ch = s[i];
                if (ch == '0')
                {
                    if (!prevZero)
                    {
                        prevZero = true;
                        where.Append(ch);
                    }
                }
                else
                {
                    prevZero = false;
                    where.Append(ch);
                }

            }
        }
        /// <summary>
        /// shorten string to specific length
        /// </summary>
        /// <param name="where"></param>
        private static void NormaliseLength(System.Text.StringBuilder where)
        {
            where.Length = 24;
        }
        /// <summary>
        /// erases everything that is not a number from string
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        private static string ClearDotsNStuff(string where)
        {
            var sb = new StringBuilder();
            char ch;
            for (var i = 0; i < where.Length; i++)
            {
                ch = where[i];
                if ((ch <= '9') && (ch >= '0')) sb.Append(ch);
            }
            return sb.ToString();
        }
        /// <summary>
        /// zips tvoo strings and puts result to "where"
        /// </summary>
        /// <param name="where"></param>
        /// <param name="append"></param>
        /// <param name="z1"></param>
        /// <param name="z2"></param>
        internal static void Zip2Strings(StringBuilder where, string append, int z1, int z2)
        {
            var s1 = new string(where.ToString().ToCharArray());
            var s2 = append;

            where.Clear();
            //int i = where.Length;
            //if (i > append.Length) i = append.Length;

            var i1 = 0; var ll1 = s1.Length;
            var i2 = 0; var ll2 = s2.Length;
            while ((s1.Length >= i1) && (s2.Length >= i2))
            {
                where.Append(s1.Substring(i1, (ll1 > z1) ? z1 : ll1));
                where.Append(s2.Substring(i2, (ll2 > z2) ? z2 : ll2));
                i1 += z1; ll1 -= z1;
                i2 += z2; ll2 -= z2;
            }
            if (s1.Length > i1) where.Append(s1.Substring(i1));
            if (s2.Length > i2) where.Append(s2.Substring(i2));
            ll1 += z1;
            ll2 += z2;
            where.Insert(z1, ll1.ToString("x2"));
            where.Insert(z2, ll2.ToString("x2"));
        }
        /// <summary>
        /// unzips string to tvoo stringBuilders "whatWhere" and "appendix"
        /// </summary>
        /// <param name="whatWhere"></param>
        /// <param name="appendix"></param>
        /// <param name="z1"></param>
        /// <param name="z2"></param>
        internal static void Unzip2Strings(StringBuilder whatWhere, StringBuilder appendix, int z1, int z2)
        {
            if ((z1 == 0) || (z2 == 0)) return;
            var what = new string(whatWhere.ToString().ToCharArray());
            whatWhere.Clear();
            appendix.Clear();

            var _ll2 = Convert.ToInt32(what.Substring(z2, 2), 16);
            what = what.Remove(z2, 2);
            var _ll1 = Convert.ToInt32(what.Substring(z1, 2), 16);
            what = what.Remove(z1, 2);

            var ll = what.Length - (_ll1 + _ll2); //dlzka casti ktore su pekne zozipsovane 

            var i = 0;
            while (what.Length > i)
            {
                whatWhere.Append(what.Substring(i, (i >= ll) ? _ll1 : z1));
                i += (i >= ll) ? _ll1 : z1;

                appendix.Append(what.Substring(i, (i >= ll) ? _ll2 : z2));
                i += (i >= ll) ? _ll2 : z2;
            }
        }

        private static int CalcBiggerZ(int lowerZ, int shorter, int bigger)
        {
            var cnt = Math.Ceiling((double)(shorter / lowerZ));
            return (int)Math.Floor((double)(bigger / cnt));
        }
    }
}
