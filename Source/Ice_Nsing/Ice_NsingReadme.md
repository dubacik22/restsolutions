library that contains object that represents licency. this licency is hardware locked, and can be valid only for some time (like year)
usage: at first we need to generate licency key. this key is generated by LicencyHelper.CreateKey(args)
args is string array. each part of that array is appendix except last one.
the last part of array is date increment.
appendix is any string that we need to provide later to check the licence (like program name or whattever)
date increment is string like "x.y.z" where x is increment of days, y is imcrement of weeks and z is increment of years.
today will be incremented with desired increment and result date (string representation) will be written into licency key.
in case we want to have licency that is valid forever, we need to provide "0.0.0"
resulting string we need to save in some file. this file is the licency.

later we can call
Licence.GetLicence(fileName, appendix).
fileName: name of the file that should contain licency key.

Licence then contains property IsValid and Message