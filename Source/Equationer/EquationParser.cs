﻿using System.IO;
using System;
using Equationer.States;
using Equationer.Stacks;

namespace Equationer
{
    public class EquationParser
    {
        #region Class data
        const char myBracketOpenChar = '(';
        const char myBracketCloseChar = ')';
        const char myPlusChar = '+';
        const char myMinusChar = '-';
        const char myMultChar = '*';
        const char myDivChar = '/';
        const char myPowChar = '^';
        const char myComaChar = ',';
        const char mySpaceChar = ' ';
        const char myQuoteChar = '\'';
        const char myBackSlash = '\\';
        const char myOr = '|';
        const char myAnd = '&';
        const char myExclamation = '!';
        const char myEquals = '=';
        const char myLess = '<';
        const char myGreater = '>';
        const char myQuestionMark = '?';
        const char myColon = ':';
        const char myBigE = 'E';
        const char mySmallE = 'e';
        private ParserStates states;

        public Func<string, IGetVariable> FetchVariable { get; set; }
        public Func<string, IFunction> FetchFunction { get; set; }
        #endregion
        public ICalculatable Parse(string text)
        {
            if (string.IsNullOrEmpty(text)) return null;

            this.states = new ParserStates(text, this.FetchFunction, this.FetchVariable);
            this.states.StatesStack.Clear();
            this.states.ContextsStack.Clear();
            this.states.StatesStack.AddNewState(this.states.InitState);
            foreach (var ch in text) this.DoState(ch);
            if (this.states.StatesStack.Last is QuoteState) throw new InvalidDataException("unexpected ' on end of eqution");
            if (this.states.StatesStack.Last is OperationExceptState) this.states.StatesStack.ToPrevious();
            if (this.states.StatesStack.Last is TextOperationExpectState) this.states.StatesStack.ToPrevious();
            if (this.states.StatesStack.Last is EndBracketState) this.states.StatesStack.ToPrevious();
            if (this.states.StatesStack.Last is FunctionNameOrVariableState) this.states.StatesStack.ToPrevious();
            while (this.states.StatesStack.Last is ConditionFalseResultState)
            {
                this.states.ConditionFalseResultState.ConsumeCondition();
                this.states.StatesStack.ToPrevious();
            }
            if (this.states.StatesStack.Last is FunctionNameOrVariableState) this.states.StatesStack.ToPrevious();
            if (this.states.StatesStack.Count != 1) throw new InvalidDataException("the count of opening and closing bracken doesnt match");
            while (this.states.ContextsStack.Stack.Count > 1) this.states.ContextsStack.ConsumeLast();
            var context = this.states.ContextsStack.GetFirst();
            context.AddVariable();
            var parsed = context.GetValue();
            return parsed as ICalculatable;
        }

        private void DoState(char ch)
        {
            var currentState = this.states.StatesStack.Last;
            switch (ch)
            {
                case EquationParser.myBracketOpenChar:
                    currentState.OnStartBracket(ch);
                    return;
                case EquationParser.myBracketCloseChar:
                    currentState.OnEndBracket(ch);
                    return;
                case EquationParser.myPlusChar:
                    currentState.OnPlus(ch);
                    return;
                case EquationParser.myMinusChar:
                    currentState.OnMinus(ch);
                    return;
                case EquationParser.myMultChar:
                    currentState.OnMultiply(ch);
                    return;
                case EquationParser.myDivChar:
                    currentState.OnDivide(ch);
                    return;
                case EquationParser.myPowChar:
                    currentState.OnPower(ch);
                    return;
                case EquationParser.myOr:
                    currentState.OnOr(ch);
                    return;
                case EquationParser.myAnd:
                    currentState.OnAnd(ch);
                    return;
                case EquationParser.myExclamation:
                    currentState.OnExclamation(ch);
                    return;
                case EquationParser.myEquals:
                    currentState.OnEquals(ch);
                    return;
                case EquationParser.myLess:
                    currentState.OnLess(ch);
                    return;
                case EquationParser.myGreater:
                    currentState.OnGreater(ch);
                    return;
                case EquationParser.myComaChar:
                    currentState.OnComa(ch);
                    return;
                case EquationParser.mySpaceChar:
                    currentState.OnSpace(ch);
                    return;
                case EquationParser.myQuoteChar:
                    currentState.OnQuote(ch);
                    return;
                case EquationParser.myBackSlash:
                    currentState.OnBackSlash(ch);
                    return;
                case EquationParser.myQuestionMark:
                    currentState.OnQuestionMark(ch);
                    return;
                case EquationParser.myColon:
                    currentState.OnColon(ch);
                    return;
                case EquationParser.myBigE:
                    currentState.OnE(ch);
                    return;
                case EquationParser.mySmallE:
                    currentState.OnE(ch);
                    return;
                default:
                    currentState.OnAnyChar(ch);
                    return;
            }
        }
    }
}