﻿using Equationer.Operations;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Equationer.Stacks
{
    internal class ParserContext
    {
        private readonly StringBuilder partBuilder = new StringBuilder();
        private readonly LinkedList<ValueBase> variableList = new LinkedList<ValueBase>();
        private IEquationPart firstValue;
        private IEquationPart lastValue;
        private string OrigString { get; }
        private Func<string, IFunction> FunctionFetcher { get; }
        private Func<string, IGetVariable> VariableFetcher { get; }

        public ParserContext(string origString, Func<string, IFunction> functionFetcher, Func<string, IGetVariable> variableFetcher)
        {
            this.OrigString = origString;
            this.FunctionFetcher = functionFetcher;
            this.VariableFetcher = variableFetcher;
        }
        #region public
        public void AddStringToBuilder(string str)
        {
            this.partBuilder.Append(str);
        }
        public void AddCharToBuilder(char ch)
        {
            this.partBuilder.Append(ch);
        }
        public void AddVariable()
        {
            if (this.partBuilder.Length < 1) return;
            this.AddToEquation(this.CreateValue());
            this.partBuilder.Clear();
        }
        public bool HaveEmptyBuilder()
        {
            return this.partBuilder.Length == 0;
        }
        public string GetBuilderContent()
        {
            return this.partBuilder.ToString();
        }
        public void ClearBuilderContent()
        {
            this.partBuilder.Clear();
        }
        public void PushToVariableList(ValueBase variable)
        {
            this.variableList.AddLast(variable);
        }
        public void AddFunction(LinkedList<ValueBase> variables)
        {
            this.AddToEquation(this.CreateFunction(variables));
            this.partBuilder.Clear();
        }
        public void AddTrueToLastCondition(ValueBase part)
        {
            if (!(this.lastValue is Condition condition)) throw new InvalidDataException("we have condition true result without condition");
            condition.TrueValue = part;
        }
        public void AddFalseToLastCondition(ValueBase part)
        {
            if (!(this.lastValue is Condition condition)) throw new InvalidDataException("we have condition true result without condition");
            condition.FalseValue = part;
        }
        public void AddToEquation(IEquationPart part)
        {
            if (part is null) return;
            if (this.lastValue is null)
            {
                this.firstValue = part;
                this.lastValue = part;
            }
            else
            {
                this.lastValue.Right = part;
                part.Left = this.lastValue;
                this.lastValue = part;
            }
        }
        public IEquationPart GetNRemoveLast()
        {
            if (this.lastValue is null) return null;
            var ret = this.lastValue;
            if (object.ReferenceEquals(this.firstValue, ret))
            {
                this.firstValue = null;
                this.lastValue = null;
                return ret;
            }
            this.lastValue = ret.Left;
            ret.Left = null;
            this.lastValue.Right = null;
            return ret;
        }
        public IEquationPart GetValue()
        {
            return this.firstValue;
        }
        public LinkedList<ValueBase> GetValues()
        {
            return this.variableList;
        }
        public void Clear()
        {
            this.firstValue = null;
            this.lastValue = null;
        }
        public override string ToString()
        {
            if (this.firstValue is null) return string.Empty;
            return this.firstValue.ToString();
        }
        public bool CanCreateConstant()
        {
            var maybeNum = this.partBuilder.ToString().Replace(',', '.').Trim();
            if (double.TryParse(maybeNum, out _)) return true;
            return false;
        }
        public bool IsEmpty()
        {
            if (this.partBuilder.Length > 0) return false;
            if (this.firstValue is object) return false;
            if (this.lastValue is object) return false;
            return true;
        }

        #endregion
        #region private
        private ValueBase CreateFunction(IEnumerable<ValueBase> arguments)
        {
            var funcName = this.partBuilder.ToString().Trim();
            if (string.IsNullOrEmpty(funcName))
            {
                if (this.lastValue is Negate) return new Bracket(arguments?.FirstOrDefault());
                return null;
            }
            if (this.FunctionFetcher is null) throw new InvalidDataException("we are not expecting functions (dont have funtion fetcher) and the function is here: " + funcName + " in eq (" + this.OrigString + ")");
            var function = this.FunctionFetcher(funcName);
            if (function is null) throw new InvalidDataException("fetcher failed to fetch function '" + funcName + "'");
            return new Function(function) { Arguments = arguments, Name = funcName };
        }
        private IValue CreateValue()
        {
            var partName = this.partBuilder.ToString();
            if (string.IsNullOrEmpty(partName)) return null;
            if (partName == "null") return new Constant(null);
            if (partName == "NaN") return new Constant(double.NaN);
            if (partName == "PositiveInfinity") return new Constant(double.PositiveInfinity);
            if (partName == "NegativeInfinity") return new Constant(double.NegativeInfinity);
            if (partName.ToLower() == "true") return new Constant(true);
            if (partName.ToLower() == "false") return new Constant(false);
            var stringConstant = this.TryGetStringConstant(partName);
            if (stringConstant is object) return new Constant(stringConstant);
            var doubleConstant = this.TryGetDoubleConstant(partName);
            if (doubleConstant is object) return new Constant(doubleConstant);

            if (this.VariableFetcher is null) throw new InvalidDataException("we are not expecting variables (dont have variable fetcher) and the variable is here: " + partName + " in eq (" + this.OrigString + ")");
            var variable = this.VariableFetcher(partName);
            if (variable is null) throw new InvalidDataException("fetcher failed to fetch variable for '" + partName + "'");
            return new Variable(variable) { Name = partName };
        }
        private string TryGetStringConstant(string partName)
        {
            if (string.IsNullOrEmpty(partName)) return null;
            var first = partName.FirstOrDefault();
            if (first != '\'') return null;
            var last = partName.LastOrDefault();
            if (last == '\'') return partName.Substring(1, partName.Length - 2);
            return partName.Substring(1, partName.Length - 1);
        }
        private double? TryGetDoubleConstant(string partName)
        {
            if (string.IsNullOrEmpty(partName)) return null;
            var maybeNum = partName.Replace(',', '.').Trim();
            if (((partName[0] < '0') || (partName[0] > '9')) && (partName[0] != '-')) return null;
            if (double.TryParse(maybeNum, NumberStyles.Any, CultureInfo.InvariantCulture, out double res)) return res;
            throw new InvalidDataException("invalid number '" + partName + "'");
        }

        #endregion
    }
}
