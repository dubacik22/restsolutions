using System;
using System.Collections.Generic;
using Equationer.Operations;

namespace Equationer.Stacks
{
    internal class ParserContextStack
    {
        private string OrigString { get; }
        private Func<string, IFunction> FunctionFetcher { get; }
        private Func<string, IGetVariable> VariableFetcher { get; }
        public readonly LinkedList<ParserContext> Stack = new LinkedList<ParserContext>();
        public ParserContext Last { get => this.Stack.Last.Value; }
        public ParserContextStack(string origString, Func<string, IFunction> functionFetcher, Func<string, IGetVariable> variableFetcher)
        {
            this.OrigString = origString;
            this.FunctionFetcher = functionFetcher;
            this.VariableFetcher = variableFetcher;
        }
        public void AddNew()
        {
            this.Stack.AddLast(new ParserContext(this.OrigString, this.FunctionFetcher, this.VariableFetcher));
        }
        public bool ConsumeLast()
        {
            if (this.Stack.Count <= 1) return false;
            var last = this.Stack.Last.Value;
            last.AddVariable();
            var former = this.Stack.Last.Previous.Value;
            former.AddToEquation(last.GetValue());
            this.Stack.RemoveLast();
            return true;
        }
        public void ConsumeLastAsBrackets()
        {
            if (this.Stack.Count == 1) return;
            var lastValue = this.Stack.Last.Value.GetValue() as ValueBase;
            var former = this.Stack.Last.Previous.Value;
            former.AddToEquation(new Bracket(lastValue));
            this.Stack.RemoveLast();
        }
        public void ConsumeLastAsCondition()
        {
            var lastContext = this.Stack.Last.Value;
            var lastValue = lastContext.GetValue() as ValueBase;
            lastContext.Clear();
            lastContext.AddToEquation(new Condition(lastValue));
        }
        public void ConsumeLastAsTrue()
        {
            if (this.Stack.Count == 1) return;
            var lastValue = this.Stack.Last.Value.GetValue() as ValueBase;
            var former = this.Stack.Last.Previous.Value;
            former.AddTrueToLastCondition(lastValue);
            this.Stack.RemoveLast();
        }
        public void ConsumeLastAsFalse()
        {
            if (this.Stack.Count == 1) return;
            var lastValue = this.Stack.Last.Value.GetValue() as ValueBase;
            var former = this.Stack.Last.Previous.Value;
            former.AddFalseToLastCondition(lastValue);
            this.Stack.RemoveLast();
        }
        public void ConsumeLastAsFunction()
        {
            var last = this.Stack.Last.Value;
            var former = this.Stack.Last.Previous.Value;
            former.AddFunction(last.GetValues());
            this.Stack.RemoveLast();
        }
        public void Clear()
        {
            this.Stack.Clear();
        }
        public ParserContext GetFirst()
        {
            return this.Stack.First.Value;
        }
        public override string ToString()
        {
            return this.Stack.First?.Value?.ToString();
        }
    }
}
