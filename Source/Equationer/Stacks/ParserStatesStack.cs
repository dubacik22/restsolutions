﻿using Equationer.States;
using System.Collections.Generic;

namespace Equationer.Stacks
{
    internal class ParserStatesStack
    {
        private readonly Stack<ParserState> Stack = new Stack<ParserState>();
        public int Count
        {
            get => this.Stack.Count;
        }
        public ParserState Last
        {
            get
            {
                if (this.Count == 0) return null;
                return this.Stack.Peek();
            }
        }
        public ParserState Previous
        {
            get
            {
                if (this.Count < 2) return null;
                var last = this.Stack.Pop();
                var ret = this.Stack.Peek();
                this.Stack.Push(last);
                return ret;
            }
        }
        public ParserState AddNewState(ParserState state)
        {
            this.Stack.Push(state);
            return state;
        }
        public ParserState ToPrevious()
        {
            this.Stack.Pop();
            if (this.Stack.Count == 0) return null;
            return this.Stack.Peek();
        }
        public void Clear()
        {
            this.Stack.Clear();
        }
    }
}
