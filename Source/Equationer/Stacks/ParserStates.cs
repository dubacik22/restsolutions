﻿using Equationer.States;
using System;

namespace Equationer.Stacks
{
    internal class ParserStates
    {
        private string OrigString { get; }
        public ParserStatesStack StatesStack { get; set; }
        public ParserContextStack ContextsStack { get; set; }

        private AfterOperationState afterOperationState;
        public AfterOperationState AfterOperationState
        {
            get
            {
                if (this.afterOperationState is object) return this.afterOperationState;
                this.afterOperationState = new AfterOperationState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.afterOperationState;
            }
        }

        private BracketState bracketState;
        public BracketState BracketState
        {
            get
            {
                if (this.bracketState is object) return this.bracketState;
                this.bracketState = new BracketState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.bracketState;
            }
        }

        private ConditionFalseResultState conditionFalseResultState;
        public ConditionFalseResultState ConditionFalseResultState
        {
            get
            {
                if (this.conditionFalseResultState is object) return this.conditionFalseResultState;
                this.conditionFalseResultState = new ConditionFalseResultState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.conditionFalseResultState;
            }
        }

        private ConditionTrueResultState conditionTrueResultState;
        public ConditionTrueResultState ConditionTrueResultState
        {
            get
            {
                if (this.conditionTrueResultState is object) return this.conditionTrueResultState;
                this.conditionTrueResultState = new ConditionTrueResultState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.conditionTrueResultState;
            }
        }

        private EndBracketState endBracketState;
        public EndBracketState EndBracketState
        {
            get
            {
                if (this.endBracketState is object) return this.endBracketState;
                this.endBracketState = new EndBracketState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.endBracketState;
            }
        }

        private EscapedState escapedState;
        public EscapedState EscapedState
        {
            get
            {
                if (this.escapedState is object) return this.escapedState;
                this.escapedState = new EscapedState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.escapedState;
            }
        }

        private ExponentState exponentState;
        public ExponentState ExponentState
        {
            get
            {
                if (this.exponentState is object) return this.exponentState;
                this.exponentState = new ExponentState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.exponentState;
            }
        }

        private FunctionArgumentsState functionArgumentsState;
        public FunctionArgumentsState FunctionArgumentsState
        {
            get
            {
                if (this.functionArgumentsState is object) return this.functionArgumentsState;
                this.functionArgumentsState = new FunctionArgumentsState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.functionArgumentsState;
            }
        }

        private FunctionNameOrVariableState functionNameOrVariableState;
        public FunctionNameOrVariableState FunctionNameOrVariableState
        {
            get
            {
                if (this.functionNameOrVariableState is object) return this.functionNameOrVariableState;
                this.functionNameOrVariableState = new FunctionNameOrVariableState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.functionNameOrVariableState;
            }
        }

        private InitState initState;
        public InitState InitState
        {
            get
            {
                if (this.initState is object) return this.initState;
                this.initState = new InitState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.initState;
            }
        }

        private OperationAndState operationAndState;
        public OperationAndState OperationAndState
        {
            get
            {
                if (this.operationAndState is object) return this.operationAndState;
                this.operationAndState = new OperationAndState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.operationAndState;
            }
        }

        private OperationEqState operationEqState;
        public OperationEqState OperationEqState
        {
            get
            {
                if (this.operationEqState is object) return this.operationEqState;
                this.operationEqState = new OperationEqState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.operationEqState;
            }
        }

        private OperationExceptState operationExpectState;
        public OperationExceptState OperationExpectState
        {
            get
            {
                if (this.operationExpectState is object) return this.operationExpectState;
                this.operationExpectState = new OperationExceptState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.operationExpectState;
            }
        }

        private OperationGqState operationGqState;
        public OperationGqState OperationGqState
        {
            get
            {
                if (this.operationGqState is object) return this.operationGqState;
                this.operationGqState = new OperationGqState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.operationGqState;
            }
        }

        private OperationLqState operationLqState;
        public OperationLqState OperationLqState
        {
            get
            {
                if (this.operationLqState is object) return this.operationLqState;
                this.operationLqState = new OperationLqState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.operationLqState;
            }
        }

        private OperationNeqState operationNeqState;
        public OperationNeqState OperationNeqState
        {
            get
            {
                if (this.operationNeqState is object) return this.operationNeqState;
                this.operationNeqState = new OperationNeqState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.operationNeqState;
            }
        }

        private OperationOrState operationOrState;
        public OperationOrState OperationOrState
        {
            get
            {
                if (this.operationOrState is object) return this.operationOrState;
                this.operationOrState = new OperationOrState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.operationOrState;
            }
        }

        private QuoteState quoteState;
        public QuoteState QuoteState
        {
            get
            {
                if (this.quoteState is object) return this.quoteState;
                this.quoteState = new QuoteState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.quoteState;
            }
        }

        private TextOperationExpectState textOperationExpectState;
        public TextOperationExpectState TextOperationExpectState
        {
            get
            {
                if (this.textOperationExpectState is object) return this.textOperationExpectState;
                this.textOperationExpectState = new TextOperationExpectState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.textOperationExpectState;
            }
        }

        private TextOperationState textOperationState;
        public TextOperationState TextOperationState
        {
            get
            {
                if (this.textOperationState is object) return this.textOperationState;
                this.textOperationState = new TextOperationState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.textOperationState;
            }
        }

        private ThrowerState throwerState;
        public ThrowerState ThrowerState
        {
            get
            {
                if (this.throwerState is object) return this.throwerState;
                this.throwerState = new ThrowerState(this.OrigString, this, this.StatesStack, this.ContextsStack);
                return this.throwerState;
            }
        }

        public ParserStates(string origString, Func<string, IFunction> functionFetcher, Func<string, IGetVariable> variableFetcher)
        {
            this.OrigString = origString;
            this.StatesStack = new ParserStatesStack();
            this.ContextsStack = new ParserContextStack(this.OrigString, functionFetcher, variableFetcher);
        }
    }
}
