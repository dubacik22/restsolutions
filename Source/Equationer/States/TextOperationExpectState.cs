﻿using Equationer.Stacks;

namespace Equationer.States
{
    internal class TextOperationExpectState : ParserState
    {
        public TextOperationExpectState(string origString, ParserStates states, ParserStatesStack statesStack, ParserContextStack contextsStack) : base(origString, states, statesStack, contextsStack) { }
        public override void OnAnyChar(char ch)
        {
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.TextOperationState);
            this.StatesStack.Last.OnAnyChar(ch);
        }
        public override void OnE(char ch)
        {
            this.OnAnyChar(ch);
        }

        public override void OnComa(char ch)
        {
            this.Throw(ch);
        }

        public override void OnStartBracket(char ch)
        {
            this.Throw(ch);
        }
        public override void OnEndBracket(char ch)
        {
            this.Throw(ch);
        }

        public override void OnPlus(char ch)
        {
            this.Throw(ch);
        }
        public override void OnMinus(char ch)
        {
            this.Throw(ch);
        }
        public override void OnMultiply(char ch)
        {
            this.Throw(ch);
        }
        public override void OnDivide(char ch)
        {
            this.Throw(ch);
        }
        public override void OnPower(char ch)
        {
            this.Throw(ch);
        }
        public override void OnOr(char ch)
        {
            this.Throw(ch);
        }
        public override void OnAnd(char ch)
        {
            this.Throw(ch);
        }
        public override void OnExclamation(char ch)
        {
            this.Throw(ch);
        }
        public override void OnEquals(char ch)
        {
            this.Throw(ch);
        }
        public override void OnLess(char ch)
        {
            this.Throw(ch);
        }
        public override void OnGreater(char ch)
        {
            this.Throw(ch);
        }
        public override void OnQuestionMark(char ch)
        {
            this.Throw(ch);
        }
        public override void OnColon(char ch)
        {
            this.Throw(ch);
        }

        public override void OnQuote(char ch)
        {
            this.Throw(ch);
        }
        public override void OnBackSlash(char ch)
        {
            this.Throw(ch);
        }
    }
}
