﻿using Equationer.Stacks;

namespace Equationer.States
{
    internal class EndBracketState : ParserState
    {
        public EndBracketState(string origString, ParserStates states, ParserStatesStack statesStack, ParserContextStack contextsStack) : base(origString, states, statesStack, contextsStack) { }
        public override void OnAnyChar(char ch)
        {
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.OperationExpectState);
            this.StatesStack.Last.OnAnyChar(ch);
        }
        public override void OnE(char ch)
        {
            this.Throw(ch);
        }
        public override void OnSpace(char ch)
        {
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.OperationExpectState);
        }
        public override void OnComa(char ch)
        {
            this.StatesStack.ToPrevious().OnComa(ch);
        }

        public override void OnStartBracket(char ch)
        {
            this.Throw(ch);
        }
        public override void OnEndBracket(char ch)
        {
            this.StatesStack.ToPrevious().OnEndBracket(ch);
        }

        public override void OnPlus(char ch)
        {
            this.StatesStack.ToPrevious();
            this.NormalOnPlus(ch);
        }
        public override void OnMinus(char ch)
        {
            this.StatesStack.ToPrevious();
            this.NormalOnMinus(ch);
        }
        public override void OnMultiply(char ch)
        {
            this.StatesStack.ToPrevious();
            this.NormalOnMultiply(ch);
        }
        public override void OnDivide(char ch)
        {
            this.StatesStack.ToPrevious();
            this.NormalOnDivide(ch);
        }
        public override void OnPower(char ch)
        {
            this.StatesStack.ToPrevious();
            this.NormalOnPower(ch);
        }
        public override void OnOr(char ch)
        {
            this.StatesStack.ToPrevious();
            this.NormalOnOr(ch);
        }
        public override void OnAnd(char ch)
        {
            this.StatesStack.ToPrevious();
            this.NormalOnAnd(ch);
        }
        public override void OnEquals(char ch)
        {
            this.ContextsStack.Last.AddVariable();
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.OperationEqState);
        }
        public override void OnLess(char ch)
        {
            this.StatesStack.ToPrevious();
            this.NormalOnLess(ch);
        }
        public override void OnGreater(char ch)
        {
            this.StatesStack.ToPrevious();
            this.NormalOnGreater(ch);
        }
        public override void OnQuestionMark(char ch)
        {
            this.NormalOnQuestionMark(ch);
        }
        public override void OnColon(char ch)
        {
            this.StatesStack.ToPrevious().OnColon(ch);
        }
        public override void OnExclamation(char ch)
        {
            this.ContextsStack.Last.AddVariable();
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.OperationNeqState);
        }
    }
}
