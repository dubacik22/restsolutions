﻿using Equationer.Stacks;

namespace Equationer.States
{
    internal class EscapedState : ParserState
    {
        public EscapedState(string origString, ParserStates states, ParserStatesStack statesStack, ParserContextStack contextsStack) : base(origString, states, statesStack, contextsStack) { }
        public override void OnAnyChar(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnE(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnComa(char ch)
        {
            this.ConsumeChar(ch);
        }

        public override void OnStartBracket(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnEndBracket(char ch)
        {
            this.ConsumeChar(ch);
        }

        public override void OnPlus(char ch)
        {
            base.OnAnyChar(ch);
            this.StatesStack.ToPrevious();
        }
        public override void OnMinus(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnMultiply(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnDivide(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnPower(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnOr(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnAnd(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnExclamation(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnEquals(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnLess(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnGreater(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnQuestionMark(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnColon(char ch)
        {
            this.ConsumeChar(ch);
        }

        public override void OnQuote(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnBackSlash(char ch)
        {
            this.ConsumeChar(ch);
        }
        public override void OnSpace(char ch)
        {
            this.ConsumeChar(ch);
        }

        private void ConsumeChar(char ch)
        {
            base.OnAnyChar(ch);
            this.StatesStack.ToPrevious();
        }
    }
}
