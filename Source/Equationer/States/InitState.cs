﻿using Equationer.Stacks;

namespace Equationer.States
{
    internal class InitState : ParserState
    {
        public InitState(string origString, ParserStates states, ParserStatesStack statesStack, ParserContextStack contextsStack) : base(origString, states, statesStack, contextsStack) { }
        public override void OnComa(char ch)
        {
            this.Throw(ch);
        }
        public override void OnAnyChar(char ch)
        {
            this.Begin();
            this.StatesStack.Last.OnAnyChar(ch);
        }
        public override void OnE(char ch)
        {
            this.OnAnyChar(ch);
        }
        public override void OnBackSlash(char ch)
        {
            this.Begin();
            this.StatesStack.AddNewState(this.ParserStates.EscapedState);
        }
        public override void OnQuote(char ch)
        {
            this.Begin();
            this.StatesStack.Last.OnQuote(ch);
        }

        public override void OnStartBracket(char ch)
        {
            this.Begin();
            this.ContextsStack.AddNew();
            this.StatesStack.AddNewState(this.ParserStates.BracketState);
        }
        public override void OnEndBracket(char ch)
        {
            this.Throw(ch);
        }

        public override void OnMinus(char ch)
        {
            this.Begin();
            this.AddMinusVariable(ch);
            //this.States.ToPrevious();
        }
        public override void OnPlus(char ch)
        {
            this.Throw(ch);
        }
        public override void OnMultiply(char ch)
        {
            this.Throw(ch);
        }
        public override void OnDivide(char ch)
        {
            this.Throw(ch);
        }
        public override void OnPower(char ch)
        {
            this.Throw(ch);
        }
        public override void OnOr(char ch)
        {
            this.Throw(ch);
        }
        public override void OnAnd(char ch)
        {
            this.Throw(ch);
        }
        public override void OnExclamation(char ch)
        {
            this.Begin();
            this.AddNegateVariable(ch);
        }
        public override void OnEquals(char ch)
        {
            this.Throw(ch);
        }
        public override void OnLess(char ch)
        {
            this.Throw(ch);
        }
        public override void OnGreater(char ch)
        {
            this.Throw(ch);
        }
        public override void OnQuestionMark(char ch)
        {
            this.Throw(ch);
        }
        public override void OnColon(char ch)
        {
            this.Throw(ch);
        }

        private void Begin()
        {
            this.ContextsStack.AddNew();
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.ThrowerState);
            this.StatesStack.AddNewState(this.ParserStates.FunctionNameOrVariableState);
        }
    }
}
