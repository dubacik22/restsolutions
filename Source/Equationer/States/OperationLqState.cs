﻿using Equationer.Operations;
using Equationer.Stacks;

namespace Equationer.States
{
    internal class OperationLqState : ParserState
    {
        public OperationLqState(string origString, ParserStates states, ParserStatesStack statesStack, ParserContextStack contextsStack) : base(origString, states, statesStack, contextsStack) { }
        public override void OnAnyChar(char ch)
        {
            this.AddOperandToContext(EOperand.Lt);
            this.StatesStack.ToPrevious();
            base.OnAnyChar(ch);
        }
        public override void OnE(char ch)
        {
            this.OnAnyChar(ch);
        }
        public override void OnSpace(char ch)
        {
            this.AddOperandToContext(EOperand.Lt);
        }
        public override void OnComa(char ch)
        {
            this.Throw(ch);
        }

        public override void OnStartBracket(char ch)
        {
            this.AddOperandToContext(EOperand.Lt);
            this.ContextsStack.AddNew();
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.BracketState);
        }
        public override void OnEndBracket(char ch)
        {
            this.Throw(ch);
        }

        public override void OnPlus(char ch)
        {
            this.Throw(ch);
        }
        public override void OnMinus(char ch)
        {
            this.AddMinusVariable(ch);
            this.StatesStack.ToPrevious();
        }
        public override void OnMultiply(char ch)
        {
            this.Throw(ch);
        }
        public override void OnDivide(char ch)
        {
            this.Throw(ch);
        }
        public override void OnPower(char ch)
        {
            this.Throw(ch);
        }
        public override void OnOr(char ch)
        {
            this.Throw(ch);
        }
        public override void OnAnd(char ch)
        {
            this.Throw(ch);
        }
        public override void OnExclamation(char ch)
        {
            this.Throw(ch);
        }
        public override void OnEquals(char ch)
        {
            this.AddOperandToContext(EOperand.Leq);
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.AfterOperationState);
        }
        public override void OnLess(char ch)
        {
            this.Throw(ch);
        }
        public override void OnGreater(char ch)
        {
            this.Throw(ch);
        }
        public override void OnQuestionMark(char ch)
        {
            this.Throw(ch);
        }
        public override void OnColon(char ch)
        {
            this.Throw(ch);
        }

        public override void OnQuote(char ch)
        {
            this.Throw(ch);
        }
        public override void OnBackSlash(char ch)
        {
            this.Throw(ch);
        }

        private void AddOperandToContext(EOperand operand)
        {
            var context = this.ContextsStack.Last;
            context.AddVariable();
            context.AddToEquation(operand.ToOperation());
            context.ClearBuilderContent();
        }
    }
}
