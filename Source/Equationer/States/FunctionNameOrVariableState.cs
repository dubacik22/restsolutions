﻿using Equationer.Operations;
using Equationer.Stacks;

namespace Equationer.States
{
    internal class FunctionNameOrVariableState : ParserState
    {
        public FunctionNameOrVariableState(string origString, ParserStates states, ParserStatesStack statesStack, ParserContextStack contextsStack) : base(origString, states, statesStack, contextsStack) { }
        public override void OnE(char ch)
        {
            if (!this.AddExponent()) this.OnAnyChar(ch);
        }
        public override void OnSpace(char ch)
        {
            var context = this.ContextsStack.Last;
            context.AddVariable();
            this.StatesStack.AddNewState(this.ParserStates.OperationExpectState);
        }
        public override void OnStartBracket(char ch)
        {
            var wasEmpty = this.ContextsStack.Last.HaveEmptyBuilder();
            this.ContextsStack.AddNew();
            this.StatesStack.ToPrevious();
            if (wasEmpty) this.StatesStack.AddNewState(this.ParserStates.BracketState);
            else this.StatesStack.AddNewState(this.ParserStates.FunctionArgumentsState);
        }
        public override void OnEndBracket(char ch)
        {
            this.StatesStack.ToPrevious().OnEndBracket(ch);
        }

        public override void OnPlus(char ch)
        {
            this.NormalOnPlus(ch);
        }
        public override void OnMinus(char ch)
        {
            this.NormalOnMinus(ch);
        }
        public override void OnMultiply(char ch)
        {
            this.NormalOnMultiply(ch);
        }
        public override void OnDivide(char ch)
        {
            this.NormalOnDivide(ch);
        }
        public override void OnPower(char ch)
        {
            this.NormalOnPower(ch);
        }
        public override void OnOr(char ch)
        {
            this.NormalOnOr(ch);
        }
        public override void OnAnd(char ch)
        {
            this.NormalOnAnd(ch);
        }
        public override void OnExclamation(char ch)
        {
            this.ContextsStack.Last.AddVariable();
            this.StatesStack.AddNewState(this.ParserStates.OperationNeqState);
        }
        public override void OnEquals(char ch)
        {
            this.NormalOnEquals(ch);
        }
        public override void OnLess(char ch)
        {
            this.NormalOnLess(ch);
        }
        public override void OnGreater(char ch)
        {
            this.NormalOnGreater(ch);
        }
        public override void OnQuestionMark(char ch)
        {
            this.Throw(ch);
        }
        public override void OnColon(char ch)
        {
            var previousState = this.StatesStack.Previous;
            if (previousState is ConditionTrueResultState) this.StatesStack.ToPrevious().OnColon(ch);
            else if (previousState is ConditionFalseResultState) this.StatesStack.ToPrevious().OnColon(ch);
            else this.OnAnyChar(ch);
        }

        public override void OnComa(char ch)
        {
            this.StatesStack.ToPrevious().OnComa(ch);
        }
    }
}
