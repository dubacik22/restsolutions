﻿using Equationer.Operations;
using Equationer.Stacks;

namespace Equationer.States
{
    internal class ConditionFalseResultState : ParserState
    {
        public ConditionFalseResultState(string origString, ParserStates states, ParserStatesStack statesStack, ParserContextStack contextsStack) : base(origString, states, statesStack, contextsStack) { }
        public override void OnAnyChar(char ch)
        {
            this.StatesStack.AddNewState(this.ParserStates.FunctionNameOrVariableState);
            this.StatesStack.Last.OnAnyChar(ch);
        }
        public override void OnE(char ch)
        {
            this.OnAnyChar(ch);
        }
        public override void OnStartBracket(char ch)
        {
            this.ContextsStack.AddNew();
            this.StatesStack.AddNewState(this.ParserStates.BracketState);
        }
        public override void OnEndBracket(char ch)
        {
            this.ConsumeCondition();
            this.StatesStack.ToPrevious().OnEndBracket(ch);
        }

        public override void OnPlus(char ch)
        {
            this.ConsumeCondition();
            this.ContextsStack.Last.AddToEquation(EOperand.Plus.ToOperation());
        }
        public override void OnMinus(char ch)
        {
            if (this.ContextsStack.Last.IsEmpty())
            {
                this.AddMinusVariable(ch);
            }
            else
            {
                this.ConsumeCondition();
                this.ContextsStack.Last.AddToEquation(EOperand.Minus.ToOperation());
            }
        }
        public override void OnMultiply(char ch)
        {
            this.ConsumeCondition();
            this.ContextsStack.Last.AddToEquation(EOperand.Multiply.ToOperation());
        }
        public override void OnDivide(char ch)
        {
            this.ConsumeCondition();
            this.ContextsStack.Last.AddToEquation(EOperand.Divide.ToOperation());
        }
        public override void OnPower(char ch)
        {
            this.ConsumeCondition();
            this.ContextsStack.Last.AddToEquation(EOperand.Power.ToOperation());
        }
        public override void OnOr(char ch)
        {
            this.ConsumeCondition();
            this.StatesStack.AddNewState(this.ParserStates.OperationOrState);
        }
        public override void OnAnd(char ch)
        {
            this.ConsumeCondition();
            this.StatesStack.AddNewState(this.ParserStates.OperationAndState);
        }
        public override void OnExclamation(char ch)
        {
            this.ConsumeCondition();
            this.StatesStack.AddNewState(this.ParserStates.OperationNeqState);
        }
        public override void OnEquals(char ch)
        {
            this.ConsumeCondition();
            this.StatesStack.AddNewState(this.ParserStates.OperationEqState);
        }
        public override void OnLess(char ch)
        {
            this.ConsumeCondition();
            this.StatesStack.AddNewState(this.ParserStates.OperationLqState);
        }
        public override void OnGreater(char ch)
        {
            this.ConsumeCondition();
            this.StatesStack.AddNewState(this.ParserStates.OperationGqState);
        }
        public override void OnQuestionMark(char ch)
        {
            this.Throw(ch);
        }
        public override void OnColon(char ch)
        {
            this.ConsumeCondition();
            this.StatesStack.ToPrevious().OnColon(ch);
        }

        public override void OnComa(char ch)
        {
            this.ConsumeCondition();
            this.StatesStack.ToPrevious().OnComa(ch);
        }

        public void ConsumeCondition()
        {
            this.ContextsStack.Last.AddVariable();
            this.ContextsStack.ConsumeLastAsFalse();
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.AfterOperationState);
        }
    }
}
