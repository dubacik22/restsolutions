﻿using Equationer.Operations;
using Equationer.Stacks;

namespace Equationer.States
{
    internal class TextOperationState : ParserState
    {
        public TextOperationState(string origString, ParserStates states, ParserStatesStack statesStack, ParserContextStack contextsStack) : base(origString, states, statesStack, contextsStack) { }
        public override void OnE(char ch)
        {
            this.OnAnyChar(ch);
        }
        public override void OnSpace(char ch)
        {
            var context = this.ContextsStack.Last;
            switch (context.GetBuilderContent())
            {
                case "and":
                    context.AddToEquation(EOperand.And.ToOperation());
                    break;
                case "or":
                    context.AddToEquation(EOperand.Or.ToOperation());
                    break;
                case "gteq":
                case "geq":
                    context.AddToEquation(EOperand.Geq.ToOperation());
                    break;
                case "gt":
                    context.AddToEquation(EOperand.Gt.ToOperation());
                    break;
                case "lteq":
                case "leq":
                    context.AddToEquation(EOperand.Leq.ToOperation());
                    break;
                case "lt":
                    context.AddToEquation(EOperand.Lt.ToOperation());
                    break;
                default:
                    this.Throw(ch);
                    return;
            }
            context.ClearBuilderContent();
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.AfterOperationState);
        }

        public override void OnComa(char ch)
        {
            this.Throw(ch);
        }

        public override void OnStartBracket(char ch)
        {
            this.OnSpace(' ');
            this.StatesStack.ToPrevious();
            this.ContextsStack.AddNew();
            this.StatesStack.AddNewState(this.ParserStates.BracketState);
        }
        public override void OnEndBracket(char ch)
        {
            this.Throw(ch);
        }

        public override void OnPlus(char ch)
        {
            var context = this.ContextsStack.Last;
            if (!context.HaveEmptyBuilder()) this.Throw(ch);
            context.AddVariable();
            context.AddToEquation(EOperand.Plus.ToOperation());
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.AfterOperationState);
        }
        public override void OnMinus(char ch)
        {
            var context = this.ContextsStack.Last;
            if (!context.HaveEmptyBuilder()) this.Throw(ch);
            context.AddVariable();
            context.AddToEquation(EOperand.Minus.ToOperation());
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.AfterOperationState);
        }
        public override void OnMultiply(char ch)
        {
            var context = this.ContextsStack.Last;
            if (!context.HaveEmptyBuilder()) this.Throw(ch);
            context.AddVariable();
            context.AddToEquation(EOperand.Multiply.ToOperation());
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.AfterOperationState);
        }
        public override void OnDivide(char ch)
        {
            var context = this.ContextsStack.Last;
            if (!context.HaveEmptyBuilder()) this.Throw(ch);
            context.AddVariable();
            context.AddToEquation(EOperand.Divide.ToOperation());
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.AfterOperationState);
        }
        public override void OnPower(char ch)
        {
            var context = this.ContextsStack.Last;
            if (!context.HaveEmptyBuilder()) this.Throw(ch);
            context.AddVariable();
            context.AddToEquation(EOperand.Power.ToOperation());
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.AfterOperationState);
        }
        public override void OnOr(char ch)
        {
            var context = this.ContextsStack.Last;
            if (!context.HaveEmptyBuilder()) this.Throw(ch);
            this.ContextsStack.Last.AddVariable();
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.OperationOrState);
        }
        public override void OnAnd(char ch)
        {
            var context = this.ContextsStack.Last;
            if (!context.HaveEmptyBuilder()) this.Throw(ch);
            this.ContextsStack.Last.AddVariable();
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.OperationAndState);
        }
        public override void OnExclamation(char ch)
        {
            var context = this.ContextsStack.Last;
            if (!context.HaveEmptyBuilder()) this.Throw(ch);
            this.ContextsStack.Last.AddVariable();
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.OperationNeqState);
        }
        public override void OnEquals(char ch)
        {
            var context = this.ContextsStack.Last;
            if (!context.HaveEmptyBuilder()) this.Throw(ch);
            this.ContextsStack.Last.AddVariable();
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.OperationEqState);
        }
        public override void OnLess(char ch)
        {
            var context = this.ContextsStack.Last;
            if (!context.HaveEmptyBuilder()) this.Throw(ch);
            this.ContextsStack.Last.AddVariable();
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.OperationLqState);
        }
        public override void OnGreater(char ch)
        {
            var context = this.ContextsStack.Last;
            if (!context.HaveEmptyBuilder()) this.Throw(ch);
            this.ContextsStack.Last.AddVariable();
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.OperationGqState);
        }
        public override void OnQuestionMark(char ch)
        {
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.ConditionTrueResultState);
            this.ContextsStack.ConsumeLastAsCondition();
            this.ContextsStack.AddNew();
        }
        public override void OnColon(char ch)
        {
            this.Throw(ch);
        }

        public override void OnQuote(char ch)
        {
            this.OnSpace(' ');
            this.StatesStack.ToPrevious();
            base.OnAnyChar(ch);
            this.StatesStack.AddNewState(this.ParserStates.QuoteState);
        }
        public override void OnBackSlash(char ch)
        {
            this.StatesStack.AddNewState(this.ParserStates.EscapedState);
        }
    }
}
