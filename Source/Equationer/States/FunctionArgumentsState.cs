﻿using Equationer.Operations;
using Equationer.Stacks;

namespace Equationer.States
{
    internal class FunctionArgumentsState : ParserState
    {
        public FunctionArgumentsState(string origString, ParserStates states, ParserStatesStack statesStack, ParserContextStack contextsStack) : base(origString, states, statesStack, contextsStack) { }
        public override void OnAnyChar(char ch)
        {
            base.OnAnyChar(ch);
            this.StatesStack.AddNewState(this.ParserStates.FunctionNameOrVariableState);
        }
        public override void OnE(char ch)
        {
            this.OnAnyChar(ch);
        }
        public override void OnStartBracket(char ch)
        {
            this.ContextsStack.AddNew();
            this.StatesStack.AddNewState(this.ParserStates.BracketState);
        }
        public override void OnEndBracket(char ch)
        {
            this.TryAddArgument();
            this.ContextsStack.ConsumeLastAsFunction();
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.FunctionNameOrVariableState);
        }

        public override void OnPlus(char ch)
        {
            this.Throw(ch);
        }
        public override void OnMinus(char ch)
        {
            this.AddMinusVariable(ch);
            this.StatesStack.AddNewState(this.ParserStates.FunctionNameOrVariableState);
        }
        public override void OnMultiply(char ch)
        {
            this.Throw(ch);
        }
        public override void OnDivide(char ch)
        {
            this.Throw(ch);
        }
        public override void OnPower(char ch)
        {
            this.Throw(ch);
        }
        public override void OnOr(char ch)
        {
            this.Throw(ch);
        }
        public override void OnAnd(char ch)
        {
            this.Throw(ch);
        }
        public override void OnExclamation(char ch)
        {
            this.AddNegateVariable(ch);
            this.StatesStack.AddNewState(this.ParserStates.FunctionNameOrVariableState);
        }
        public override void OnEquals(char ch)
        {
            this.Throw(ch);
        }
        public override void OnLess(char ch)
        {
            this.Throw(ch);
        }
        public override void OnGreater(char ch)
        {
            this.Throw(ch);
        }
        public override void OnQuestionMark(char ch)
        {
            this.Throw(ch);
        }
        public override void OnColon(char ch)
        {
            this.Throw(ch);
        }

        public override void OnQuote(char ch)
        {
            this.StatesStack.AddNewState(this.ParserStates.FunctionNameOrVariableState);
            this.StatesStack.Last.OnQuote(ch);
        }

        public override void OnComa(char ch)
        {
            if (!this.TryAddArgument()) this.Throw(ch);
        }

        private bool TryAddArgument()
        {
            var lastContext = this.ContextsStack.Last;
            lastContext.AddVariable();
            var lastValue = lastContext.GetValue();
            lastContext.Clear();
            if (!(lastValue is ValueBase argument)) return false;
            lastContext.PushToVariableList(argument);
            return true;
        }
    }
}
