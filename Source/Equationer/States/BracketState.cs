﻿using Equationer.Stacks;

namespace Equationer.States
{
    internal class BracketState : ParserState
    {
        public BracketState(string origString, ParserStates states, ParserStatesStack statesStack, ParserContextStack contextsStack) : base(origString, states, statesStack, contextsStack) { }
        public override void OnAnyChar(char ch)
        {
            base.OnAnyChar(ch);
            this.StatesStack.AddNewState(this.ParserStates.FunctionNameOrVariableState);
        }
        public override void OnE(char ch)
        {
            if (!this.AddExponent()) this.OnAnyChar(ch);
        }

        public override void OnStartBracket(char ch)
        {
            this.ContextsStack.AddNew();
            this.StatesStack.AddNewState(this.ParserStates.BracketState);
        }
        public override void OnEndBracket(char ch)
        {
            if (this.ContextsStack.Last.IsEmpty()) this.Throw(ch);
            this.ContextsStack.Last.AddVariable();
            this.ContextsStack.ConsumeLastAsBrackets();
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.EndBracketState);
        }

        public override void OnPlus(char ch)
        {
            this.Throw(ch);
        }
        public override void OnMinus(char ch)
        {
            this.AddMinusVariable(ch);
        }
        public override void OnMultiply(char ch)
        {
            this.Throw(ch);
        }
        public override void OnDivide(char ch)
        {
            this.Throw(ch);
        }
        public override void OnPower(char ch)
        {
            this.Throw(ch);
        }
        public override void OnOr(char ch)
        {
            this.Throw(ch);
        }
        public override void OnAnd(char ch)
        {
            this.Throw(ch);
        }
        public override void OnExclamation(char ch)
        {
            this.AddNegateVariable(ch);
        }
        public override void OnEquals(char ch)
        {
            this.Throw(ch);
        }
        public override void OnLess(char ch)
        {
            this.Throw(ch);
        }
        public override void OnGreater(char ch)
        {
            this.Throw(ch);
        }
        public override void OnQuestionMark(char ch)
        {
            this.Throw(ch);
        }
        public override void OnColon(char ch)
        {
            this.Throw(ch);
        }
        public override void OnComa(char ch)
        {
            this.Throw(ch);
        }
    }
}
