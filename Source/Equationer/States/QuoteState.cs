﻿using Equationer.Stacks;

namespace Equationer.States
{
    internal class QuoteState : ParserState
    {
        public QuoteState(string origString, ParserStates states, ParserStatesStack statesStack, ParserContextStack contextsStack) : base(origString, states, statesStack, contextsStack) { }
        public override void OnE(char ch)
        {
            base.OnAnyChar(ch);
        }
        public override void OnComa(char ch)
        {
            base.OnAnyChar(ch);
        }
        public override void OnSpace(char ch)
        {
            base.OnAnyChar(ch);
        }

        public override void OnStartBracket(char ch)
        {
            base.OnAnyChar(ch);
        }
        public override void OnEndBracket(char ch)
        {
            base.OnAnyChar(ch);
        }

        public override void OnPlus(char ch)
        {
            base.OnAnyChar(ch);
        }
        public override void OnMinus(char ch)
        {
            base.OnAnyChar(ch);
        }
        public override void OnMultiply(char ch)
        {
            base.OnAnyChar(ch);
        }
        public override void OnDivide(char ch)
        {
            base.OnAnyChar(ch);
        }
        public override void OnPower(char ch)
        {
            base.OnAnyChar(ch);
        }
        public override void OnOr(char ch)
        {
            base.OnAnyChar(ch);
        }
        public override void OnAnd(char ch)
        {
            base.OnAnyChar(ch);
        }
        public override void OnExclamation(char ch)
        {
            base.OnAnyChar(ch);
        }
        public override void OnEquals(char ch)
        {
            base.OnAnyChar(ch);
        }
        public override void OnLess(char ch)
        {
            base.OnAnyChar(ch);
        }
        public override void OnGreater(char ch)
        {
            base.OnAnyChar(ch);
        }
        public override void OnQuestionMark(char ch)
        {
            base.OnAnyChar(ch);
        }
        public override void OnColon(char ch)
        {
            base.OnAnyChar(ch);
        }

        public override void OnQuote(char ch)
        {
            base.OnAnyChar(ch);
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.OperationExpectState);
        }
    }
}
