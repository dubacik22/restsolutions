﻿using Equationer.Operations;
using Equationer.Stacks;
using System.IO;

namespace Equationer.States
{
    internal abstract class ParserState
    {
        protected string OrigString { get; }
        protected ParserStates ParserStates { get; }
        protected ParserStatesStack StatesStack { get; }
        protected ParserContextStack ContextsStack { get; }
        public ParserState(string origString, ParserStates states, ParserStatesStack statesStack, ParserContextStack contextsStack)
        {
            this.OrigString = origString;
            this.ParserStates = states;
            this.StatesStack = statesStack;
            this.ContextsStack = contextsStack;
        }

        public virtual void OnAnyChar(char ch)
        {
            this.ContextsStack.Last.AddCharToBuilder(ch);
        }
        public abstract void OnE(char ch);
        public virtual void OnSpace(char ch) { }

        public abstract void OnComa(char ch);

        public abstract void OnStartBracket(char ch);
        public abstract void OnEndBracket(char ch);

        public abstract void OnPlus(char ch);
        public abstract void OnMinus(char ch);
        public abstract void OnMultiply(char ch);
        public abstract void OnDivide(char ch);
        public abstract void OnPower(char ch);
        public abstract void OnOr(char ch);
        public abstract void OnAnd(char ch);
        public abstract void OnExclamation(char ch);
        public abstract void OnEquals(char ch);
        public abstract void OnLess(char ch);
        public abstract void OnGreater(char ch);
        public abstract void OnQuestionMark(char ch);
        public abstract void OnColon(char ch);

        public virtual void OnQuote(char ch)
        {
            //this.Contexts.Last.AddCharToBuilder(ch);
            this.OnAnyChar(ch);
            this.StatesStack.AddNewState(this.ParserStates.QuoteState);
        }
        public virtual void OnBackSlash(char ch)
        {
            this.StatesStack.AddNewState(this.ParserStates.EscapedState);
        }

        protected void Throw(char ch)
        {
            throw new InvalidDataException("unexpected '" + ch + "' in " + this.OrigString);
        }
        protected void AddMinusVariable(char ch)
        {
            var context = this.ContextsStack.Last;
            if (!context.HaveEmptyBuilder()) return;
            context.AddStringToBuilder("-1");
            context.AddVariable();
            context.AddToEquation(EOperand.PMultiply.ToOperation());
        }
        protected void AddNegateVariable(char ch)
        {
            var context = this.ContextsStack.Last;
            if (!context.HaveEmptyBuilder()) return;
            context.AddStringToBuilder("null");
            context.AddVariable();
            context.AddToEquation(EOperand.Negate.ToOperation());
        }
        protected bool AddExponent()
        {
            if (!this.ContextsStack.Last.CanCreateConstant()) return false;
            this.ConsumeValue();
            this.ContextsStack.Last.AddToEquation(EOperand.Multiply.ToOperation());
            this.ContextsStack.Last.AddStringToBuilder("10");
            this.ConsumeValue();
            this.ContextsStack.Last.AddToEquation(EOperand.PPower.ToOperation());
            this.StatesStack.AddNewState(this.ParserStates.AfterOperationState);
            return true;
        }
        protected void ConsumeValue()
        {
            this.ContextsStack.Last.AddVariable();
        }
        private void AfterOperation()
        {
            this.StatesStack.AddNewState(this.ParserStates.AfterOperationState);
        }
        protected void NormalOnPlus(char ch)
        {
            this.ConsumeValue();
            this.ContextsStack.Last.AddToEquation(EOperand.Plus.ToOperation());
            this.AfterOperation();
        }
        protected void NormalOnMinus(char ch)
        {
            this.ConsumeValue();
            this.ContextsStack.Last.AddToEquation(EOperand.Minus.ToOperation());
            this.AfterOperation();
        }
        protected void NormalOnMultiply(char ch)
        {
            this.ConsumeValue();
            this.ContextsStack.Last.AddToEquation(EOperand.Multiply.ToOperation());
            this.AfterOperation();
        }
        protected void NormalOnDivide(char ch)
        {
            this.ConsumeValue();
            this.ContextsStack.Last.AddToEquation(EOperand.Divide.ToOperation());
            this.AfterOperation();
        }
        protected void NormalOnPower(char ch)
        {
            this.ConsumeValue();
            this.ContextsStack.Last.AddToEquation(EOperand.Power.ToOperation());
            this.AfterOperation();
        }
        protected void NormalOnOr(char ch)
        {
            this.ConsumeValue();
            this.StatesStack.AddNewState(this.ParserStates.OperationOrState);
        }
        protected void NormalOnAnd(char ch)
        {
            this.ConsumeValue();
            this.StatesStack.AddNewState(this.ParserStates.OperationAndState);
        }
        protected void NormalOnEquals(char ch)
        {
            this.ConsumeValue();
            this.StatesStack.AddNewState(this.ParserStates.OperationEqState);
        }
        protected void NormalOnLess(char ch)
        {
            this.ConsumeValue();
            this.StatesStack.AddNewState(this.ParserStates.OperationLqState);
        }
        protected void NormalOnGreater(char ch)
        {
            this.ConsumeValue();
            this.StatesStack.AddNewState(this.ParserStates.OperationGqState);
        }
        protected void NormalOnQuestionMark(char ch)
        {
            this.ContextsStack.ConsumeLastAsCondition();
            this.ContextsStack.AddNew();
            this.StatesStack.ToPrevious();
            this.StatesStack.AddNewState(this.ParserStates.ConditionTrueResultState);
        }
    }
}
