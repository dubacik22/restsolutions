kniznica na parsovanie vzorcov.

vracia ICalculatable ktore ma GetCalculatedValue() na zyskanie vysledku

ak sa vo vzorci nachadza premenna, tak ju parser ziska pomocou fetchera "FetchVariable". tento fetcher musi vratit triedu IGetVariable ktora musi mat get propertu Variable typu object, ale je vhodne ak vracia double, bool alebo string.
ak sa vo vzorci nachadza funkcia, tak ju parser ziska pomocou fetchera "FetchFunction". tento fetcher musi vratit triedu IFunction ktora musi mat metodu double Calculate(IEnumerable<double> arguments)
