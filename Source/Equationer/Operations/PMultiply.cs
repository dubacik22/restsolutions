﻿namespace Equationer.Operations
{
    internal class PMultiply : Multiply
    {
        public override string Type { get => "PMultiply"; }
        public override EOperand Operation { get => EOperand.PMultiply; }
        public override IEquationPart CloneJustMe()
        {
            return new PMultiply();
        }
        public override string ToStringJustMe()
        {
            return " !* ";
        }
    }
}
