﻿namespace Equationer.Operations
{
    internal interface IOperation : IEquationPart
    {
        EOperand Operation { get; }
        Constant Calculate();
    }
}
