﻿namespace Equationer.Operations
{
    internal class Or : OperationBase
    {
        public override string Type { get => "Or"; }
        public override EOperand Operation { get => EOperand.Or; }
        public override Constant Calculate()
        {
            var left = this.ToBool(this.ValueLeft);
            var right = this.ToBool(this.ValueRight);
            if (!left.HasValue || !right.HasValue) return new Constant(null);
            return new Constant((left == true) || (right == true));
        }
        public override IEquationPart CloneJustMe()
        {
            return new Or();
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return " || ";
        }
    }
}
