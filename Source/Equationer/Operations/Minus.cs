﻿namespace Equationer.Operations
{
    internal class Minus : OperationBase
    {
        public override string Type { get => "Minus"; }
        public override EOperand Operation { get => EOperand.Minus; }
        public override Constant Calculate()
        {
            var left = this.ToDouble(this.ValueLeft);
            var right = this.ToDouble(this.ValueRight);
            return new Constant(left - right);
        }
        public override IEquationPart CloneJustMe()
        {
            return new Minus();
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return " - ";
        }
    }
}
