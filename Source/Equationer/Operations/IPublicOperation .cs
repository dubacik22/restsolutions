﻿namespace Equationer.Operations
{
    public interface IPublicOperation
    {
        EOperand Operation { get; }
    }
}
