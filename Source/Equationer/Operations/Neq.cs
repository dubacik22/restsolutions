﻿namespace Equationer.Operations
{
    internal class Neq : OperationBase
    {
        public override string Type { get => "Neq"; }
        public override EOperand Operation { get => EOperand.Neq; }
        public override Constant Calculate()
        {
            var left = this.ValueLeft.Value;
            var right = this.ValueRight.Value;
            return new Constant(!object.Equals(left, right));
        }
        public override IEquationPart CloneJustMe()
        {
            return new Neq();
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return " != ";
        }
    }
}
