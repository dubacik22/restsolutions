﻿namespace Equationer.Operations
{
    internal class Bracket : ValueBase
    {
        public override string Type { get => "Bracket"; }

        private readonly ValueBase inner;
        public override object Value
        {
            get => this.inner.GetCalculatedValue();
        }
        public Bracket(ValueBase inner)
        {
            this.inner = inner;
        }
        public override IEquationPart CloneJustMe()
        {
            return new Bracket(this.inner.Clone());
        }

        public override string ToString()
        {
            return this.ToStringJustMe() + " " + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return "(" + this.inner.ToString() + ")";
        }
    }
}
