﻿namespace Equationer.Operations
{
    public interface IPublicVariable
    {
        IGetVariable MyVariable { get; set; }
    }
}
