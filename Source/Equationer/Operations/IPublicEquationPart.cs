﻿namespace Equationer.Operations
{
    public interface IPublicEquationPart
    {
        string Type { get; }
    }
}
