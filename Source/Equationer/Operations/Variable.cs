﻿namespace Equationer.Operations
{
    internal class Variable : ValueBase, IPublicVariable
    {
        public string Name;
        public override string Type { get => "Variable"; }

        public IGetVariable MyVariable { get; set; }
        public override object Value
        {
            get => this.MyVariable.Variable;
        }

        public Variable(IGetVariable varianle)
        {
            this.MyVariable = varianle;
        }

        public override IEquationPart CloneJustMe()
        {
            return new Variable(this.MyVariable)
            {
                Name = this.Name,
            };
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return "$" + this.Name;
        }
    }
}
