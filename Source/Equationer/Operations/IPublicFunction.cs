﻿namespace Equationer.Operations
{
    public interface IPublicFunction
    {
        string Name { get; set; }
        IFunction MyFunction { get; set; }
    }
}
