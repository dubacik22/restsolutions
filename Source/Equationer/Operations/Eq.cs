﻿using DisplayValue;
using System.Globalization;

namespace Equationer.Operations
{
    internal class Eq : OperationBase
    {
        public override string Type { get => "Eq"; }
        public override EOperand Operation { get => EOperand.Eq; }
        public override Constant Calculate()
        {
            var left = this.ValueLeft.Value;
            var right = this.ValueRight.Value;
            if (object.Equals(left, right)) return new Constant(true);
            if (left == right) return new Constant(true);
            if (Eq.EqualsObjects(left, right)) return new Constant(true);
            return new Constant(false);
        }
        public override IEquationPart CloneJustMe()
        {
            return new Eq();
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return " == ";
        }

        private static bool EqualsObjects(object o1, object o2)
        {
            var o1d = Eq.ToNullableDouble(o1);
            var o2d = Eq.ToNullableDouble(o2);
            if (o1d is object && o2d is object) return o1d.Value == o2d.Value;
            if (o1 is string s1 && o2 is string s2) return s1 == s2;
            return false;

        }
        public static double? ToNullableDouble(object value)
        {
            if (value is DispValue dispValue) return dispValue.Value;
            if (value is double doubleValue) return doubleValue;
            if (value is int intValue) return intValue;
            return null;
        }
    }
}
