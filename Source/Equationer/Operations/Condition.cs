﻿namespace Equationer.Operations
{
    internal class Condition : ValueBase
    {
        public override string Type { get => "Condition"; }

        public ValueBase Cond { get; set; }
        public ValueBase TrueValue { get; set; }
        public ValueBase FalseValue { get; set; }
        public override object Value
        {
            get
            {
                if (!(this.Cond.GetCalculatedValue() is bool condResult)) return double.NaN;
                if (condResult) return this.TrueValue.GetCalculatedValue();
                return this.FalseValue.GetCalculatedValue();
            }
        }

        public Condition(ValueBase cond)
        {
            this.Cond = cond;
        }

        public override IEquationPart CloneJustMe()
        {
            return new Condition(this.Cond.Clone())
            {
                TrueValue = this.TrueValue.Clone(),
                FalseValue = this.FalseValue.Clone(),
            };
        }
        public override object GetCalculatedValue()
        {
            return this.Value;
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return this.Cond.ToString() + "? " + this.TrueValue.ToString() + " : " + this.FalseValue.ToString();
        }
    }
}
