﻿using System.Runtime.CompilerServices;

namespace Equationer.Operations
{
    internal class Constant : ValueBase, IPublicConstant
    {
        public override string Type { get => "Constant"; }

        private readonly object value;
        public override object Value
        {
            get => this.value;
        }
        public Constant(object value)
        {
            this.value = value;
        }
        public override IEquationPart CloneJustMe()
        {
            return new Constant(this.value);
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            if (this.value is string) return $"'{this.value}'";
            if (this.value is object) return this.value?.ToString();
            if (this.Right is Negate) return string.Empty;
            return "null";
        }
    }
}
