﻿namespace Equationer.Operations
{
    internal interface IValue : IEquationPart
    {
        object Value { get; }
    }
}
