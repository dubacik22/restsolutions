﻿using System.Linq;

namespace Equationer.Operations
{
    internal static class ValueBaseCalculator
    {
        internal static object GetCalculatedValue(ValueBase valueBase)
        {
            var retIValue = ValueBaseCalculator.GetCalculatedIValue(valueBase);
            if (retIValue is null) return double.NaN;
            return retIValue.Value;
        }
        internal static IValue GetCalculatedIValue(ValueBase valueBase)
        {
            if (!(ValueBaseCalculator.GetFirst(valueBase) is ValueBase first)) return null;
            IEquationPart eqRet = ValueBaseCalculator.Clone(first);

            eqRet = ValueBaseCalculator.CalculateOperations(eqRet, EOperand.PMultiply)?.MostLeft();
            if (eqRet is object && eqRet.Left is null && eqRet.Right is null) return (IValue)eqRet;
            eqRet = ValueBaseCalculator.CalculateOperations(eqRet, EOperand.PPower)?.MostLeft();
            if (eqRet is object && eqRet.Left is null && eqRet.Right is null) return (IValue)eqRet;

            eqRet = ValueBaseCalculator.CalculateOperations(eqRet, EOperand.Power)?.MostLeft();
            if (eqRet is object && eqRet.Left is null && eqRet.Right is null) return (IValue)eqRet;
            eqRet = ValueBaseCalculator.CalculateOperations(eqRet, EOperand.Multiply, EOperand.Divide)?.MostLeft();
            if (eqRet is object && eqRet.Left is null && eqRet.Right is null) return (IValue)eqRet;
            eqRet = ValueBaseCalculator.CalculateOperations(eqRet, EOperand.Plus, EOperand.Minus)?.MostLeft();
            if (eqRet is object && eqRet.Left is null && eqRet.Right is null) return (IValue)eqRet;

            eqRet = ValueBaseCalculator.CalculateOperations(eqRet, EOperand.Gt, EOperand.Lt, EOperand.Geq, EOperand.Leq)?.MostLeft();
            if (eqRet is object && eqRet.Left is null && eqRet.Right is null) return (IValue)eqRet;

            eqRet = ValueBaseCalculator.CalculateOperations(eqRet, EOperand.Negate)?.MostLeft();
            if (eqRet is object && eqRet.Left is null && eqRet.Right is null) return (IValue)eqRet;
            eqRet = ValueBaseCalculator.CalculateOperations(eqRet, EOperand.Or, EOperand.And, EOperand.Neq, EOperand.Eq)?.MostLeft();
            if (eqRet is object && eqRet.Left is null && eqRet.Right is null) return (IValue)eqRet;

            return null;
        }
        private static IEquationPart GetFirst(ValueBase valueBase)
        {
            var thisPart = valueBase as IEquationPart;
            while (true)
            {
                if (thisPart.Left is null) return thisPart;
                thisPart = thisPart.Left;
            }
        }
        internal static ValueBase Clone(this ValueBase valueBase)
        {
            var clone = valueBase.CloneJustMe() as ValueBase;
            clone.Left = ValueBaseCalculator.CloneLeft(valueBase);
            if (clone.Left is object) clone.Left.Right = clone;
            clone.Right = ValueBaseCalculator.CloneRight(valueBase);
            if (clone.Right is object) clone.Right.Left = clone;
            return clone;
        }
        private static IEquationPart CalculateOperations(IEquationPart eqPart, params EOperand[] calculatingOperands)
        {
            while (true)
            {
                if (eqPart is IOperation operation && calculatingOperands.Contains(operation.Operation))
                    eqPart = ValueBaseCalculator.CalculateOperation(operation);
                if (eqPart.Right is null) return eqPart;
                eqPart = eqPart.Right;
            }
        }
        private static IEquationPart CalculateOperation(IOperation operation)
        {
            var result = operation.Calculate();
            if (operation.Left?.Left?.Right is object)
            {
                operation.Left.Left.Right = result;
                result.Left = operation.Left.Left;
            }
            if (operation.Right?.Right?.Left is object)
            {
                operation.Right.Right.Left = result;
                result.Right = operation.Right.Right;
            }
            ValueBaseCalculator.BreakImmediateConnections(operation);
            return result;
        }
        private static void BreakImmediateConnections(IOperation part)
        {
            if (part.Left is object)
            {
                part.Left.Left = null;
                part.Left.Right = null;
            }
            if (part.Right is object)
            {
                part.Right.Right = null;
                part.Right.Left = null;
            }
            part.Left = null;
            part.Right = null;
        }
        private static IEquationPart CloneLeft(IValue valueBase)
        {
            var next = valueBase.Left;
            if (next is null) return null;
            var firstClone = next?.CloneJustMe();
            var clone = firstClone;
            while (true)
            {
                next = next.Left;
                if (next is null) return firstClone;
                var nextClone = next?.CloneJustMe();
                nextClone.Right = clone;
                clone.Left = nextClone;
                clone = nextClone;
            }
        }
        private static IEquationPart CloneRight(IValue valueBase)
        {
            var next = valueBase.Right;
            if (next is null) return null;
            var firstClone = next?.CloneJustMe();
            var clone = firstClone;
            while (true)
            {
                next = next.Right;
                if (next is null) return firstClone;
                var nextClone = next?.CloneJustMe();
                nextClone.Left = clone;
                clone.Right = nextClone;
                clone = nextClone;
            }
        }
    }
}
