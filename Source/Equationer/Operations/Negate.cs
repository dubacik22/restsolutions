﻿namespace Equationer.Operations
{
    internal class Negate : OperationBase
    {
        public override string Type { get => "!"; }
        public override EOperand Operation { get => EOperand.Negate; }
        public override Constant Calculate()
        {
            var right = this.ToBool(this.ValueRight);
            if (!right.HasValue) return new Constant(null);
            return new Constant(!(right == true));
        }
        public override IEquationPart CloneJustMe()
        {
            return new Negate();
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return "!";
        }
    }
}
