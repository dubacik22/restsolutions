﻿namespace Equationer.Operations
{
    internal class And : OperationBase
    {
        public override string Type { get => "And"; }
        public override EOperand Operation { get => EOperand.And; }
        public override Constant Calculate()
        {
            var left = this.ToBool(this.ValueLeft);
            var right = this.ToBool(this.ValueRight);
            if (!left.HasValue || !right.HasValue) return new Constant(null);
            return new Constant((left == true) && (right == true));
        }
        public override IEquationPart CloneJustMe()
        {
            return new And();
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return " && ";
        }
    }
}
