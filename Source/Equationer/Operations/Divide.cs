﻿namespace Equationer.Operations
{
    internal class Divide : OperationBase
    {
        public override string Type { get => "Divide"; }
        public override EOperand Operation { get => EOperand.Divide; }
        public override Constant Calculate()
        {
            var left = this.ToDouble(this.ValueLeft);
            var right = this.ToDouble(this.ValueRight);
            return new Constant(left / right);
        }
        public override IEquationPart CloneJustMe()
        {
            return new Divide();
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return " / ";
        }
    }
}
