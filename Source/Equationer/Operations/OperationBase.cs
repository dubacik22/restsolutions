﻿using DisplayValue;
using System;

namespace Equationer.Operations
{
    internal abstract class OperationBase : IOperation, IPublicOperation
    {
        public abstract string Type { get; }
        public abstract EOperand Operation { get; }
        public IValue ValueLeft { get; private set; }
        public IEquationPart Left
        {
            get => this.ValueLeft;
            set => this.ValueLeft = value as IValue;
        }
        public IValue ValueRight { get; private set; }
        public IEquationPart Right
        {
            get => this.ValueRight;
            set => this.ValueRight = value as IValue;
        }

        public abstract Constant Calculate();
        public abstract IEquationPart CloneJustMe();
        public abstract string ToStringJustMe();
        public IEquationPart MostLeft()
        {
            if (this.Left is null) return this;
            return this.Left.MostLeft();
        }
        public IEquationPart MostRight()
        {
            if (this.Right is null) return this;
            return this.Right.MostRight();
        }
        protected bool IsString(IValue value)
        {
            var retObj = value?.Value;
            return retObj is string;
        }
        protected string ToString(IValue value)
        {
            var retObj = value?.Value;
            if (retObj is string retString) return retString;
            return null;
        }
        protected double ToDouble(IValue value)
        {
            return this.ToDouble(value?.Value);
        }
        protected double ToDouble(object value)
        {
            if (value is double retDouble) return retDouble;
            if (value is DispValue retDispValue) return retDispValue.Value;
            if (value is int retInt) return retInt;
            return double.NaN;
        }
        protected bool? ToBool(IValue value)
        {
            return this.ToBool(value?.Value);
        }
        protected bool? ToBool(object value)
        {
            if (value is bool retBool) return retBool;
            if (value is Boolean retBoolean) return retBoolean;
            return null;
        }
    }
}
