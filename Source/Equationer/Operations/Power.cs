﻿using System;

namespace Equationer.Operations
{
    internal class Power : OperationBase
    {
        public override string Type { get => "Power"; }
        public override EOperand Operation { get => EOperand.Power; }
        public override Constant Calculate()
        {
            var left = this.ToDouble(this.ValueLeft);
            var right = this.ToDouble(this.ValueRight);
            return new Constant(Math.Pow(left, right));
        }
        public override IEquationPart CloneJustMe()
        {
            return new Power();
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return " ^";
        }
    }
}
