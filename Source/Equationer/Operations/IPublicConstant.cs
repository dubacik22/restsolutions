﻿namespace Equationer.Operations
{
    public interface IPublicConstant
    {
        object Value { get; }
    }
}
