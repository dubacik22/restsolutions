﻿using System.Collections.Generic;

namespace Equationer.Operations
{
    internal abstract class ValueBase : IValue, ICalculatable
    {
        public abstract string Type { get; }
        public IEquationPart Left { get; set; }
        public IEquationPart Right { get; set; }
        public abstract object Value { get; }

        public abstract IEquationPart CloneJustMe();

        public virtual object GetCalculatedValue()
        {
            return ValueBaseCalculator.GetCalculatedValue(this);
        }
        public abstract string ToStringJustMe();
        public IEquationPart MostLeft()
        {
            if (this.Left is null) return this;
            return this.Left.MostLeft();
        }
        public IEquationPart MostRight()
        {
            if (this.Right is null) return this;
            return this.Right.MostRight();
        }

        public IEnumerable<IPublicEquationPart> GetInner()
        {
            IEquationPart ret = this;
            while (ret is object)
            {
                yield return ret;
                ret = ret.Right;
            }
            yield break;
        }
    }
}
