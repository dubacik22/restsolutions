﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Equationer.Operations
{
    internal class Function : ValueBase, IPublicFunction
    {
        public override string Type { get => "Function"; }

        public string Name { get; set; }
        public IFunction MyFunction { get; set; }
        public IEnumerable<ValueBase> Arguments { get; set; }
        public override object Value
        {
            get => this.MyFunction.Calculate(this.Arguments?.Select(value => value.GetCalculatedValue()));
        }

        public Function(IFunction myFunction)
        {
            this.MyFunction = myFunction;
        }
        public override IEquationPart CloneJustMe()
        {
            var cloneArguments = new List<ValueBase>();
            foreach (var argument in this.Arguments) cloneArguments.Add(argument.Clone());
            return new Function(this.MyFunction)
            {
                Arguments = cloneArguments,
                Name = this.Name,
            };
        }

        public override string ToString()
        {
            return this.ToStringJustMe() + " " + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            var argumentsSb = this.Arguments?.Aggregate(new StringBuilder(), (sb, argument) =>
            {
                if (sb.Length > 0) sb.Append(", ");
                sb.Append(argument?.ToString());
                return sb;
            });

            return this.Name + "(" + argumentsSb.ToString() + ")";
        }
    }
}
