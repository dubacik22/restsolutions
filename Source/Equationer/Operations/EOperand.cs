namespace Equationer.Operations
{
    public enum EOperand
    {
        Plus,
        Minus,
        Multiply,
        PMultiply,
        Divide,
        Power,
        PPower,
        Negate,
        Or,
        And,
        Lt,
        Gt,
        Eq,
        Leq,
        Geq,
        Neq
    }
    public static class OperandExtension
    {
        internal static EOperand? ToEOperand(this string value)
        {
            switch (value)
            {
                case "+": return EOperand.Plus;
                case "-": return EOperand.Minus;
                case "*": return EOperand.Multiply;
                case "!*": return EOperand.PMultiply;
                case "/": return EOperand.Divide;
                case "^": return EOperand.Power;
                case "!^": return EOperand.PPower;
                case "!": return EOperand.Negate;
                case "||": return EOperand.Or;
                case "&&": return EOperand.And;
                case "<": return EOperand.Lt;
                case ">": return EOperand.Gt;
                case "==": return EOperand.Eq;
                case "<=": return EOperand.Leq;
                case ">=": return EOperand.Geq;
                case "!=": return EOperand.Neq;
                default: return null;
            }
        }
        internal static IOperation ToOperation(this EOperand operand)
        {
            switch (operand)
            {
                case EOperand.Plus: return new Plus();
                case EOperand.Minus: return new Minus();
                case EOperand.Multiply: return new Multiply();
                case EOperand.PMultiply: return new PMultiply();
                case EOperand.Divide: return new Divide();
                case EOperand.Power: return new Power();
                case EOperand.PPower: return new PPower();
                case EOperand.Negate: return new Negate();
                case EOperand.Or: return new Or();
                case EOperand.And: return new And();
                case EOperand.Lt: return new Lq();
                case EOperand.Gt: return new Gq();
                case EOperand.Eq: return new Eq();
                case EOperand.Leq: return new Leq();
                case EOperand.Geq: return new Geq();
                case EOperand.Neq: return new Neq();
                default: return null;
            }
        }
        internal static string ToStringRepresentation(this EOperand operand)
        {
            switch (operand)
            {
                case EOperand.Plus: return "+";
                case EOperand.Minus: return "-";
                case EOperand.Multiply: return "*";
                case EOperand.PMultiply: return "!*";
                case EOperand.Divide: return "/";
                case EOperand.Power: return "^";
                case EOperand.PPower: return "!^";
                case EOperand.Negate: return "!";
                case EOperand.Or: return "||";
                case EOperand.And: return "&&";
                case EOperand.Lt: return "<";
                case EOperand.Gt: return ">";
                case EOperand.Eq: return "==";
                case EOperand.Leq: return "<=";
                case EOperand.Geq: return ">=";
                case EOperand.Neq: return "!=";
                default: return null;
            }
        }
        public static bool IsOneOfOperand(string operand)
        {
            switch (operand)
            {
                case "+": return true;
                case "-": return true;
                case "*": return true;
                case "!*": return true;
                case "/": return true;
                case "^": return true;
                case "!^": return true;
                case "!": return true;
                case "||": return true;
                case "&&": return true;
                case "<": return true;
                case ">": return true;
                case "==": return true;
                case "<=": return true;
                case ">=": return true;
                case "!=": return true;
                default: return false;
            }
        }
    }
}
