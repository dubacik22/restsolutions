﻿namespace Equationer.Operations
{
    internal class Multiply : OperationBase
    {
        public override string Type { get => "Multiply"; }
        public override EOperand Operation { get => EOperand.Multiply; }
        public override Constant Calculate()
        {
            var left = this.ToDouble(this.ValueLeft);
            var right = this.ToDouble(this.ValueRight);
            return new Constant(left * right);
        }
        public override IEquationPart CloneJustMe()
        {
            return new Multiply();
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return " * ";
        }
    }
}
