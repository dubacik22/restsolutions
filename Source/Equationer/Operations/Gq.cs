﻿namespace Equationer.Operations
{
    internal class Gq : OperationBase
    {
        public override string Type { get => "Gq"; }
        public override EOperand Operation { get => EOperand.Gt; }
        public override Constant Calculate()
        {
            var left = this.ToDouble(this.ValueLeft);
            var right = this.ToDouble(this.ValueRight);
            return new Constant(left > right);
        }
        public override IEquationPart CloneJustMe()
        {
            return new Gq();
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return " > ";
        }
    }
}
