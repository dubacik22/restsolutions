﻿namespace Equationer.Operations
{
    internal class Geq : OperationBase
    {
        public override string Type { get => "Geq"; }
        public override EOperand Operation { get => EOperand.Geq; }
        public override Constant Calculate()
        {
            var left = this.ToDouble(this.ValueLeft);
            var right = this.ToDouble(this.ValueRight);
            return new Constant(left >= right);
        }
        public override IEquationPart CloneJustMe()
        {
            return new Geq();
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return " >= ";
        }
    }
}
