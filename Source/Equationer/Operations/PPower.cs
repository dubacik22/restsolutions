﻿namespace Equationer.Operations
{
    internal class PPower : Power
    {
        public override string Type { get => "PPower"; }
        public override EOperand Operation { get => EOperand.PPower; }
        public override IEquationPart CloneJustMe()
        {
            return new PPower();
        }
        public override string ToStringJustMe()
        {
            return " !^";
        }
    }
}
