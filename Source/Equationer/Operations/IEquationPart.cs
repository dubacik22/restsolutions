﻿namespace Equationer.Operations
{
    internal interface IEquationPart : IPublicEquationPart
    {
        IEquationPart Left { get; set; }
        IEquationPart Right { get; set; }
        IEquationPart CloneJustMe();
        IEquationPart MostLeft();
        IEquationPart MostRight();
        string ToStringJustMe();
    }
}
