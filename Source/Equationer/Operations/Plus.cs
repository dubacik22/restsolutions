﻿namespace Equationer.Operations
{
    internal class Plus : OperationBase
    {
        public override string Type { get => "Plus"; }
        public override EOperand Operation { get => EOperand.Plus; }
        public override Constant Calculate()
        {
            var left = this.ValueLeft?.Value;
            var right = this.ValueRight?.Value;
            if (left is string || right is string) return new Constant(left?.ToString() + right?.ToString());
            var leftDouble = this.ToDouble(left);
            var rightDouble = this.ToDouble(right);
            return new Constant(leftDouble + rightDouble);
        }
        public override IEquationPart CloneJustMe()
        {
            return new Plus();
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return " + ";
        }
    }
}
