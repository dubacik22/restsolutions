﻿namespace Equationer.Operations
{
    internal class Lq : OperationBase
    {
        public override string Type { get => "Lq"; }
        public override EOperand Operation { get => EOperand.Lt; }
        public override Constant Calculate()
        {
            var left = this.ToDouble(this.ValueLeft);
            var right = this.ToDouble(this.ValueRight);
            return new Constant(left < right);
        }
        public override IEquationPart CloneJustMe()
        {
            return new Lq();
        }
        public override string ToString()
        {
            return this.ToStringJustMe() + this.Right?.ToString();
        }
        public override string ToStringJustMe()
        {
            return " < ";
        }
    }
}
