﻿using Equationer.Operations;
using System.Collections.Generic;

namespace Equationer
{
    public interface ICalculatable
    {
        object GetCalculatedValue();
        IEnumerable<IPublicEquationPart> GetInner();
    }
}
