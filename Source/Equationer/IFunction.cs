﻿using System.Collections.Generic;

namespace Equationer
{
    public interface IFunction
    {
        object Calculate(IEnumerable<object> arguments);
    }
}
