formula parsing library.

this library can parse string equations and recognize user functions and user variables like:
func1(null, 1, var0, func2(var1, some.other.var2)+'str', -60)
where va0, var1, some.other.var2 are recognized as variables
func1, func2 are recognized as functions
and 'str' is recognized as string constant. also -60 as double constant

returns ICalculatable which has GetCalculatedValue() to get the result

if there is a variable in the formula, then the parser gets it using the "FetchVariable" fetcher. this fetcher must return the IGetVariable class, which must have the property Variable of type object, but it is useful if it returns a double, bool or string.
if there is a function in the formula, then the parser gets it using the "FetchFunction" fetcher. this fetcher must return an IFunction class which must have a double Calculate(IEnumerable<double> arguments)

when string "(4>5)==false" is parsed, and GetCalculatedValue() of resulting ICalculatable is called, we gets true as result.
the GetCalculatedValue() of Calculatable calls Variable getter for each IGetVariable and Calculate method for each IFunction class and returns resulting object.


    public class DummyValue : IGetVariable
    {
        public object Variable { get => double.NaN; }
    }
    public class DummyFunc : IFunction
    {
        public object Calculate(IEnumerable<object> arguments)
        {
            return arguments.First();
        }
    }
    public class Test()
    {
        var parser = new EquationParser
        {
            FetchFunction = name => new DummyFunc(),
            FetchVariable = name => new DummyValue(),
        }
        parser.Parse("(v) ? 1 : (v > 1) ? 's' : f(v, 2)");
        parser.Parse("(4>5)==false");
        parser.Parse("(true) and ('qw' == 'ivan')");
        parser.Parse("(true) ? (4 gteq 4) ? 1 : 2 : 3");
        parser.Parse("func1(null, 1, #var0, func2(var1, some.other.var2)+'_a', -60)");
    }
