﻿namespace Equationer
{
    public interface IGetVariable
    {
        object Variable { get; }
    }
}
