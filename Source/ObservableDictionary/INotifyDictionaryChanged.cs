﻿namespace ObservableDictionary
{
    public interface INotifyDictionaryChanged
    {
        event NotifyDictionaryChangedEventHandler DictionaryChanged;
    }
}
