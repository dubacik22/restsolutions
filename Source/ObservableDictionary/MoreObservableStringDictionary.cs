using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ObservableDictionary
{
    public class MoreObservableStringDictionary<TValue> : IObservableMap<TValue>, INotifyDictionaryChanged, INotifyPropertyChanged
    {
        #region fields
        private const string CountString = "Count";

        // This must agree with Binding.IndexerName.  It is declared separately
        // here so as to avoid a dependency on PresentationFramework.dll.
        private const string IndexerNameBeg = "Item[";
        private const string IndexerNameEnd = "]";

        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add => this.PropertyChanged += value;
            remove => this.PropertyChanged -= value;
        }
#if !FEATURE_NETCORE
        [field: NonSerializedAttribute()]
#endif
        public virtual event NotifyDictionaryChangedEventHandler DictionaryChanged;

#if !FEATURE_NETCORE
        [field: NonSerializedAttribute()]
#endif
        protected virtual event PropertyChangedEventHandler PropertyChanged;

        private readonly SimpleMonitor _monitor = new SimpleMonitor();
        private readonly Dictionary<int, List<KeyValuePair<string, TValue>>> InternalList = new Dictionary<int, List<KeyValuePair<string, TValue>>>();

        #endregion
        public int Count
        {
            get
            {
                var ret = 0;
                foreach (var list in this.InternalList)
                {
                    ret += list.Value.Count;
                }
                return ret;
            }
        }
        public TValue this[string key]
        {
            get => this.Get(key);
            set => this.Set(key, value);
        }
        public void Add(string key, TValue value)
        {
            var coords = this.GetIndex(key);
            if (coords is null || (coords.listIndex >= 0)) throw new KeyNotFoundException("key: " + key + " is already in the dictionary");
            var index = coords.listIndex;
            if (index < 0) index = -coords.listIndex - 1;
            coords.list.Insert(index, new KeyValuePair<string, TValue>(key, value));

            this.OnPropertyChanged(CountString);
            this.OnPropertyChanged(IndexerNameBeg + IndexerNameEnd);
            this.OnPropertyChanged(IndexerNameBeg + key + IndexerNameEnd);
            this.OnCollectionChanged(NotifyCollectionChangedAction.Add, key, value);
        }
        public bool ContainsKey(string key)
        {
            var coords = this.GetIndex(key);
            if (coords is null || (coords.listIndex < 0)) return false;
            return true;
        }
        public void Remove(string key)
        {
            var coords = this.GetIndex(key);
            if (coords is null) return;
            if (coords.listIndex >= 0)
            {
                var removedItem = coords.list[coords.listIndex];
                coords.list.RemoveAt(coords.listIndex);
                this.OnPropertyChanged(CountString);
                this.OnPropertyChanged(IndexerNameBeg + IndexerNameEnd);
                this.OnPropertyChanged(IndexerNameBeg + key + IndexerNameEnd);
                this.OnCollectionChanged(NotifyCollectionChangedAction.Remove, key, removedItem.Value);
            }
            if (coords.list.Count <= 0) this.InternalList.Remove(coords.dictionaryKey);
        }

        public void Clear()
        {
            this.InternalList.Clear();
            this.OnPropertyChanged(CountString);
            this.OnPropertyChanged(IndexerNameBeg + IndexerNameEnd);
            this.OnCollectionReset();
        }
        public IEnumerator<KeyValuePair<string, TValue>> GetEnumerator()
        {
            foreach (var kvp in this.InternalList)
            {
                foreach (var item in kvp.Value)
                {
                    yield return item;
                }
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (var kvp in this.InternalList)
            {
                foreach (var item in kvp.Value)
                {
                    yield return item;
                }
            }
        }

        #region private
        private TValue Get(string key)
        {
            var coords = this.GetIndex(key);
            if (coords is null || (coords.listIndex < 0)) return default;
            //throw new KeyNotFoundException("key: " + key + " is not in the dictionary");
            return coords.list[coords.listIndex].Value;
        }
        private void Set(string key, TValue value)
        {
            var coords = this.GetIndex(key);
            if (coords is null || (coords.listIndex < 0)) throw new KeyNotFoundException("key: " + key + " is not in the dictionary");
            coords.list[coords.listIndex] = new KeyValuePair<string, TValue>(key, value);

            this.OnPropertyChanged(CountString);
            this.OnPropertyChanged(IndexerNameBeg + IndexerNameEnd);
            this.OnPropertyChanged(IndexerNameBeg + key + IndexerNameEnd);
            this.OnCollectionChanged(NotifyCollectionChangedAction.Reset, key, value);
        }
        protected Coords<KeyValuePair<string, TValue>> GetIndex(string key)
        {
            if (key is null) return null;
            var hash = key.GetHashCode();
            hash %= 30;
            return this.FindIndex(hash, key);
        }
        private Coords<KeyValuePair<string, TValue>> FindIndex(int dictKey, string key)
        {
            var coords = new Coords<KeyValuePair<string, TValue>>
            {
                dictionaryKey = dictKey
            };
            if (!this.InternalList.ContainsKey(coords.dictionaryKey))
            {
                coords.list = new List<KeyValuePair<string, TValue>>();
                this.InternalList.Add(coords.dictionaryKey, coords.list);
            }
            else
            {
                coords.list = this.InternalList[coords.dictionaryKey];
            }
            var listIndex = this.GetIndex(coords.list, key, 0);
            if (listIndex.HasValue)
            {
                coords.listIndex = listIndex.Value;
                return coords;
            }
            return null;
        }
        private int? GetIndex(List<KeyValuePair<string, TValue>> tlist, string key, int startingIndex)
        {
            int mi = startingIndex;
            int i = mi;
            int Mi = tlist.Count;

            if (Mi == mi) return -(mi + 1);
            int cr = tlist[i].Key.CompareTo(key);
            if (cr > 0) return -(mi + 1);
            if (cr == 0) return mi;
            if (Mi == 1) return -2;

            i = --Mi;
            cr = tlist[i].Key.CompareTo(key);
            if (cr < 0) return -Mi - 2;
            if (cr == 0) return Mi;

            bool doloop = true;
            while (doloop)
            {
                i = ((Mi - mi) / 2) + mi;
                cr = tlist[i].Key.CompareTo(key);
                if (cr > 0)
                {
                    Mi = i;
                    if ((Mi - mi) == 1) return -Mi - 1;
                }
                else if (cr < 0)
                {
                    mi = i;
                    if ((Mi - mi) == 1) return -Mi - 1;
                }
                if (cr == 0) return i;
            }
            return null;
        }
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            this.PropertyChanged?.Invoke(this, e);
        }
        private void OnPropertyChanged(string propertyName)
        {
            this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }
        private void OnCollectionReset()
        {
            this.OnCollectionChanged(new NotifyDictionaryChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
        private void OnCollectionChanged(NotifyCollectionChangedAction action, string key, TValue value)
        {
            this.OnCollectionChanged(new NotifyDictionaryChangedEventArgs(action, value, key));
        }
        protected virtual void OnCollectionChanged(NotifyDictionaryChangedEventArgs e)
        {
            if (this.DictionaryChanged is null) return;
            using (this.BlockReentrancy())
            {
                this.DictionaryChanged(this, e);
            }
        }
        protected IDisposable BlockReentrancy()
        {
            this._monitor.Enter();
            return this._monitor;
        }
        #endregion
        #region Private Types
        // this class helps prevent reentrant calls
#if !FEATURE_NETCORE
        [Serializable()]
        [TypeForwardedFrom("WindowsBase, Version=3.0.0.0, Culture=Neutral, PublicKeyToken=31bf3856ad364e35")]
#endif
        private class SimpleMonitor : IDisposable
        {
            public void Enter()
            {
                ++this._busyCount;
            }

            public void Dispose()
            {
                --this._busyCount;
            }

            public bool Busy { get => this._busyCount > 0; }

            int _busyCount;
        }

        #endregion
    }
}
