﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ObservableDictionary
{
    public class ObservableStringHashTable<TValue> : IObservableMap<TValue>, INotifyDictionaryChanged, INotifyPropertyChanged
    {
        #region fields
        private readonly System.Collections.Hashtable table = new System.Collections.Hashtable();
        #endregion
        #region public
        public TValue this[string key]
        {
            get => (TValue)this.table[key];
            set
            {
                this.table[key] = value;
                this.OnPropertyChanged(CountString);
                this.OnPropertyChanged(IndexerNameBeg + IndexerNameEnd);
                this.OnPropertyChanged(IndexerNameBeg + key + IndexerNameEnd);
                this.OnCollectionChanged(NotifyCollectionChangedAction.Reset, key, value);
            }
        }

        public void Add(string key, TValue value)
        {
            this.table.Add(key, value);
            this.OnPropertyChanged(CountString);
            this.OnPropertyChanged(IndexerNameBeg + IndexerNameEnd);
            this.OnPropertyChanged(IndexerNameBeg + key + IndexerNameEnd);
            this.OnCollectionChanged(NotifyCollectionChangedAction.Add, key, value);
        }
        public void Clear()
        {
            this.table.Clear();
            this.OnPropertyChanged(CountString);
            this.OnPropertyChanged(IndexerNameBeg + IndexerNameEnd);
            this.OnCollectionReset();
        }
        public void Remove(string key)
        {
            TValue removedItem = this[key];
            this.table.Remove(key);
            this.OnPropertyChanged(CountString);
            this.OnPropertyChanged(IndexerNameBeg + IndexerNameEnd);
            this.OnPropertyChanged(IndexerNameBeg + key + IndexerNameEnd);
            this.OnCollectionChanged(NotifyCollectionChangedAction.Remove, key, removedItem);
        }
        public bool ContainsKey(string key)
        {
            return this.table.ContainsKey(key);
        }
        public IEnumerator<KeyValuePair<string, TValue>> GetEnumerator()
        {
            return null;
            //foreach (var item in this.table.) yield return item
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.table.GetEnumerator();
        }

        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add => this.PropertyChanged += value;
            remove => this.PropertyChanged -= value;
        }
#if !FEATURE_NETCORE
        [field: NonSerializedAttribute()]
#endif
        public virtual event NotifyDictionaryChangedEventHandler DictionaryChanged;

#if !FEATURE_NETCORE
        [field: NonSerializedAttribute()]
#endif
        protected virtual event PropertyChangedEventHandler PropertyChanged;

        private void OnCollectionChanged(NotifyCollectionChangedAction action, string key, TValue value)
        {
            this.OnCollectionChanged(new NotifyDictionaryChangedEventArgs(action, value, key));
        }
        protected virtual void OnCollectionChanged(NotifyDictionaryChangedEventArgs e)
        {
            if (this.DictionaryChanged is null) return;
            using (this.BlockReentrancy())
            {
                this.DictionaryChanged(this, e);
            }
        }
        protected IDisposable BlockReentrancy()
        {
            this._monitor.Enter();
            return this._monitor;
        }
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            this.PropertyChanged?.Invoke(this, e);
        }

        #endregion
        #region private
        private void OnPropertyChanged(string propertyName)
        {
            this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }
        private void OnCollectionReset()
        {
            this.OnCollectionChanged(new NotifyDictionaryChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        #endregion Private Methods
        #region Private Types

        // this class helps prevent reentrant calls
#if !FEATURE_NETCORE
        [Serializable()]
        [TypeForwardedFrom("WindowsBase, Version=3.0.0.0, Culture=Neutral, PublicKeyToken=31bf3856ad364e35")]
#endif
        private class SimpleMonitor : IDisposable
        {
            public void Enter()
            {
                ++this._busyCount;
            }

            public void Dispose()
            {
                --this._busyCount;
            }

            public bool Busy { get => this._busyCount > 0; }

            int _busyCount;
        }

        #endregion Private Types
        #region Private Fields

        private const string CountString = "Count";

        // This must agree with Binding.IndexerName.  It is declared separately
        // here so as to avoid a dependency on PresentationFramework.dll.
        private const string IndexerNameBeg = "Item[";
        private const string IndexerNameEnd = "]";

        private SimpleMonitor _monitor = new SimpleMonitor();

        #endregion Private Fields
    }
}
