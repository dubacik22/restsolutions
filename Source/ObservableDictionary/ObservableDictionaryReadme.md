Library similar to Observablecollection, but it contains KeyValues and it can search by Key. Key is strictly string because of searching. The dictionary is intended to have large amount of items.
Library have this dictionaries:
ObservableStringDictionary
ObservableStringHashTable
ObservableStringSortedDictionary
MoreObservableStringDictionary

Each class have common functionality, but is slightly different in internal functionality and speed. So far the MoreObservableStringDictionary is quickest when you have large amount of data. Each class implements IObservableMap<TValue>, INotifyDictionaryChanged, INotifyPropertyChanged.

IObservableMap: defines basic functionality of ObservableDictionary like Add, Remove, ContainsKey and this[strig key]
INotifyDictionaryChanged: is similar to INotifyCollectionChanged, but it have event NotifyDictionaryChangedEventHandler DictionaryChanged
INotifyPropertyChanged: dont need to describe
