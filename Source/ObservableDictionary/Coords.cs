﻿using System.Collections.Generic;

namespace ObservableDictionary
{
    public class Coords<T>
    {
        public int dictionaryKey;
        public int listIndex;
        public List<T> list;
    }
}
