﻿using System.Collections.Specialized;

namespace ObservableDictionary
{
    public delegate void NotifyDictionaryChangedEventHandler(object sender, NotifyDictionaryChangedEventArgs e);
    public class NotifyDictionaryChangedEventArgs
    {
        public NotifyCollectionChangedAction Action { get; }
        public object AddedKey { get; }
        public object AddedItem { get; }
        public object RemovedKey { get; }
        public object RemovedItem { get; }

        public NotifyDictionaryChangedEventArgs(NotifyCollectionChangedAction action)
        {
            this.Action = action;
        }
        public NotifyDictionaryChangedEventArgs(NotifyCollectionChangedAction action, object changedItem, object key)
        {
            this.Action = action;
            this.AddedItem = changedItem;
            this.AddedKey = key;
        }
        public NotifyDictionaryChangedEventArgs(NotifyCollectionChangedAction action, object addedItem, object addedKey, object removedItem, object removedKey)
        {
            this.Action = action;
            this.AddedItem = addedItem;
            this.AddedKey = addedKey;
            this.RemovedItem = removedItem;
            this.RemovedKey = removedKey;
        }
    }
}
