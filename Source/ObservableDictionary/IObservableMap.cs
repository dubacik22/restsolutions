﻿using System.Collections.Generic;

namespace ObservableDictionary
{
    public interface IObservableMap<TValue> : IEnumerable<KeyValuePair<string, TValue>>, INotifyDictionaryChanged
    {
        TValue this[string key] { get; set; }
        bool ContainsKey(string key);
        void Remove(string key);
        void Add(string key, TValue value);
    }
}
